
#Compilador
CC = gcc

#Atributos del propio makefile
CURRENT_DIR = $(notdir $(CURDIR))
CURRENTVER = 1.5
VERDATE = 2017/04/29
AUTHOR = Jesualdo Garcia Lopez

#Directorios del programa.
SRCDIR = src
OBJDIR = obj
DOCDIR = doc
HDRDIR = includes

#Directorios de librerias.
LIBDIR = lib
SRCLIBDIR = srclib
OBJLIBDIR = objlib
LIB_UTILS = libutils
LIB_CON = connect
LIBDIR_LIB = $(LIBDIR)/$(LIB_UTILS)
LIBDIR_CON = $(LIBDIR)/$(LIB_CON)
#LIBDIR_ALL = $(LIBDIR_CON) $(LIBDIR_LIB) $(LIBDIR_CON)
SRCLIBDIR_LIB = $(SRCLIBDIR)/$(LIB_UTILS)
SRCLIBDIR_CON = $(SRCLIBDIR)/$(LIB_CON)
OBJLIBDIR_LIB = $(OBJLIBDIR)/$(LIB_UTILS)
OBJLIBDIR_CON = $(OBJLIBDIR)/$(LIB_CON)
LIBRARES = $(OBJLIBDIR)/$(LIB_UTILS).a $(OBJLIBDIR)/$(LIB_CON).a
#Documentacion
MANDIR = man

#Banderas del programa
CFLAGS =  -Wall -g
LFLAGS =
LDIRCFLAGS = -lircinterface -lircredes -lirctad -lsoundredes
LDMYFLAGS = $(OBJLIBDIR)/connect.a $(OBJLIBDIR)/libutils.a
LDFLAGS = -lm -pthread $(LDIRCFLAGS) $(LDMYFLAGS)

CLI_CFLAGS = `pkg-config --cflags gtk+-3.0` -rdynamic
CLI_LFLAGS = `pkg-config --libs gtk+-3.0` -rdynamic

OBJECTS_CON = $(patsubst $(SRCLIBDIR_CON)/%.c, $(OBJLIBDIR_CON)/%.o, $(wildcard $(SRCLIBDIR_CON)/*.c))
OBJECTS_LIB = $(patsubst $(SRCLIBDIR_LIB)/%.c, $(OBJLIBDIR_LIB)/%.o, $(wildcard $(SRCLIBDIR_LIB)/*.c))
OBJECTS = 	  $(patsubst $(SRCDIR)/%.c,  	   $(OBJDIR)/%.o,        $(wildcard $(SRCDIR)/*.c))
OBJECTS_SRV = $(patsubst $(SRCDIR)/server/%.c, $(OBJDIR)/%.o,        $(wildcard $(SRCDIR)/server/*.c))
OBJECTS_CLI = $(patsubst $(SRCDIR)/client/%.c, $(OBJDIR)/%.o,        $(wildcard $(SRCDIR)/client/*.c))
PROGRAMS = server client

all: authors libraries programs
	@echo "\033[33mAll done\033[0m"

libraries: $(LIBRARES)
	@echo "\033[31mLibraries created:\033[0m $^"

programs: $(PROGRAMS)
	@echo "\033[31mPrograms created:\033[0m $^"

server: $(OBJECTS_SRV) $(OBJECTS)
	@echo "\033[96mLinking\033[0m $@"
	@$(CC) -o $@ $^ $(LDFLAGS)

client: $(OBJECTS_CLI) $(OBJECTS)
	@echo "\033[96mLinking\033[0m $@"
	@$(CC) -o $@ $^ $(LDFLAGS) $(CLI_LFLAGS)

$(OBJECTS_SRV): $(OBJDIR)/%.o: $(SRCDIR)/server/%.c
	@echo "\033[94mCompiling\033[0m $@"
	@$(CC) -c $(CFLAGS) -o $@ $< -I$(HDRDIR) -I$(LIBDIR_CON) -I$(LIBDIR) -I$(LIBDIR_LIB)

$(OBJECTS_CLI): $(OBJDIR)/%.o: $(SRCDIR)/client/%.c
	@echo "\033[94mCompiling\033[0m $@"
	@$(CC) -c $(CFLAGS) -o $@ $< $(CLI_CFLAGS) -I$(HDRDIR) -I$(LIBDIR_CON) -I$(LIBDIR) -I$(LIBDIR_LIB)

$(OBJLIBDIR)/$(LIB_UTILS).a: $(OBJECTS_LIB)
	@echo "\033[95mCreating\033[0m library $@"
	@ar rcs $@ $^

$(OBJLIBDIR)/$(LIB_CON).a: $(OBJECTS_CON)
	@echo "\033[95mCreating\033[0m library $@"
	@ar rcs $@ $^

$(OBJECTS_CON): $(OBJLIBDIR_CON)/%.o: $(SRCLIBDIR_CON)/%.c $(LIBDIR_CON) $(LIBDIR)
	@echo "\033[94mCompiling\033[0m $@"
	@$(CC) -c $(CFLAGS) -o $@ $< -I$(LIBDIR) -I$(LIBDIR_CON)

$(OBJECTS_LIB): $(OBJLIBDIR_LIB)/%.o: $(SRCLIBDIR_LIB)/%.c $(LIBDIR_LIB) $(LIBDIR)
	@echo "\033[94mCompiling\033[0m $@"
	@$(CC) -c $(CFLAGS) -o $@ $< -I$(LIBDIR) -I$(LIBDIR_LIB) -I$(LIBDIR_CON)

authors:
	@echo "\033[1m=================================="
	@echo "======= Practica G-2362-06 ======="
	@echo "====== Jesualdo García López ====="
	@echo "======== 2017/04/29 = 1.5 ========"
	@echo "==================================\033[21m"

cleanall: clean cleanlibs
	@rm -f $(PROGRAMS)

clean:
	@rm -f $(OBJDIR)/*.o

cleanlibs:
	@rm -f $(OBJLIBDIR)/$(LIB_UTILS)/*.o
	@rm -f $(OBJLIBDIR)/$(LIB_CON)/*.o
	@rm -f $(OBJLIBDIR)/*.a

archive: cleanall
	@echo "\033[95mArchiving\033[0m"
	@tar --exclude='$(CURRENT_DIR)/tests' --warning=no-file-changed --exclude='$(CURRENT_DIR)/obj/*' --exclude='$(CURRENT_DIR)/.*' --exclude='.*' --exclude='$(CURRENT_DIR).tar.gz' -C .. -zcvf $(CURRENT_DIR).tar.gz $(CURRENT_DIR) || true #Para que no devuelva error.
