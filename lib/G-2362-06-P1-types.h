#ifndef _TYPES_H
#define _TYPES_H

/* Resultado de funciones OK/ERR */
#define OK 0
#define ERR -1
/*Valor de retorno de finalizaci&oacute;n.*/
#define FINISHED 5
/* Macros definidos para usar True False como tal. */
#define True 1
#define False 0

#define IRCERR_ALREADYREGISTERED        0b11100000000000000000000000000000
#define IRCERR_CHANCANNOTSENDTOSOME     0b11100000000000000000000000000001
#define IRCERR_CHANCANNOTSENDTOMULTIPLE 0b11100000000000000000000000000010
#define IRCERR_CANNOTRECV               0b11100000000000000000000000001000

#endif
