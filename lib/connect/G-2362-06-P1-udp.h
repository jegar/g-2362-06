#ifndef udp_h
#define udp_h

#include "G-2362-06-P1-types.h"

/*Servidor*/

int servidorConexionUDP(char* ip_addr, int port);

int servidorRecibirDatosUDP(char* ip_addr, int port, char* message, int size);

int servidorEnviarDatosUDP(char* ip_addr, int port, char* message);

int servidorCerrarConexionUDP(char* ip_addr, int port);


/*Cliente*/

int clienteConexionUDP(char* ip_addr, int port);

int clienteRecibirDatosUDP(int sockfd, char* message, int length);

int clienteEnviarDatosUDP(int sockfd, char* message, int length);

int clienteCerrarConexionUDP(int sockfd);

#endif
