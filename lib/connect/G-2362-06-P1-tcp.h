/**
 * @file tcp.h
 * @author Alberto Velasco y Jesualdo Garcia
 * @date 11/02/17
 * @brief Fichero que contiene la descripcion de las funciones de la libreria TCP
 *
 */

#ifndef tcp_h
#define tcp_h

#include "G-2362-06-P1-types.h"
#include <sys/time.h>

/**
 * @brief Fichero que contiene la descripcion de las funciones de la libreria TCP
 *
 * Librería de conexión TCP
 */
 #define TCPERR_MSG_OK 		  ": La accion TCP se ha completado correctamente."
 #define TCPERR_MSG_OPEN 	  ": El socket TCP no se ha podido abrir."
 #define TCPERR_MSG_BIND 	  ": El socket TCP no se ha podido ligar."
 #define TCPERR_MSG_LISTEN 	  ": No se puede escuchar en el socket TCP."
 #define TCPERR_MSG_ACCEPT    ": Fallo al aceptar la conexion TCP."
 #define TCPERR_MSG_NOENOUGH  ": No se puede reservar memoria suficiente."
 #define TCPERR_MSG_CANTHOST  ": Fallo al obtener el host."
 #define TCPERR_MSG_CONNECT   ": Fallo al abrir la conexion TCP."
 #define TCPERR_MSG_RECV 	  ": Fallo al recibir TCP."
 #define TCPERR_MSG_SEND 	  ": Fallo al enviar TCP."
 #define TCPERR_MSG_CLOSE 	  ": Fallo al cerrar la conexion TCP."
 #define TCPERR_MSG_UNKN 	  ": Error TCP desconocido."


#define TCPERR_INT_MIN -12
#define MAXCLI 1000
/**
 * C&oacute;digos de error del modulo.
 */
enum TcpErrorCode {
    TCPERR_ERROR = TCPERR_INT_MIN,  /*!<Error generico.*/
    TCPERR_CLOSE,                   /*!<Error de cierre de conexion.*/
 	TCPERR_SEND,                    /*!<Error de enviado de datos.*/
 	TCPERR_RECV,                    /*!<Error de recepcion de datos.*/
    TCPERR_CONNECT,                 /*!<Error de conexion.*/
 	TCPERR_CANNOTGETHOST,           /*!<Error de obtenci&oacute;n del host.*/
 	TCPERR_NOENOUGHMEMORY,          /*!<Error de falta de memoria.*/
    TCPERR_SOCKOPT,
 	TCPERR_ACCEPT,                  /*!<Error de aceptacion de conexi&oacute;n.*/
 	TCPERR_LISTENSOCKET,            /*!<Error de escuchar en el socket.*/
 	TCPERR_BINDSOCKET,              /*!<Error de ligadura del socket.*/
 	TCPERR_OPENSOCKET,    			 /*!<Error de apertura de socket.*/
    TCP_OK = 0                      /*!<Caso de &eacute;xito.*/
};

 /*---SERVIDOR---*/

/**
 * @brief Establecer conexion TCP
 *
 * Establece la conexion con el servidor TCP usando el puerto que se le pase por
 * argumento
 *
 * @param port Puerto de la conexion
 * @return El sockfd del socket en caso de exito. En caso de error devuelve
 *         TCPERR_OPENSOCKET si no se puede abrir el socket,
 *         TCPERR_BINDSOCKET si no se puede hacer bind en el socket,
 *         TCPERR_LISTENSOCKET si no se puede escuchar en el socket.
 */
int tcp_server_connection(int port);

/**
 * Acepta una conexi&oacute;n TCP en el socket.
 * @param  sockval  Socket en el cual eceptamos la conexion TCP.
 * @param  hostname Parametro de salida. Devuelve el nombre del host en memoria
 *                  dinamica.
 * @param  ipAddr   Parametro de salida. Devuelve la IP recibida en el socket.
 * @return          En caso de &eacute;xito devuelve el descriptor de fichero
 *                  del socket. O en caso de fallo, devuelve
 *                  TCPERR_ACCEPT si no se puede aceptar la conexion,
 *                  TCPERR_NOENOUGHMEMORY si no se puede reservar memoria y
 *                  TCPERR_CANNOTGETHOST si no se puede conseguir el host.
 */
int tcp_server_accept(int sockval, char** hostname, char** ipAddr);

/**
 * @brief Funcion para recibir datos
 *
 * @param new_fd id socket
 * @param message Donde se almacena el mensaje
 * @param length Longitud maxima
 * @return TCP_OK si todo ha ido bien, TCPERR_RECV en caso fallo de RECV.
 */
int tcp_server_recieve(int new_fd, char* message, int length);

/**
 * @brief Funcion para enviar datos
 *
 * @param new_fd id socket
 * @param message mensaje a enviar
 * @param length Longitud del mensaje
 * @return TCP_OK si todo ha ido bien, TCPERR_SEND en caso fallo en el send.
 */
int tcp_server_send(int new_fd, char* message);

/**
 * @brief Funcion para cerrar la conexion
 *
 * @param new_fd id socket
 * @return TCP_OK si todo ha ido bien, TCPERR_CLOSE en caso contrario.
 */
int tcp_server_close(int sockfd);

/*---CLIENTE---*/

/**
 * @brief Funcion para conectarse un cliente
 *
 * @param ip_addr Ip a la que conectarse
 * @param port Puero al que conectarse
 * @return El sockfd del socket, o TCPERROR en caso de fallar.
 */
int tcp_client_connection(char* hostname, int port, struct timeval *timeout);

/**
 * @brief Funcion para recibir datos
 *
 * @param new_fd id socket
 * @param message Donde se almacena el mensaje
 * @param length Longitud maxima
 * @return OK si todo ha ido bien, ERR en caso de fallo
 */
int tcp_client_recieve(int sockfd, char* message, int length);

/**
 * @brief Funcion para enviar datos
 *
 * @param new_fd id socket
 * @param message mensaje a enviar
 * @param length Longitud del mensaje
 * @return OK si todo ha ido bien, ERR en caso de fallo
 */
int tcp_client_send(int sockfd, char* message, int length);

/**
 * @brief Funcion para cerrar la conexion
 *
 * @param new_fd id socket
 * @return OK si todo ha ido bien, ERR en caso de fallo
 */
int tcp_client_close (int sockfd);

/**
 * Funcion que escribe en stderr un error con su prioridad y formato si procede.
 * @param  error   Codigo de error TCP.
 * @param  format  Formato del mensaje que se escribira.
 * @param  VARARGS Argumentos del formato.
 * @return         El error de entrada.
 */
int TCPErr_perror (int error, char* format, ...);

/**
 * Funcion que escribe en syslog un error con su prioridad y formato si procede.
 * @param  error   Codigo de error TCP.
 * @param  format  Formato del mensaje que se escribira.
 * @param  VARARGS Argumentos del formato.
 * @return         El error de entrada.
 */
int TCPErr_syslog (int error, char* format, ...);

/**
 * @brief  Devuelve la prioridad de LOG de un tipo de error concreto.
 *
 * @param  error Error del que queremos saber la prioridad.
 * @return       La proiridad obtenida.
 */
int tcp_error_priority (enum TcpErrorCode error);

/**
 * @brief Funci&oacute;n que proporciona un prefijo asociado a un error concreto
 *        y lo encadena al principio de una cadena de formato.
 * @param  error     Error de el que queremos obtener la cadena formato.
 * @param  outstring Cadena de salida, su longitud debe ser mayor que
 *                   TCPERR_STRING_MSG.
 * @param  outlen    Logitud maxima de la cadena de salida. Por lo menos
 *                   TCPERR_STRING_MSG.
 * @return           TCP_OK en caso de exito, TCPERR_ERROR en caso de error.
 */
int tcp_error_phrase (int error, char* outstring, int outlen);


#endif
