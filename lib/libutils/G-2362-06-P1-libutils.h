#ifndef _LIBUTILS_H
#define _LIBUTILS_H

#include "G-2362-06-P1-types.h"
#include "G-2362-06-P1-tcp.h"
#include <redes2/irctad.h>
#include <redes2/irc.h>

#define IRC_NICK_LEN 9
#define IRC_USID_LEN 10
#define IRC_CHAN_LEN 200

/**
 * Env&iacute;a un mensaje a un canal IRC especificado de parte de un usuario.
 * No verifica si es un canal correcto.
 * @param  message Mensaje a enviar.
 * @param  channel Canal a donde enviarlo.
 * @param  user    Usuario que manda en mensaje.
 * @return         OK en caso de &eacute;xito, ERR en caso de fallo.
 */
 long channel_send (char* message, char* channel, User* user);

/**
 * Env&iacute;a un mensaje a todos los canales asociados con un usuario.
 * @param  message Mensaje a enviar.
 * @param  user    Usuario al cual est&aacute;n asociados los mensajes.
 * @return         OK en caso de &eacute;xito, ERR en caso de fallo.
 */
long all_channels_send   (char *message, User* user);

void channel_array_free (char **array, long num);
/**
 * Coloca la estructura User proporcionada a cero.
 * @param user Usuario a hacer 0.
 */
void user_zero  (User *user);

/**
 * Libera los punteros que se han reservado de memoria.
 * @param user Usuario del que limpiar los campos.
 */
void user_clear (User *user);
/**
 * Busca un usuario y lo rellena en la estructura.
 * @param  user Estructura que se rellenara. Debe de tener uno o varios de los
 *              siguentes campos: userId, user, nick, real o bien solo el
 *              socket.
 * @return      OK en caso de exito, ERR en caso de error.
 */
long user_find   (User *user);
/**
 * Crea una mascara de para un usuario.
 * @param  user Usuario en el cual se comprobara la mascara.
 * @param  mask Mascara a aplicar.
 * @return      True si la marcara coincide, False si no.
 */
int user_mask   (char *user, char* mask);
/**
 * A&nacute;ade un usuario de una estructura a la lista de usuarios del servidor.
 * @param  user El usuario a anyadir.
 * @return      Ok en caso de exito, ERR en caso de fallo.
 */
long user_new    (User* user);
/**
 * Renueva el timestamp de el usuario actual.
 * @param  user Usuario de el que renovar el timestamp.
 * @return      OK en caso de exito, ERR en caso de fallo.
 */
long user_set_ts (User* user);

long user_set (User* user, char* username, char* nick, char* real);

long user_complex (User* user, char** complexUser);

long user_delete (User* user);

void user_update (User *user);

#endif
