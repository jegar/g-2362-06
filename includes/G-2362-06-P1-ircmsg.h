#ifndef _IRCMSG_H
#define _IRCMSG_H

#include "G-2362-06-P1-serverinfo.h"

#define DEBUGTEXT   "\nDEBUG:"

void irc_errmsg     (int priority, char* errcode, char* message);
void irc_error      (long error, int priority, char* added);
void irc_notice     (char* format, ...);
void irc_debug      (char* format, ...);
void irc_info       (char* format, ...);
void irc_warning    (char* format, ...);

#endif
