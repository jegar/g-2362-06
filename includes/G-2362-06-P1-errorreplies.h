#ifndef _ERRORS_H
#define _ERRORS_H

#include <redes2/irc.h>
#include <redes2/irctad.h>

#include "G-2362-06-P1-tcp.h"
#include "G-2362-06-P1-serverinfo.h"
#include "G-2362-06-P1-ircmsg.h"

#define STRERR_ALREADYREG     "The user is already registered."
#define STREER_PASS_TOO_LATE  "The pasword must be the first message sent."
#define STRERR_MISSING_PARAM  "One or more of the required parameters are missing."
#define STRERR_CANNOT_USER    "Cannot create user."

/*#define ERROR_MAX_PARAMS 3*/ /*Parametros adicionales*/
/*Estos errores son los codigos de salida de error_manager, le dicen al programa
  que tipo de accion (si debe realizar alguna) debe realizar.*/
enum errorSeverity {NONE = 0, ERRSEV_NOTICE, ERRSEV_WARNING, ERRSEV_CRITICAL};

long error_needmoreparams        (User* user, char* failedcommand);
long error_alreadyregistered     (User* user);
long error_nonicknamegiven       (User* user);
long error_erroneusnickname      (User* user, char* erroneousnick);
long error_nicknameinuse         (User* user, char* erroneousnick);
long error_nickcollision         (User* user, char* nick2);
long error_unavailresource       (User* user, char* nickorchannel);
long error_restricted            (User* user);
long error_nooperhost            (User* user);
long error_passwdmismatch        (User* user);
long error_umodeunknownflag      (User* user);
long error_usersdontmatch        (User* user);
long error_noprivileges          (User* user);
long error_nosuchserver          (User* user, char* servername);
long error_wasnosuchnick         (User* user, char* nickname);
long error_badmask               (User* user, char* mask);
long error_cannotsendtochan      (User* user, char* channel);
long error_notexttosend          (User* user);
long error_notoplevel            (User* user, char* mask);
long error_wildtoplevel          (User* user, char* mask);
long error_badchanmask           (User* user, char* channel);
long error_badchannelkey         (User* user, char* channel);
long error_bannedfromchan        (User* user, char* channel);
long error_channelisfull         (User* user, char* channel);
long error_chanoprivsneeded      (User* user, char* channel);
long error_inviteonlychan        (User* user, char* channel);
long error_keyset                (User* user, char* channel);
long error_nochanmodes           (User* user, char* channel);
long error_nosuchchannel         (User* user, char* channel);
long error_notonchannel          (User* user, char* nick2, char* channel);
long error_toomanychannels       (User* user, char* channelname);
long error_toomanytargets        (User* user, char* target, char* errorcode, char* abortmsg);
long error_unknownmode           (User* user, char* modechar, char* channel);
long error_usernotinchannel      (User* user, char* nick2, char* channel);
long error_useronchannel         (User* user, char* user2, char* channel);
long error_nomotd                (User* user);
long error_nosuchservice         (User* user, char* servicename);
long error_cantkillserver        (User* user, char* msg);
long error_noorigin              (User* user, char* msg);
long error_fileerror             (User* user, char* filerror, char* file);
long error_nologin               (User* user, char* username);
long error_summondisabled        (User* user);
long error_usersdisabled         (User* user);
long error_nosuchnick            (User* user, char* nickname);
long error_unknowncommand        (User* user, char* errcommand);
long error_noadmininfo           (User* user);
long error_notregistered         (User* user);
long error_nopermforhost         (User* user);
long error_yourebannedcreep      (User* user);
long error_youwillbebanned       (User* user);
long error_banlistfull           (User* user, char *channel, char *character, char *msg);
long error_uniqopprivsneeded     (User* user, char* msg);
long error_norecipient           (User* user, char* errcommand);
long error_toomanymatches        (User* user, char* errcommand, char* mask, char* info);

#endif
