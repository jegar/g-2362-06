#ifndef _CLIERRORS_H
#define _CLIERRORS_H
#include "G-2362-06-P2-clientinfo.h"

long error_needmoreparams       (char* msg);
long error_alreadyregistred     (char* msg);
long error_nonicknamegiven      (char* msg);
long error_erroneusnickname     (char* msg);
long error_nicknameinuse        (char* msg);
long error_nickcollision        (char* msg);
long error_unavailresource      (char* msg);
long error_restricted           (char* msg);
long error_nooperhost           (char* msg);
long error_passwdmismatch       (char* msg);
long error_umodeunknownflag     (char* msg);
long error_usersdontmatch       (char* msg);
long error_noprivileges         (char* msg);
long error_nosuchserver         (char* msg);
long error_wasnosuchnick        (char* msg);
long error_badmask              (char* msg);
long error_cannotsendtochan     (char* msg);
long error_notexttosend         (char* msg);
long error_notoplevel           (char* msg);
long error_wildtoplevel         (char* msg);
long error_badchanmask          (char* msg);
long error_badchannelkey        (char* msg);
long error_bannedfromchan       (char* msg);
long error_channelisfull        (char* msg);
long error_chanoprivsneeded     (char* msg);
long error_inviteonlychan       (char* msg);
long error_keyset               (char* msg);
long error_nochanmodes          (char* msg);
long error_nosuchchannel        (char* msg);
long error_toomanychannels      (char* msg);
long error_toomanytargets       (char* msg);
long error_unknownmode          (char* msg);
long error_usernotinchannel     (char* msg);
long error_useronchannel        (char* msg);
long error_nomotd               (char* msg);
long error_nosuchservice        (char* msg);
long error_cantkillserver       (char* msg);
long error_noorigin             (char* msg);
long error_fileerror            (char* msg);
long error_nologin              (char* msg);
long error_summondisabled       (char* msg);
long error_usersdisabled        (char* msg);
long error_nosuchnick           (char* msg);
long error_unknowncommand       (char* msg);
long error_noadmininfo          (char* msg);
long error_notregistered        (char* msg);
long error_nopermforhost        (char* msg);
long error_yourebannedcreep     (char* msg);
long error_youwillbebanned      (char* msg);
long error_banlistfull          (char* msg);
long error_uniqopprivsneeded    (char* msg);
long error_norecipient          (char* msg);
long error_toomanymatches       (char* msg);
long error_notonchannel         (char* msg);

#endif
