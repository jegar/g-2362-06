#ifndef _PROCESAR_H
#define _PROCESAR_H

#include "G-2362-06-P1-types.h"
#include "G-2362-06-P1-errorreplies.h"
#include "G-2362-06-P1-tcp.h"
#include "G-2362-06-P1-command.h"
#include "G-2362-06-P1-libutils.h"



int     process_connection      (int socket);
int     process_new_connection  (int socket, char* hostname, char* ippadr);
int     process_message         (User *user, char *message);
void    process_delete_users   (void);
void    process_timeout        (void);

#endif
