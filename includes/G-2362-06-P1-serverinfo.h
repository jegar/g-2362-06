#ifndef _SRVINFO_H
#define _SRVINFO_H

/**
 * @file flags.c
 * @author Jesualdo Garcia
 * @date 20/03/2017
 * @brief Fichero de cabecera que gestiona las banderas internas del programa.
 */

#include "G-2362-06-P1-types.h"
#include "G-2362-06-P1-errorreplies.h"

/*Especificaciones del protocolo */
#define IRC_MSG_MAX_LEN     512
#define SERVER_NAME_LEN     63 /*Definido en las especificaciones */
#define CHAN_NAME_LEN       200

/* Banderas */
#define FLAG_DEBUG          0b0000000000000001 /*! Marca que el modo debug esta activado.*/
#define FLAG_VERBOSE        0b0000000000000010 /*! Marca que el modo verbose esta activado.*/
#define FLAG_SSL            0b0000000000000100 /*! Marca que la encriptacion SSL esta activada.*/
#define FLAG_DATA           0b0000000000001000 /*! Marca que se han tenido que enviar datos.*/
#define FLAG_NAME           0b0000000000010000 /*! Marca que se ha recibido el nombre*/
#define FLAG_DAEMON         0b0000000000100000 /*! Marca que el proceso se ha daemonizado.*/
#define FLAG_WILLDAEMON     0b0000000001000000 /*! Marca que el proceso se he pedido daemonizar*/

#define FLAG_ALL            0b1111111111111111 /*! Todas las banderas*/
#define FLAG_NONE           0b0000000000000000 /*! Ninguna bandera*/

typedef unsigned short Flag;
/**
 * Coloca el nombre del servidor, como un nombre del propio y otro del dominio.
 * @param  name   Nombre del servidor.
 * @param  domain Nombre del dominio donde este se encuentra.
 * @return        OK en caso de exito, ERR en caso de fallo.
 */
int server_set_name(char* name, char* domain);

/**
 * Coloca en los atributos del servidor la hora de creación como actual.
 */
void server_set_time();

/**
 * Coloca el archivo de MOTD.
 * @param  server_motf Archivo que contiene el MOTD.
 * @return             OK en caso de exito, ERR en caso de fallo.
 */
int server_set_motd_file(char* server_motf);

/**
 * Coloca la informacion del servidor.
 * @param  serverinfo Cadena de info del servidor de longitud max SERVER_INFO_LEN.
 * @return        OK en caso de exito, ERR en caso de fallo.
 */
int server_set_info(char* serverinfo);

/**
* Devuelve el nombre del servidor.
* @return Un puntero a la cadena name.
*/
char* server_get_name();

/**
 * Devuelve la cadena de versi&oacute;n del servidor.
 * @return Un puntero a la cadena version.
 */
char* server_get_vers();

/**
 * Devuelve la cadena de versi&oacute;n del servidor.
 * @return Un puntero a la cadena info.
 */
char* server_get_info();

/**
 * Devuelve el tiempo de creacion.
 * @return El tiempo de creaci&oacute;n.
 */
long server_get_time();

/**
 * Devuelve el archivo que contiene el MOTD.
 * @return El archivo que contiene el MOTD.
 */
char* server_get_motd_file();

/**
 * Lee el archivo de configuracion proporcionado.
 * @param  arg0 Nombre del programa.
 * @return      Ok en caso de exito ERR en caso de error.s
 */
int server_read_conf(char* arg0);

void server_show_attrs();

/**
 * Activa una (o varias) banderas.
 * @param flagname bandera(s) que comprobar.
 */
void flag_add (Flag flagname);

/**
 * Comprueba si una bandera esta activada o no. Puede usarse para varias, pero
 * devolverá 1 en caso de que alguna de ellas esté activada.
 * @param  flagname bandera(s) que comprobar.
 * @return 1 si alguna de las banderas estan activadas, 0 si no.
 */
int flag_check (Flag flagname);

/**
 * Comprueba que todas las banderas que se le pasen en un OR esten activadas.
 * @param  flagname Una o varias banderas a comprobar.
 * @return          1 si están todas activadas, 0 si no.
 */
int flag_check_mul (Flag flagname);

/**
 * Desactiva la bandera.
 * @param flagname Bandera(s) a desactivar.
 */
void flag_del (Flag flagname);

#endif
