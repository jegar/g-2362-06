#ifndef _REPLIES_H
#define _REPLIES_H



#include "G-2362-06-P1-serverinfo.h"
#include "G-2362-06-P1-errorreplies.h"
#include "G-2362-06-P1-tcp.h"
#include "G-2362-06-P1-libutils.h"

long reply_privmsg_user         (User *user, char* nickdest, char* msg);
long reply_privmsg_chan         (User *user, char* channel, char* msg);
long reply_umodeis              (User *user, long modeOnChannel);
long reply_quit                 (User* user, char* msg);
long reply_yourhost             (User* user);
long reply_endofwhois           (User* user);
long reply_whoisuser            (User* user, User* userdest);
long reply_whoischannels        (User* user, User* userdest);
long reply_whoisoperator        (User* user, User* userdest);
long reply_whoisserver          (User* user, User* userdest);
long reply_whoisidle            (User* user, User* userdest);
long reply_whois                (User* user, User* targetuser);
long reply_whoreply             (User *user, User *userDest, char *channel, char* oppar, char* mask);
long reply_channelmodechange    (User *user, char* channel, char* inputmode);
long reply_channelmodeis        (User *user, char* channel, char* mode);
long reply_endofnames           (User* user, char* channel);
long reply_liststart            (User *user);
long reply_lists                (User* user, char* channel);
long reply_list                 (User* user, char* channel);
long reply_listend              (User *user);
long reply_namreply             (User* user, char* channel);
long reply_notopic              (User *user, char* channel);
long reply_newtopic             (User* user, char* channel, char* topic);
long reply_kicked               (User* user, User* userdest, char* comment, char* channel);
long reply_topic                (User *user, char* channel, char* topic);
long reply_part                 (User* user, char* channel, char* msg);
long reply_motd_start           (User* user);
long reply_motd                 (User *user);
long reply_motd_end             (User* user);
long reply_pong                 (User* user, char* server);
long reply_ping                 (User* user, char* server);
long reply_nowaway              (User *user);
long reply_unaway               (User *user);
long reply_nick                 (User* user, char* newnick);
long reply_away                 (User* user, User* userdest);
long reply_welcome              (User* user);
long reply_created              (User* user);
long reply_join                 (User* user, char* channel);
long reply_adminme 			    ();
long reply_adminloc1 		    ();
long reply_adminloc2 		    ();
long reply_adminemail 		    ();
long reply_info 				();
long reply_uniqopis 			();
long reply_youreoper 		    ();
long reply_endoflinks 		    ();
long reply_exceptlist 		    ();
long reply_invitelist 		    ();
long reply_inviting 			();
long reply_endofinfo 		    ();
long reply_endofmotd 		    ();
long reply_endofstats 		    ();
long reply_links 			    ();
long reply_luserchannels 	    ();
long reply_luserclient 		    ();
long reply_luserme 			    ();
long reply_luserop 			    ();
long reply_luserunknown 		();
long reply_endofbanlist 		();
long reply_endofexceptlist 	    ();
long reply_endofinvitelist 	    ();
long reply_youreservice 	    ();
long reply_myinfo 			    ();
long reply_endofwho 		    ();
long reply_endofwhowas 		    ();
long reply_whowasuser 		    ();
long reply_banlist 			    ();
long reply_statscommands 	    ();
long reply_statslinkinfo 	    ();
long reply_statsoline 		    ();
long reply_statsuptime 		    ();
long reply_time 			    ();
long reply_traceclass 		    ();
long reply_traceconnect 	    ();
long reply_traceconnecting 	    ();
long reply_tracehandshake 	    ();
long reply_tracelink 		    ();
long reply_tracenewtype 	    ();
long reply_traceoperator 	    ();
long reply_traceserver 		    ();
long reply_traceservice 	    ();
long reply_traceuser 		    ();
long reply_traceunknown 	    ();
long reply_tracelog 		    ();
long reply_traceend 		    ();
long reply_version 			    ();
long reply_servlist 		    ();
long reply_servlistend 		    ();
long reply_endofusers 		    ();
long reply_ison 			    ();
long reply_nousers 			    ();
long reply_rehashing 		    ();
long reply_summoning 		    ();
long reply_userhost 		    ();
long reply_users 			    ();
long reply_usersstart 		    ();
long reply_bounce 			    ();
long reply_tryagain 		    ();
long reply_yourid 			    ();
long reply_creationtime 	    ();
long reply_localusers 		    ();
long reply_globalusers 		    ();
long reply_topicwhotime 	    ();
long reply_channelurl 		    ();

#endif
