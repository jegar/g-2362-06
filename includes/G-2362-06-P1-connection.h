/*
    Este objeto gestionará las conexiones en general.
 */
#ifndef _CONNECTION_H
#define _CONNECTION_H

#include "G-2362-06-P1-types.h"
#include "G-2362-06-P1-errorreplies.h"

#include "G-2362-06-P1-tcp.h"
#include "G-2362-06-P1-udp.h"
#include "G-2362-06-P1-process.h"

#define MAX_THREADS 1000

/*typedef struct _connections_t Connections;*/

int     connect_create          (int port);
int     connect_in              (void);
int     connect_send            (int socket, char* message);
void    connect_close           (int port);
void    connect_end             (void);
void    connect_timeout         (void);

#endif
