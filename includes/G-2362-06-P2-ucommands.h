#ifndef _PARSECOMM_H
#define _PARSECOMM_H

#include "G-2362-06-P1-tcp.h"
#include "G-2362-06-P2-clientinfo.h"

typedef long (* ucmdFunc) (char*);

long ucmd_name      (char* command);
long ucmd_help      (char* command);
long ucmd_list      (char* command);
long ucmd_join      (char* command);
long ucmd_part      (char* command);
long ucmd_leave     (char* command);
long ucmd_quit      (char* command);
long ucmd_nick      (char* command);
long ucmd_away      (char* command);
long ucmd_whois     (char* command);
long ucmd_invite    (char* command);
long ucmd_kick      (char* command);
long ucmd_topic     (char* command);
long ucmd_me        (char* command);
long ucmd_msg       (char* command);
long ucmd_query     (char* command);
long ucmd_notice    (char* command);
long ucmd_notify    (char* command);
long ucmd_ignore    (char* command);
long ucmd_ping      (char* command);
long ucmd_who       (char* command);
long ucmd_whowas    (char* command);
long ucmd_ison      (char* command);
long ucmd_cycle     (char* command);
long ucmd_motd      (char* command);
long ucmd_rule      (char* command);
long ucmd_lusers    (char* command);
long ucmd_version   (char* command);
long ucmd_admi      (char* command);
long ucmd_userhost  (char* command);
long ucmd_knock     (char* command);
long ucmd_vhost     (char* command);
long ucmd_mode      (char* command);
long ucmd_time      (char* command);
long ucmd_botmotd   (char* command);
long ucmd_identify  (char* command);
long ucmd_dns       (char* command);
long ucmd_userip    (char* command);
long ucmd_stats     (char* command);
long ucmd_ctcp      (char* command);
long ucmd_dcc       (char* command);
long ucmd_map       (char* command);
long ucmd_link      (char* command);
long ucmd_setname   (char* command);
long ucmd_license   (char* command);
long ucmd_module    (char* command);
long ucmd_partall   (char* command);
long ucmd_chat      (char* command);
long ucmd_back      (char* command);
long ucmd_unaway    (char* command);
long ucmd_close     (char* command);
long ucmd_oper      (char* command);
long ucmd_fsen      (char* command);
long ucmd_faccept   (char* command);
long ucmd_fclose    (char* command);
long ucmd_priv      (char* command);

#endif
