#ifndef _CLIENTINFO_H
#define _CLIENTINFO_H

#include "G-2362-06-P1-types.h"
#include "G-2362-06-P1-tcp.h"

#define IRC_MSG_MAX_LEN     512
#define IRC_NICK_MAX_LEN    9
#define TCP_SGMNT_MAX_LEN   8192
#define TIMESTAMP_LEN       10
#define NICK_WHITESPACES    7
#define NICKSPACE           (IRC_NICK_MAX_LEN + TIMESTAMP_LEN + NICK_WHITESPACES + 1)

#define CHAN_NAM_LEN        20
#define SERVER_NAME_LEN     63 /*Definido en las especificaciones */

void    start_client_info    ();

void    set_server_name      (char* nick);
char*   get_server_name      ();
void    set_socket_fd        (int fd);
int     get_socket_fd        ();

int     recep_thread_start   (void* recep_funct);
int     recep_thread_check   () ;
void    recep_thread_stop    ();
int     recep_thread_join    () ;
void    recep_thread_stop_own();
char*   add_time_format      (char* nick);


int     add_to_ignore        (char* nick);
int     check_ignore         (char* nick);
void    remove_ignore        (char* nick);

long    send_msg             (char* message);
long    check_complex_prefix (char* prefix);

#endif
