#ifndef _COMMAND_H
#define _COMMAND_H

#define IRC_NICK_LEN 9
#define IRC_USID_LEN 10

#include "G-2362-06-P1-types.h"
#include "G-2362-06-P1-errorreplies.h"
#include "G-2362-06-P1-ircmsg.h"
#include "G-2362-06-P1-reply.h"
#include "G-2362-06-P1-libutils.h"

long cmd_pass 		(char* command, User *user);
long cmd_nick 		(char* command, User *user);
long cmd_user 		(char* command, User *user);
long cmd_oper 		(char* command, User *user);
long cmd_mode 		(char* command, User *user);
long cmd_service 	(char* command, User *user);
long cmd_quit 		(char* command, User *user);
long cmd_squit 		(char* command, User *user);
long cmd_unexpected_quit            (User *user);
long cmd_join 		(char* command, User *user);
long cmd_part 		(char* command, User *user);
long cmd_topic 		(char* command, User *user);
long cmd_names 		(char* command, User *user);
long cmd_list 		(char* command, User *user);
long cmd_invite 	(char* command, User *user);
long cmd_kick 		(char* command, User *user);
long cmd_privmsg 	(char* command, User *user);
long cmd_notice 	(char* command, User *user);
long cmd_motd 		(char* command, User *user);
long cmd_lusers 	(char* command, User *user);
long cmd_version 	(char* command, User *user);
long cmd_stats 		(char* command, User *user);
long cmd_links 		(char* command, User *user);
long cmd_time 		(char* command, User *user);
long cmd_connect 	(char* command, User *user);
long cmd_trace 		(char* command, User *user);
long cmd_admin 		(char* command, User *user);
long cmd_info 		(char* command, User *user);
long cmd_servlist 	(char* command, User *user);
long cmd_squery 	(char* command, User *user);
long cmd_who 		(char* command, User *user);
long cmd_whois 		(char* command, User *user);
long cmd_whowas 	(char* command, User *user);
long cmd_kill 		(char* command, User *user);
long cmd_ping 		(char* command, User *user);
long cmd_pong 		(char* command, User *user);
long cmd_error 		(char* command, User *user);
long cmd_away 		(char* command, User *user);
long cmd_rehash 	(char* command, User *user);
long cmd_die 		(char* command, User *user);
long cmd_restart 	(char* command, User *user);
long cmd_summon 	(char* command, User *user);
long cmd_users 		(char* command, User *user);
long cmd_wallops 	(char* command, User *user);
long cmd_userhost 	(char* command, User *user);
long cmd_ison 		(char* command, User *user);
long cmd_help 		(char* command, User *user);
long cmd_rules 		(char* command, User *user);
long cmd_server 	(char* command, User *user);
long cmd_encap 		(char* command, User *user);
long cmd_cnotice 	(char* command, User *user);
long cmd_cprivmsg 	(char* command, User *user);
long cmd_namesx 	(char* command, User *user);
long cmd_silence 	(char* command, User *user);
long cmd_uhnames 	(char* command, User *user);
long cmd_watch 		(char* command, User *user);
long cmd_knock 		(char* command, User *user);
long cmd_userip 	(char* command, User *user);
long cmd_setname 	(char* command, User *user);

#endif
