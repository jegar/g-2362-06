#include "G-2362-06-P1-libutils.h"


/**
 * Coloca la estructura User proporcionada a cero.
 * @param user Usuario a hacer 0.
 */
void user_zero(User *user){
	user->user = NULL;
	user->actionTS = 0;
	user->away = NULL;
	user->channelsUserFirst = NULL;
	user->channelsUserLast = NULL;
	user->creationTS = 0;
	user->host = NULL;
	user->IP = NULL;
	user->nick = NULL;
	user->password = NULL;
	user->real = NULL;
	user->socket = 0;
	user->thrId = 0;
	user->userId = 0;
}

/**
 * Libera los punteros que se han reservado de memoria.
 * @param user Usuario del que limpiar los campos.
 */
void user_clear(User *user){
	if (user->user)			free(user->user);
	if (user->away)			free(user->away);
	if (user->host)			free(user->host);
	if (user->IP)			free(user->IP);
	if (user->nick)			free(user->nick);
	if (user->password)		free(user->password);
	if (user->real)			free(user->real);
}

/**
 * Busca un usuario y lo rellena en la estructura.
 * @param  user Estructura que se rellenara. Debe de tener uno o varios de los
 *              siguentes campos: userId, user, nick, real o bien solo el
 *              socket.
 * @return      OK en caso de exito, ERR en caso de error.
 */
long user_find(User *user){
	long errCheck;

	errCheck = IRCTADUser_GetData (&user->userId, &user->user, &user->nick,
					&user->real, &user->host, &user->IP, &user->socket,
					&user->creationTS, &user->actionTS, &user->away);
	return errCheck;
}

void user_update (User *user){
		IRCTADUser_SetActionTS(user->userId, user->user, user->nick, user->real);
		IRCTADUser_SetThreadId (user->userId, user->user, user->nick,
								user->real, pthread_self());
}

/**
 * Crea una mascara de para un usuario.
 * @param  user Usuario en el cual se comprobara la mascara.
 * @param  mask Mascara a aplicar.
 * @return      True si la marcara coincide, False si no.
 */
int user_mask(char *nick, char* mask){
	int i, j, len1, len2;

	len1 = strlen(nick);
	len2 = strlen(mask);
	for (i = 0, j= 0; (i < len1) && (j < len2); i++){
		if (nick[i] == mask[j]){
			j++;
		} else if (mask[j] == '?') {
			j++;
		} else if (mask[j] != '*'){
			break;
		}
	}
	if (nick[i] == '\0' && mask[j] == '\0')
		return True;
	else
		return False;
}

/**
 * A&nacute;ade un usuario de una estructura a la lista de usuarios del servidor.
 * @param  user El usuario a anyadir.
 * @return      Ok en caso de exito, ERR en caso de fallo.
 */
long user_new (User* user){
    long errCheck = IRCERR_NOUSER;
	if (user){
    	errCheck = IRCTADUser_New(user->user, user->nick, user->real,
							user->password, user->host, user->IP, user->socket);
	}
    return errCheck;
}

/**
 * Renueva el timestamp de el usuario actual.
 * @param  user Usuario de el que renovar el timestamp.
 * @return      OK en caso de exito, ERR en caso de fallo.
 */
long user_set_ts (User* user){
    long errCheck = IRCERR_NOUSER;
	if (user){
    	errCheck = IRCTADUser_SetActionTS (user->userId, user->user,
                                    	user->nick, user->real);
	}
    return errCheck;
}

long user_set (User* user, char* username, char* nick, char* real){
	long errCheck = IRCERR_NOUSER;
	if (user){
		errCheck = IRCTADUser_Set(user->userId, user->user, user->nick,
								user->real, username, nick, real);
	}
	return errCheck;
}

long user_complex (User* user, char** complexUser){
	long errCheck = IRCERR_NOUSER;

	if (user){
		errCheck = IRC_ComplexUser1459 (complexUser, user->nick, user->user,
									user->host, NULL);
	}
	return errCheck;
}

long user_delete (User* user){
	long errCheck = IRCERR_NOUSER;
	if (user){
		errCheck = IRCTADUser_Delete(user->userId, user->user,
									user->nick, user->real);
	}
	return errCheck;
}
