#include "G-2362-06-P1-libutils.h"
#include <syslog.h>

#define ERRLIBS_STR_LEN 510
#define SEND_TO_CHAN "Sending message to channel"
/**
 * Env&iacute;a un mensaje a un canal IRC especificado de parte de un usuario.
 * No verifica si es un canal correcto.
 * @param  message Mensaje a enviar.
 * @param  channel Canal a donde enviarlo.
 * @param  user    Usuario que manda en mensaje.
 * @return         OK en caso de &eacute;xito, ERR en caso de fallo.
 */
long channel_send (char* message, char* channel, User* user){
	long errCheck, numberOfUsers;
	register int i;
	int tcpErr;
	char** userlist;
	User userDest;

	errCheck = IRCTAD_ListNicksOnChannelArray (channel,
					&userlist, &numberOfUsers);
	if (errCheck == IRC_OK){
		for (i = 0; i < numberOfUsers; i++){
			if (strcmp(user->nick, userlist[i])){
				user_zero(&userDest);
		        userDest.nick = userlist[i];
				if (user_find(&userDest) == OK){
					tcpErr = tcp_server_send (userDest.socket, message);
					if (tcpErr != TCP_OK){
						errCheck = IRCERR_CHANCANNOTSENDTOSOME;
					}
					userDest.nick = NULL;
					user_clear(&userDest);
				}
			}
		}
		IRCTADUser_FreeList (userlist, numberOfUsers);
	}
	return errCheck;
}

/**
 * Env&iacute;a un mensaje a todos los canales asociados con un usuario.
 * @param  message Mensaje a enviar.
 * @param  user    Usuario al cual est&aacute;n asociados los mensajes.
 * @return         OK en caso de &eacute;xito, ERR en caso de fallo.
 */
long all_channels_send (char *message, User* user){
	long errCheck, numberOfChannels, chanErr;
	register int i;
	char** channelList;

	errCheck =  IRCTAD_ListChannelsOfUserArray (user->user, user->nick,
											&channelList, &numberOfChannels);
    if (errCheck == IRC_OK){
		for (i = 0; i < numberOfChannels; i++){
			chanErr = channel_send (message, channelList[i], user);
			if (chanErr != IRC_OK){
				errCheck = (errCheck == IRCERR_CHANCANNOTSENDTOSOME) ?
							IRCERR_CHANCANNOTSENDTOMULTIPLE :
							IRCERR_CHANCANNOTSENDTOSOME;
			}
		}
		IRCTADChan_FreeList(channelList, numberOfChannels);
	}
	return errCheck;
}

void channel_array_free (char **array, long num){
    register int i;

    if (array){
        for (i = 0; i < num; i++){
            if (array[i]){
                free(array[i]);
            }
        }
        free(array);
    }
}
