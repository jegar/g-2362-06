/**************************************
*** Libreria UDP
***
*** Autores:
*** 	Alberto Velasco Lopez de Ochoa
***		Jesualdo Garcia Lopez
***
*** Versión: 0.1
*************************************/

#include "G-2362-06-P1-udp.h"
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <syslog.h>
#include <netdb.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h>

/*Servidor*/

int servidorConexionUDP(char* ip_addr, int port);

int servidorRecibirDatosUDP(char* ip_addr, int port, char* message, int size);

int servidorEnviarDatosUDP(char* ip_addr, int port, char* message);

int servidorCerrarConexionUDP(char* ip_addr, int port);


/*Cliente*/

int clienteConexionUDP(char* ip_addr, int port);

int clienteRecibirDatosUDP(int sockfd, char* message, int length);

int clienteEnviarDatosUDP(int sockfd, char* message, int length);

int clienteCerrarConexionUDP(int sockfd);
