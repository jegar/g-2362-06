/**
 * @file tcp.c
 * @author Alberto Velasco y Jesualdo García
 * @date 11/02/17
 * @brief Fichero que contiene las funciones de la libreria TCP
 */

#include "G-2362-06-P1-tcp.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

//#include <sys/types.h>
#include <sys/socket.h>

#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <syslog.h>

#include <unistd.h>
#include <errno.h>

#define TCPERR_STRING_MSG 60

/*-----------------------------------------------SERVIDOR----------------------------------------------------*/

/**
 * @brief Establecer conexion TCP
 *
 * Establece la conexion con el servidor TCP usando el puerto que se le pase por
 * argumento
 *
 * @param port Puerto de la conexion
 * @return El sockfd del socket en caso de exito. En caso de error devuelve
 *            TCPERR_OPENSOCKET si no se puede abrir el socket,
 *            TCPERR_BINDSOCKET si no se puede hacer bind en el socket,
 *            TCPERR_LISTENSOCKET si no se puede escuchar en el socket.
 */
int tcp_server_connection(int port){
	int sockfd;
    struct sockaddr_in server;
    int option = 1;

    sockfd = socket(AF_INET , SOCK_STREAM , 0);
    if (sockfd == -1){
        return TCPERR_OPENSOCKET;
    }

    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons(port);

    if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR,
        (void *)&option, sizeof(option)) < 0){
            tcp_server_close(sockfd);
            return TCPERR_LISTENSOCKET;
    }

    if(bind(sockfd,(struct sockaddr *)&server , sizeof(server)) < 0){
        tcp_server_close(sockfd);
        return TCPERR_BINDSOCKET;
    }

    if (listen(sockfd, MAXCLI) == -1) {
        tcp_server_close(sockfd);
        return TCPERR_LISTENSOCKET;
    }

    return sockfd;
}

/**
 * @brief Acepta una conexi&oacute;n TCP en el socket.
 *
 * @param  sockval  Socket en el cual eceptamos la conexion TCP.
 * @param  hostname Parametro de salida. Devuelve el nombre del host en memoria
 *                  dinamica.
 * @param  ipAddr   Parametro de salida. Devuelve la IP recibida en el socket.
 * @return          En caso de &eacute;xito devuelve el descriptor de fichero
 *                  del socket. O en caso de fallo, devuelve
 *                  TCPERR_ACCEPT si no se puede aceptar la conexion,
 *                  TCPERR_NOENOUGHMEMORY si no se puede reservar memoria y
 *                  TCPERR_CANNOTGETHOST si no se puede conseguir el host.
 */
int tcp_server_accept(int sockval, char** hostname, char** ipAddr){
    int desc, len;
    struct sockaddr_in conexion;
	struct hostent *host;

	len = sizeof (struct sockaddr_in);
    if ((desc = accept(sockval, (struct sockaddr *) &conexion, (socklen_t*)&len))<0){
        return TCPERR_ACCEPT;
    }
	*ipAddr = (char*)malloc(sizeof(char)*INET_ADDRSTRLEN);
	if (*ipAddr == NULL){
		return TCPERR_NOENOUGHMEMORY;
	}

	inet_ntop(AF_INET, &conexion.sin_addr.s_addr, *ipAddr, INET_ADDRSTRLEN );
	len = sizeof(struct in_addr);
	host = gethostbyaddr(&conexion.sin_addr, (socklen_t)len, AF_INET);
	if (host == NULL){
        free(*ipAddr);
		return TCPERR_CANNOTGETHOST;
	}

    *hostname = (char*)malloc((strlen(host->h_name)+1) * sizeof(char));
	if (*hostname == NULL){
        free(*ipAddr);
		return TCPERR_NOENOUGHMEMORY;
	}
	strcpy(*hostname, host->h_name);

    return desc;
}

/**
 * @brief Funcion para recibir datos
 *
 * @param new_fd id socket
 * @param message Donde se almacena el mensaje
 * @param length Longitud maxima
 * @return TCP_OK si todo ha ido bien, TCPERR_RECV en caso fallo de RECV.
 */
int tcp_server_recieve(int new_fd, char* message, int length){
    int numbytes = 0;

    //Recibir Datos
    memset(message, 0, length);
    if ((numbytes = recv(new_fd, message, length-1, 0 /*flags*/)) < 0) {
        return TCPERR_RECV;
    }
    message[numbytes] = '\0'; //Para indicar fin de cadena despues del mensaje.
    return TCP_OK;
}

/**
 * @brief Funcion para enviar datos
 *
 * @param new_fd id socket
 * @param message mensaje a enviar
 * @param length Longitud del mensaje
 * @return TCP_OK si todo ha ido bien, TCPERR_SEND en caso fallo en el send.
 */
int tcp_server_send(int new_fd, char* message){

    if (send(new_fd, message, strlen(message), 0) == -1){
        return TCPERR_SEND;
    }
    return TCP_OK;
}
/**
 * @brief Funcion para cerrar la conexion
 *
 * @param new_fd id socket
 * @return TCP_OK si todo ha ido bien, TCPERR_CLOSE en caso contrario.
 */
int tcp_server_close(int sockfd){

    if (shutdown(sockfd, SHUT_RDWR) == 0){
        if(close(sockfd) < 0){
    	   return TCPERR_CLOSE;
        }
    }

    return TCP_OK;
}

/*--------------------------------------------------------CLIENTE-------------------------------------------------*/

/**
 * @brief Funcion para conectarse un cliente
 *
 * @param ip_addr Ip a la que conectarse
 * @param port Puero al que conectarse
 * @return El sockfd del socket, o TCPERROR en caso de fallar.
 */
int tcp_client_connection(char* hostname, int port, struct timeval *timeout){
    struct sockaddr_in server;
	struct hostent *host;
    int sockfd = 0;
	int optsize;

    /*Socket*/
    sockfd = socket(AF_INET,SOCK_STREAM,0/*TCP*/);
    if (sockfd == -1){
        return TCPERR_OPENSOCKET;
    }

	optsize = (timeout != NULL)? sizeof(struct timeval) : 0 ;


	host = gethostbyname(hostname);

    memset(&server, '0', sizeof(server));
    server.sin_family = host->h_addrtype;
    server.sin_port = htons(port);
    memset(&(server.sin_zero), '\0', 8);

    server.sin_addr = *((struct in_addr *)host->h_addr_list[0]);

    /*Connect*/
    if (connect(sockfd, (struct sockaddr*)&server, sizeof(server)) < 0){
        return TCPERR_CONNECT;
    }

	if (setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO,
					(struct timeval *)timeout, optsize) < 0)
	{
			tcp_client_close(sockfd);
			return TCPERR_CONNECT;

	}

    return sockfd;
}

/**
 * @brief Funcion para recibir datos
 *
 * @param new_fd id socket
 * @param message Donde se almacena el mensaje
 * @param length Longitud maxima
 * @return El numero de bytes leidos, o -1 en caso de error.
 */
int tcp_client_recieve(int sockfd, char* message, int length){
	int numbytes = 0;

    /*Esperamos respuesta del servidor*/
	memset(message, 0, length);
    if ((numbytes = recv(sockfd, message, length-1, 0 /*flags*/)) == -1) {
        return TCPERR_RECV;
    }
    message[numbytes] = '\0';

    return numbytes;
}

/**
 * @brief Funcion para enviar datos
 *
 * @param new_fd id socket
 * @param message mensaje a enviar
 * @param length Longitud del mensaje
 * @return OK si todo ha ido bien, ERR en caso de fallo
 */
int tcp_client_send(int sockfd, char* message, int length){

    /*Enviamos el mensaje*/
    if (send(sockfd, (const void *)message, length, 0 /*flags*/) == -1){
        return TCPERR_SEND;
    }

    return TCP_OK;
}

/**
 * @brief Funcion para cerrar la conexion
 *
 * @param new_fd id socket
 * @return OK si todo ha ido bien, ERR en caso de fallo
 */
int tcp_client_close(int sockfd){

	if (shutdown(sockfd, SHUT_RDWR) == 0){
    	if(close(sockfd) < 0){
    		return TCPERR_CLOSE;
    	}
	}

    return TCP_OK;
}

/**
 * @brief  Devuelve la prioridad de LOG de un tipo de error concreto.
 *
 * @param  error Error del que queremos saber la prioridad.
 * @return       La proiridad obtenida.
 */
int tcp_error_priority (int error){
	int priority;

	switch (error) {
        case TCP_OK:
            priority = LOG_INFO;
            break;
        case TCPERR_CLOSE:
		case TCPERR_CONNECT:
		case TCPERR_CANNOTGETHOST:
		case TCPERR_NOENOUGHMEMORY:
		case TCPERR_ACCEPT:
			priority = LOG_ERR;
			break;
		case TCPERR_LISTENSOCKET:
		case TCPERR_SOCKOPT:
		case TCPERR_BINDSOCKET:
		case TCPERR_OPENSOCKET:
			priority = LOG_CRIT;
			break;
        case TCPERR_SEND:
		case TCPERR_RECV:
        case TCPERR_ERROR:
		default:
			priority = LOG_WARNING;
			break;
	}
	return priority;
}

/**
 * @brief Funci&oacute;n que proporciona un prefijo asociado a un error concreto
 *        y lo encadena al principio de una cadena de formato.
 * @param  error     Error de el que queremos obtener la cadena formato.
 * @param  outstring Cadena de salida, su longitud debe ser mayor que
 *                   TCPERR_STRING_MSG.
 * @param  outlen    Logitud maxima de la cadena de salida. Por lo menos
 *                   TCPERR_STRING_MSG.
 * @return           TCP_OK en caso de exito, TCPERR_ERROR en caso de error.
 */
int tcp_error_phrase (int error, char* outstring, int outlen){
	int retVal = TCPERR_ERROR;
	int posError;

	static const char * const errorMessages[] = {
		TCPERR_MSG_OK
		TCPERR_MSG_OPEN, TCPERR_MSG_BIND, TCPERR_MSG_LISTEN,
		TCPERR_MSG_ACCEPT, TCPERR_MSG_NOENOUGH,
		TCPERR_MSG_CANTHOST, TCPERR_MSG_CONNECT
		TCPERR_MSG_RECV, TCPERR_MSG_SEND, TCPERR_MSG_CLOSE,
		TCPERR_MSG_UNKN
	};

	if(outstring && (outlen >= TCPERR_STRING_MSG)){
		if (error >= 0){
			posError = 0;
		} else {
			posError = (error >= TCPERR_INT_MIN)? abs(error) : abs(TCPERR_INT_MIN);
		}
		outstring = strncat (outstring, errorMessages[posError],
			 				outlen - strlen(outstring));
		if (outstring)
			retVal = TCP_OK;
    }
    return retVal;
}
