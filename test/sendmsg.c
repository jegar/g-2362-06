#include "sendmsg.h"

long send_pass (int sockfd, char* password){
    long errCheck;
    int tcpErr;
    char* command;

    errCheck = IRCMsg_Pass (&command, NULL, password);
    if (errCheck == IRC_OK){
        tcpErr = tcp_client_send(sockfd, command, strlen(command));
        errCheck = (tcpErr == TCP_OK)? IRC_OK : errCheck;
        free(command);
    }
    return errCheck;
}

long send_nick (int sockfd, char* nick){
    long errCheck;
    int tcpErr;
    char* command;

    errCheck = IRCMsg_Nick (&command, NULL, nick, NULL);
    if (errCheck == IRC_OK){
        tcpErr = tcp_client_send(sockfd, command, strlen(command));
        errCheck = (tcpErr == TCP_OK)? IRC_OK : errCheck;
        free(command);
    }
    return errCheck;
}

long send_user (int sockfd, char* user, char* realname){
    long errCheck;

    errCheck = IRCMsg_User (&command, NULL, user, NULL, realname);
    if (errCheck == IRC_OK){
        tcpErr = tcp_client_send(sockfd, command, strlen(command));
        errCheck = (tcpErr == TCP_OK)? IRC_OK : errCheck;
        free(command);
    }
    return errCheck;
}
