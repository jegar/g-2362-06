#include "G-2362-06-P1-errorreplies.h"
#include "G-2362-06-P1-types.h"

#define ID_LEN 12

void ircerr_msg(int userid, int priority, char* errmsg, char* command){
    if (command && (priority >= 0)){
        if (flag_check(FLAG_DAEMON)){
            syslog(priority, "ID%*ld %s : %s",ID_LEN, userid, errmsg, command);
        } else {
            fprintf(stderr, "ID%*ld %s : %s",ID_LEN, userid, errmsg, command));
        }
    }
}

int ircerr_noavailableusermodes (User* user, char* message){

}

int ircerr_noavailablechannelmodes (User* user, char* message){
}

int ircerr_noerroneusnick (User* user, char* message){
}

int ircerr_nofailedcommand (User* user, char* message){
}

int ircerr_nohost (User* user, char* message){
}

int ircerr_nonick (User* user, char* message){
    long errCheck;
    char *prefix, *newnick, *msg;

    IRCParse_Nick(message, &prefix, &newnick, &msg);
    errCheck = error_erroneusnickname (user, newnick);
    IRC_MFree(3, &prefix, &newnick, &msg);

    if (errCheck != IRC_OK){
        return error_manager(user, message, errCheck);
    }
}

int ircerr_nonickorchannel (User* user, char* message){
}

int ircerr_noservername (User* user, char* message){
}

int ircerr_noservicename (User* user, char* message){
}

int ircerr_nouser (User* user, char* message){
}

int ircerr_nousermodestring (User* user, char* message){
}

int ircerr_noversion (User* user, char* message){
}

int ircerr_nopassword (User* user, char* message){
}

int ircerr_nomode (User* user, char* message){
}

int ircerr_norealname (User* user, char* message){
}

int ircerr_noname (User* user, char* message){
}

int ircerr_noservice (User* user, char* message){
}

int ircerr_nomaskdistribution (User* user, char* message){
}

int ircerr_noinfo (User* user, char* message){
}

int ircerr_nomessage (User* user, char* message){
}

int ircerr_noserver (User* user, char* message){
}

int ircerr_nochannel (User* user, char* message){
}

int ircerr_notarget (User* user, char* message){
}

int ircerr_nocommand (User* user, char* message){
}

int ircerr_unknowncommand (User* user, char* message){
}

int ircerr_erroneuscommand (User* user, char* message){
    long errCheck;
    errCheck = error_needmoreparams (user, message);
    ircerr_msg(user->userId, LOG_WARNING, "Params missing on command.", message);

    if (errCheck != IRC_OK){
        return error_manager(user, message, errCheck);
    }
    return ERRSEV_WARNING;
}

int ircerr_nopasscommand (User* user, char* message){
}

int ircerr_nonickcommand (User* user, char* message){
}

int ircerr_nousercommand (User* user, char* message){
}

int ircerr_noopercommand (User* user, char* message){
}

int ircerr_nomodecommand (User* user, char* message){
}

int ircerr_noservicecommand (User* user, char* message){
}

int ircerr_noquitcommand (User* user, char* message){
}

int ircerr_nosquitcommand (User* user, char* message){
}

int ircerr_nomask (User* user, char* message){
}

int ircerr_noquery (User* user, char* message){
}

int ircerr_noservermask (User* user, char* message){
}

int ircerr_nocount (User* user, char* message){
}

int ircerr_nooperatorparam (User* user, char* message){
}

int ircerr_toomanytargets (User* user, char* message){
}

int ircerr_noprivileges (User* user, char* message){
}

int ircerr_toomuchmask (User* user, char* message){
}

int ircerr_noparams (User* user, char* message){
}

int ircerr_nofixedmatch (User* user, char* message){
}

int ircerr_novalidusercommand (User* user, char* message){
}

int ircerr_nomodeparams (User* user, char* message){
}

int ircerr_noexceptionmask (User* user, char* message){
}

int ircerr_noinvitemask (User* user, char* message){
}

int ircerr_novisible (User* user, char* message){
}

int ircerr_nochannelname (User* user, char* message){
}

int ircerr_noerrcommand (User* user, char* message){
}

int ircerr_noerrorcode (User* user, char* message){
}

int ircerr_nomodechar (User* user, char* message){
}

int ircerr_nonickname (User* user, char* message){
}

int ircerr_nocharacter (User* user, char* message){
}

int ircerr_noport (User* user, char* message){
}

int ircerr_nofileerror (User* user, char* message){
}

int ircerr_nofile (User* user, char* message){
}

int ircerr_toomuchnicks (User* user, char* message){
}

int ircerr_noconfigfile (User* user, char* message){
}

int ircerr_noreply (User* user, char* message){
}

int ircerr_toomuchreplies (User* user, char* message){
}

int ircerr_noserverinfo (User* user, char* message){
}

int ircerr_nolinkname (User* user, char* message){
}

int ircerr_nohostmask (User* user, char* message){
}

int ircerr_noclass (User* user, char* message){
}

int ircerr_nodestination (User* user, char* message){
}

int ircerr_nonextserver (User* user, char* message){
}

int ircerr_nonewtype (User* user, char* message){
}

int ircerr_noclientname (User* user, char* message){
}

int ircerr_nocomplexuser (User* user, char* message){
}

int ircerr_notype (User* user, char* message){
}

int ircerr_noactivetype (User* user, char* message){
}

int ircerr_noavailablechannel (User* user, char* message){
}

int ircerr_prefixuserwithouthost (User* user, char* message){
}

int ircerr_prefixnickwithouthost (User* user, char* message){
}

int ircerr_noprefix (User* user, char* message){
}

int ircerr_nochanid (User* user, char* message){
}

int ircerr_nochanidlength (User* user, char* message){
}

int ircerr_nonicklength (User* user, char* message){
}

int ircerr_ip4overip6 (User* user, char* message){
}

int ircerr_noip4 (User* user, char* message){
}

int ircerr_noip (User* user, char* message){
}

int ircerr_notargetserver (User* user, char* message){
}

int ircerr_nochannelornick (User* user, char* message){
}

int ircerr_nostring (User* user, char* message){
    ircerr_msg (user->userId, LOG_WARNING,
        "Mensaje vacio.", message);
    return ERRSEV_WARNING;
}

int ircerr_nonumber (User* user, char* message){
}

int ircerr_nodigits (User* user, char* message){
}

int ircerr_nohexdigits (User* user, char* message){
}

int ircerr_nospecials (User* user, char* message){
}

int ircerr_nospecialsuppers (User* user, char* message){
}

int ircerr_nospecialslowers (User* user, char* message){
}

int ircerr_noletters (User* user, char* message){
}

int ircerr_nouppers (User* user, char* message){
}

int ircerr_nolowers (User* user, char* message){
}

int ircerr_nochannamelength (User* user, char* message){
}

int ircerr_nochanlength (User* user, char* message){
}

int ircerr_nokey (User* user, char* message){
}

int ircerr_nokeylength (User* user, char* message){
}

int ircerr_nouserlength (User* user, char* message){
}

int ircerr_noid (User* user, char* message){
}

int ircerr_outoflength (User* user, char* message){
}

int ircerr_params (User* user, char* message){
}

int ircerr_invalidchannelname (User* user, char* message){
}

int ircerr_channelnameinuse (User* user, char* message){
}

int ircerr_noenoughmemory (User* user, char* message){
}

int ircerr_undeletablechannel (User* user, char* message){
}

int ircerr_invalidnick (User* user, char* message){
}

int ircerr_invaliduser (User* user, char* message){
}

int ircerr_invalidrealname (User* user, char* message){
}

int ircerr_invalidusername (User* user, char* message){
}

int ircerr_undeletableuser (User* user, char* message){
}

int ircerr_userused (User* user, char* message){
}

int ircerr_nickused (User* user, char* message){
}

int ircerr_realnameused (User* user, char* message){
}

int ircerr_nomutex (User* user, char* message){
}

int ircerr_userslimitexceeded (User* user, char* message){
}

int ircerr_novalidchannel (User* user, char* message){
}

int ircerr_nousersonchannel (User* user, char* message){
}

int ircerr_nouseronchannel (User* user, char* message){
}

int ircerr_novaliduser (User* user, char* message){
}

int ircerr_nameinuse (User* user, char* message){
}

int ircerr_fail (User* user, char* message){
}

int ircerr_invalidmask (User* user, char* message){
}

int ircerr_baneduseronchannel (User* user, char* message){
}

int ircerr_noinviteduser (User* user, char* message){
}

int ircerr_yetinchannel (User* user, char* message){
}

int ircerr_invalidip (User* user, char* message){
}

int ircerr_invalidhost (User* user, char* message){
}

int ircerr_nocongruentnumberofchannels (User* user, char* message){
}

int ircerr_nocorrectformat (User* user, char* message){
}

int ircerr_unknownerror (User* user, char* message){
}

int ircerr_cantchangetopic (User* user, char* message){
}

int ircerr_commandtoolong (User* user, char* message){
}

int ircerr_isnotoperator (User* user, char* message){
}

int ircerr_alreadyregistered (User* user, char* message){
    long errCheck;

    errCheck = error_alreadyregistered (user);
    ircerr_msg(user->userId, LOG_NOTICE, "User already registered.", message);
    if (errCheck != IRC_OK){
        return error_manager(user, message, errCheck);
    }
    return ERRSEV_NOTICE;
}

int ircerr_chancannotsendtosome (User* user, char* message){
}

int ircerr_chancannotsendtomultiple (User* user, char* message){
}

int ircerr_cannotsend (User* user, char* message){
}

int ircerr_cannotrecv (User* user, char* message){
}


int error_manager (User* user, char* message, long errId){
    int retVal = ERR;
    int priority;

    if (user){
        if (message){
            if (errId < 0){
                switch(errId){
                    case IRCERR_NOAVAILABLEUSERMODES:
                        retVal = ircerr_noavailableusermodes(user, message);
                        break;
                    case IRCERR_NOAVAILABLECHANNELMODES:
                        retVal = ircerr_noavailablechannelmodes(user, message);
                        break;
                    case IRCERR_NOERRONEUSNICK:
                        retVal = ircerr_noerroneusnick(user, message);
                        break;
                    case IRCERR_NOFAILEDCOMMAND:
                        retVal = ircerr_nofailedcommand(user, message);
                        break;
                    case IRCERR_NOHOST:
                        retVal = ircerr_nohost(user, message);
                        break;
                    case IRCERR_NONICK:
                        retVal = ircerr_nonick(user, message);
                        break;
                    case IRCERR_NONICKORCHANNEL:
                        retVal = ircerr_nonickorchannel(user, message);
                        break;
                    case IRCERR_NOSERVERNAME:
                        retVal = ircerr_noservername(user, message);
                        break;
                    case IRCERR_NOSERVICENAME:
                        retVal = ircerr_noservicename(user, message);
                        break;
                    case IRCERR_NOUSER:
                        retVal = ircerr_nouser(user, message);
                        break;
                    case IRCERR_NOUSERMODESTRING:
                        retVal = ircerr_nousermodestring(user, message);
                        break;
                    case IRCERR_NOVERSION:
                        retVal = ircerr_noversion(user, message);
                        break;
                    case IRCERR_NOPASSWORD:
                        retVal = ircerr_nopassword(user, message);
                        break;
                    case IRCERR_NOMODE:
                        retVal = ircerr_nomode(user, message);
                        break;
                    case IRCERR_NOREALNAME:
                        retVal = ircerr_norealname(user, message);
                        break;
                    case IRCERR_NONAME:
                        retVal = ircerr_noname(user, message);
                        break;
                    case IRCERR_NOSERVICE:
                        retVal = ircerr_noservice(user, message);
                        break;
                    case IRCERR_NOMASKDISTRIBUTION:
                        retVal = ircerr_nomaskdistribution(user, message);
                        break;
                    case IRCERR_NOINFO:
                        retVal = ircerr_noinfo(user, message);
                        break;
                    case IRCERR_NOMESSAGE:
                        retVal = ircerr_nomessage(user, message);
                        break;
                    case IRCERR_NOSERVER:
                        retVal = ircerr_noserver(user, message);
                        break;
                    case IRCERR_NOCHANNEL:
                        retVal = ircerr_nochannel(user, message);
                        break;
                    case IRCERR_NOTARGET:
                        retVal = ircerr_notarget(user, message);
                        break;
                    case IRCERR_NOCOMMAND:
                        retVal = ircerr_nocommand(user, message);
                        break;
                    case IRCERR_UNKNOWNCOMMAND:
                        retVal = ircerr_unknowncommand(user, message);
                        break;
                    case IRCERR_ERRONEUSCOMMAND:
                        retVal = ircerr_erroneuscommand(user, message);
                        break;
                    case IRCERR_NOPASSCOMMAND:
                        retVal = ircerr_nopasscommand(user, message);
                        break;
                    case IRCERR_NONICKCOMMAND:
                        retVal = ircerr_nonickcommand(user, message);
                        break;
                    case IRCERR_NOUSERCOMMAND:
                        retVal = ircerr_nousercommand(user, message);
                        break;
                    case IRCERR_NOOPERCOMMAND:
                        retVal = ircerr_noopercommand(user, message);
                        break;
                    case IRCERR_NOMODECOMMAND:
                        retVal = ircerr_nomodecommand(user, message);
                        break;
                    case IRCERR_NOSERVICECOMMAND:
                        retVal = ircerr_noservicecommand(user, message);
                        break;
                    case IRCERR_NOQUITCOMMAND:
                        retVal = ircerr_noquitcommand(user, message);
                        break;
                    case IRCERR_NOSQUITCOMMAND:
                        retVal = ircerr_nosquitcommand(user, message);
                        break;
                    case IRCERR_NOMASK:
                        retVal = ircerr_nomask(user, message);
                        break;
                    case IRCERR_NOQUERY:
                        retVal = ircerr_noquery(user, message);
                        break;
                    case IRCERR_NOSERVERMASK:
                        retVal = ircerr_noservermask(user, message);
                        break;
                    case IRCERR_NOCOUNT:
                        retVal = ircerr_nocount(user, message);
                        break;
                    case IRCERR_NOOPERATORPARAM:
                        retVal = ircerr_nooperatorparam(user, message);
                        break;
                    case IRCERR_TOOMANYTARGETS:
                        retVal = ircerr_toomanytargets(user, message);
                        break;
                    case IRCERR_NOPRIVILEGES:
                        retVal = ircerr_noprivileges(user, message);
                        break;
                    case IRCERR_TOOMUCHMASK:
                        retVal = ircerr_toomuchmask(user, message);
                        break;
                    case IRCERR_NOPARAMS:
                        retVal = ircerr_noparams(user, message);
                        break;
                    case IRCERR_NOFIXEDMATCH:
                        retVal = ircerr_nofixedmatch(user, message);
                        break;
                    case IRCERR_NOVALIDUSERCOMMAND:
                        retVal = ircerr_novalidusercommand(user, message);
                        break;
                    case IRCERR_NOMODEPARAMS:
                        retVal = ircerr_nomodeparams(user, message);
                        break;
                    case IRCERR_NOEXCEPTIONMASK:
                        retVal = ircerr_noexceptionmask(user, message);
                        break;
                    case IRCERR_NOINVITEMASK:
                        retVal = ircerr_noinvitemask(user, message);
                        break;
                    case IRCERR_NOVISIBLE:
                        retVal = ircerr_novisible(user, message);
                        break;
                    case IRCERR_NOCHANNELNAME:
                        retVal = ircerr_nochannelname(user, message);
                        break;
                    case IRCERR_NOERRCOMMAND:
                        retVal = ircerr_noerrcommand(user, message);
                        break;
                    case IRCERR_NOERRORCODE:
                        retVal = ircerr_noerrorcode(user, message);
                        break;
                    case IRCERR_NOMODECHAR:
                        retVal = ircerr_nomodechar(user, message);
                        break;
                    case IRCERR_NONICKNAME:
                        retVal = ircerr_nonickname(user, message);
                        break;
                    case IRCERR_NOCHARACTER:
                        retVal = ircerr_nocharacter(user, message);
                        break;
                    case IRCERR_NOPORT:
                        retVal = ircerr_noport(user, message);
                        break;
                    case IRCERR_NOFILEERROR:
                        retVal = ircerr_nofileerror(user, message);
                        break;
                    case IRCERR_NOFILE:
                        retVal = ircerr_nofile(user, message);
                        break;
                    case IRCERR_TOOMUCHNICKS:
                        retVal = ircerr_toomuchnicks(user, message);
                        break;
                    case IRCERR_NOCONFIGFILE:
                        retVal = ircerr_noconfigfile(user, message);
                        break;
                    case IRCERR_NOREPLY:
                        retVal = ircerr_noreply(user, message);
                        break;
                    case IRCERR_TOOMUCHREPLIES:
                        retVal = ircerr_toomuchreplies(user, message);
                        break;
                    case IRCERR_NOSERVERINFO:
                        retVal = ircerr_noserverinfo(user, message);
                        break;
                    case IRCERR_NOLINKNAME:
                        retVal = ircerr_nolinkname(user, message);
                        break;
                    case IRCERR_NOHOSTMASK:
                        retVal = ircerr_nohostmask(user, message);
                        break;
                    case IRCERR_NOCLASS:
                        retVal = ircerr_noclass(user, message);
                        break;
                    case IRCERR_NODESTINATION:
                        retVal = ircerr_nodestination(user, message);
                        break;
                    case IRCERR_NONEXTSERVER:
                        retVal = ircerr_nonextserver(user, message);
                        break;
                    case IRCERR_NONEWTYPE:
                        retVal = ircerr_nonewtype(user, message);
                        break;
                    case IRCERR_NOCLIENTNAME:
                        retVal = ircerr_noclientname(user, message);
                        break;
                    case IRCERR_NOCOMPLEXUSER:
                        retVal = ircerr_nocomplexuser(user, message);
                        break;
                    case IRCERR_NOTYPE:
                        retVal = ircerr_notype(user, message);
                        break;
                    case IRCERR_NOACTIVETYPE:
                        retVal = ircerr_noactivetype(user, message);
                        break;
                    case IRCERR_NOAVAILABLECHANNEL:
                        retVal = ircerr_noavailablechannel(user, message);
                        break;
                    case IRCERR_PREFIXUSERWITHOUTHOST:
                        retVal = ircerr_prefixuserwithouthost(user, message);
                        break;
                    case IRCERR_PREFIXNICKWITHOUTHOST:
                        retVal = ircerr_prefixnickwithouthost(user, message);
                        break;
                    case IRCERR_NOPREFIX:
                        retVal = ircerr_noprefix(user, message);
                        break;
                    case IRCERR_NOCHANID:
                        retVal = ircerr_nochanid(user, message);
                        break;
                    case IRCERR_NOCHANIDLENGTH:
                        retVal = ircerr_nochanidlength(user, message);
                        break;
                    case IRCERR_NONICKLENGTH:
                        retVal = ircerr_nonicklength(user, message);
                        break;
                    case IRCERR_IP4OVERIP6:
                        retVal = ircerr_ip4overip6(user, message);
                        break;
                    case IRCERR_NOIP4:
                        retVal = ircerr_noip4(user, message);
                        break;
                    case IRCERR_NOIP:
                        retVal = ircerr_noip(user, message);
                        break;
                    case IRCERR_NOTARGETSERVER:
                        retVal = ircerr_notargetserver(user, message);
                        break;
                    case IRCERR_NOCHANNELORNICK:
                        retVal = ircerr_nochannelornick(user, message);
                        break;
                    case IRCERR_NOSTRING:
                        retVal = ircerr_nostring(user, message);
                        break;
                    case IRCERR_NONUMBER:
                        retVal = ircerr_nonumber(user, message);
                        break;
                    case IRCERR_NODIGITS:
                        retVal = ircerr_nodigits(user, message);
                        break;
                    case IRCERR_NOHEXDIGITS:
                        retVal = ircerr_nohexdigits(user, message);
                        break;
                    case IRCERR_NOSPECIALS:
                        retVal = ircerr_nospecials(user, message);
                        break;
                    case IRCERR_NOSPECIALSUPPERS:
                        retVal = ircerr_nospecialsuppers(user, message);
                        break;
                    case IRCERR_NOSPECIALSLOWERS:
                        retVal = ircerr_nospecialslowers(user, message);
                        break;
                    case IRCERR_NOLETTERS:
                        retVal = ircerr_noletters(user, message);
                        break;
                    case IRCERR_NOUPPERS:
                        retVal = ircerr_nouppers(user, message);
                        break;
                    case IRCERR_NOLOWERS:
                        retVal = ircerr_nolowers(user, message);
                        break;
                    case IRCERR_NOCHANNAMELENGTH:
                        retVal = ircerr_nochannamelength(user, message);
                        break;
                    case IRCERR_NOCHANLENGTH:
                        retVal = ircerr_nochanlength(user, message);
                        break;
                    case IRCERR_NOKEY:
                        retVal = ircerr_nokey(user, message);
                        break;
                    case IRCERR_NOKEYLENGTH:
                        retVal = ircerr_nokeylength(user, message);
                        break;
                    case IRCERR_NOUSERLENGTH:
                        retVal = ircerr_nouserlength(user, message);
                        break;
                    case IRCERR_NOID:
                        retVal = ircerr_noid(user, message);
                        break;
                    case IRCERR_OUTOFLENGTH:
                        retVal = ircerr_outoflength(user, message);
                        break;
                    case IRCERR_PARAMS:
                        retVal = ircerr_params(user, message);
                        break;
                    case IRCERR_INVALIDCHANNELNAME:
                        retVal = ircerr_invalidchannelname(user, message);
                        break;
                    case IRCERR_CHANNELNAMEINUSE:
                        retVal = ircerr_channelnameinuse(user, message);
                        break;
                    case IRCERR_NOENOUGHMEMORY:
                        retVal = ircerr_noenoughmemory(user, message);
                        break;
                    case IRCERR_UNDELETABLECHANNEL:
                        retVal = ircerr_undeletablechannel(user, message);
                        break;
                    case IRCERR_INVALIDNICK:
                        retVal = ircerr_invalidnick(user, message);
                        break;
                    case IRCERR_INVALIDUSER:
                        retVal = ircerr_invaliduser(user, message);
                        break;
                    case IRCERR_INVALIDREALNAME:
                        retVal = ircerr_invalidrealname(user, message);
                        break;
                    case IRCERR_INVALIDUSERNAME:
                        retVal = ircerr_invalidusername(user, message);
                        break;
                    case IRCERR_UNDELETABLEUSER:
                        retVal = ircerr_undeletableuser(user, message);
                        break;
                    case IRCERR_USERUSED:
                        retVal = ircerr_userused(user, message);
                        break;
                    case IRCERR_NICKUSED:
                        retVal = ircerr_nickused(user, message);
                        break;
                    case IRCERR_REALNAMEUSED:
                        retVal = ircerr_realnameused(user, message);
                        break;
                    case IRCERR_NOMUTEX:
                        retVal = ircerr_nomutex(user, message);
                        break;
                    case IRCERR_USERSLIMITEXCEEDED:
                        retVal = ircerr_userslimitexceeded(user, message);
                        break;
                    case IRCERR_NOVALIDCHANNEL:
                        retVal = ircerr_novalidchannel(user, message);
                        break;
                    case IRCERR_NOUSERSONCHANNEL:
                        retVal = ircerr_nousersonchannel(user, message);
                        break;
                    case IRCERR_NOUSERONCHANNEL:
                        retVal = ircerr_nouseronchannel(user, message);
                        break;
                    case IRCERR_NOVALIDUSER:
                        retVal = ircerr_novaliduser(user, message);
                        break;
                    case IRCERR_NAMEINUSE:
                        retVal = ircerr_nameinuse(user, message);
                        break;
                    case IRCERR_FAIL:
                        retVal = ircerr_fail(user, message);
                        break;
                    case IRCERR_INVALIDMASK:
                        retVal = ircerr_invalidmask(user, message);
                        break;
                    case IRCERR_BANEDUSERONCHANNEL:
                        retVal = ircerr_baneduseronchannel(user, message);
                        break;
                    case IRCERR_NOINVITEDUSER:
                        retVal = ircerr_noinviteduser(user, message);
                        break;
                    case IRCERR_YETINCHANNEL:
                        retVal = ircerr_yetinchannel(user, message);
                        break;
                    case IRCERR_INVALIDIP:
                        retVal = ircerr_invalidip(user, message);
                        break;
                    case IRCERR_INVALIDHOST:
                        retVal = ircerr_invalidhost(user, message);
                        break;
                    case IRCERR_NOCONGRUENTNUMBEROFCHANNELS:
                        retVal = ircerr_nocongruentnumberofchannels(user, message);
                        break;
                    case IRCERR_NOCORRECTFORMAT:
                        retVal = ircerr_nocorrectformat(user, message);
                        break;
                    case IRCERR_UNKNOWNERROR:
                        retVal = ircerr_unknownerror(user, message);
                        break;
                    case IRCERR_CANTCHANGETOPIC:
                        retVal = ircerr_cantchangetopic(user, message);
                        break;
                    case IRCERR_COMMANDTOOLONG:
                        retVal = ircerr_commandtoolong(user, message);
                        break;
                    case IRCERR_ISNOTOPERATOR:
                        retVal = ircerr_isnotoperator(user, message);
                        break;
                    case IRCERR_ALREADYREGISTERED:
                        retVal = ircerr_alreadyregistered(user, message);
                        break;
                    case IRCERR_CHANCANNOTSENDTOSOME:
                        retVal = ircerr_chancannotsendtosome(user, message);
                        break;
                    case IRCERR_CHANCANNOTSENDTOMULTIPLE:
                        retVal = ircerr_chancannotsendtomultiple(user, message);
                        break;
                    case IRCERR_INVALIDSOCKET:
                        retVal = ircerr_cannotsend(user, message);
                        break;
                    case IRCERR_CANNOTRECV:
                        retVal = ircerr_cannotrecv(user, message);
                        break;
                    default:
                }
            } else if (errId == FINISHED){
                retVal = FINISHED;
            } else {
                retVal = 0;
            }
        } else {

        }
    } else {

    }
}
