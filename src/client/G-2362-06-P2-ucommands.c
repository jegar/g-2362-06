#include "G-2362-06-P2-ucommands"
#include <netdb.h>
#include <stdio.h>
#include <redes2/ircxchat.h>
#include <redes2/irc.h>

#define SOH 0x01

long ucmd_name(char* command){
    long errCheck;
    char *channel, *targetserver;
    //struct hostent *serverData;
    errCheck = IRCUserParse_Names (command, &channel, &targetserver);

/*
*/
    return errCheck;
}


long ucmd_priv (char* command){
    long errCheck;
    int port, ssl;
    char *message=NULL, *nickorchannel=NULL, *tempDest=NULL, *tempMsg=NULL;
    char *msg=NULL, *dateNick=NULL;
    char *nick2, *user2, *real2, *password2, *server2;

    if (command[0] == '/'){
        errCheck = IRCUserParse_Priv (command, &nickorchannel, &msg);
        if (errCheck == IRC_OK){
            if (!nickorchannel){
                tempDest = IRCInterface_ActiveChannelName();
            } else {
                tempDest = nickorchannel;
            }
            tempMsg = msg;
        }
    } else {
        tempDest = IRCInterface_ActiveChannelName();
        tempMsg = command;
    }

    errCheck = IRCMsg_Privmsg (&message, NULL, tempDest, tempMsg);
    if (errCheck == IRC_OK && tempDest != NULL){
        IRCInterface_GetMyUserInfo (&nick2, &user2, &real2,
                                        &password2, &server2, &port, &ssl);
        dateNick = add_time_format(nick2);
        IRCInterface_WriteChannel(tempDest, dateNick, tempMsg);
        IRCInterface_PlaneRegisterInMessage (message);
        errCheck = send_msg (message);
    }
    IRC_MFree(8, &nickorchannel, &message, &msg,
            &nick2, &user2, &real2, &password2, &server2);
    return errCheck;
}

long ucmd_help (char* command){
    long errCheck;
    char *helpCommand, *dateNick;
    char commandResp[100];

    errCheck = IRCUserParse_Help (command, &helpCommand);
    if (errCheck == IRC_OK){
        dateNick = add_time_format ("-");
        snprintf (commandResp, 100, "%s %s", "Theres no help for", helpCommand);
        IRCInterface_WriteSystem (dateNick, commandResp);
    }
    IRC_MFree(2, &helpCommand, &dateNick);
    return errCheck;
}

long ucmd_list (char* command){
    long errCheck;
    char *channel, *searchstring, *message;

    errCheck = IRCUserParse_List (command, &channel, &searchstring);
    if (errCheck == IRC_OK){
        errCheck = IRCMsg_List (&message, NULL, channel, searchstring);
        if (errCheck == IRC_OK){
            IRCInterface_PlaneRegisterInMessage (message);
            errCheck = send_msg (message);
        }
    }
    IRC_MFree(3, &channel, &searchstring, &message);
    return errCheck;
}

long ucmd_join (char* command){
    long errCheck;
    char *channel=NULL, *password=NULL, *message=NULL, *dateNick=NULL;

    errCheck = IRCUserParse_Join (command, &channel, &password);
    if(errCheck == IRC_OK && channel != NULL && strlen(channel)!=0){
        errCheck = IRCMsg_Join (&message, NULL, channel, password, NULL);
        if (errCheck == IRC_OK){
            IRCInterface_PlaneRegisterInMessage (message);
            errCheck = send_msg (message);
        }
    } else {
        dateNick = add_time_format ("-");
        IRCInterface_WriteSystem (dateNick, "Usage: /JOIN <channel name>");
    }
    IRC_MFree(4, &channel, &password, &message, &dateNick);
    return errCheck;
}

long ucmd_part(char* command){
    long errCheck;
    char *msg=NULL, *message=NULL, *channel=NULL;

    errCheck = IRCUserParse_Part (command, &msg);
    if (errCheck == IRC_OK){
        channel = IRCInterface_ActiveChannelName ();
        errCheck = IRCMsg_Part (&message, NULL, channel, msg);
        if (errCheck == IRC_OK){
            IRCInterface_PlaneRegisterInMessage (message);
            errCheck = send_msg (message);
        }
    }
    IRC_MFree(2, &msg, &message);
    return errCheck;
}

long ucmd_leave(char* command){
    return ucmd_part(command);
}

long ucmd_quit(char* command){
    long errCheck;
    char *reason=NULL, *message=NULL, *tempMsg=NULL;
    char leaving[] = "Leaving";

    errCheck = IRCUserParse_Quit (command, &reason);
    if (errCheck == IRC_OK){
        if (reason) tempMsg = reason;
        else tempMsg = leaving;
        errCheck = IRCMsg_Quit (&message, NULL, tempMsg);
        if (errCheck == IRC_OK){
            IRCInterface_PlaneRegisterInMessage (message);
            errCheck = send_msg (message);
        }
    }
    IRC_MFree(2, &reason, &message);
    return errCheck;
}

long ucmd_nick(char* command){
    long errCheck;
    char *newnick, *message;

    errCheck = IRCUserParse_Nick (command, &newnick);
    if (errCheck == IRC_OK){
        errCheck = IRCMsg_Nick (&message, NULL, newnick, NULL);
        if (errCheck == IRC_OK){
            IRCInterface_PlaneRegisterInMessage (message);
            errCheck = send_msg(message);
        }
    }
    IRC_MFree(2, &newnick, &message);
    return errCheck;
}

long ucmd_away(char* command){
    long errCheck;
    char *reason, *message;

    errCheck = IRCUserParse_Away (command, &reason);
    if (errCheck == IRC_OK){
        errCheck = IRCMsg_Away (&message, NULL, reason);
        if (errCheck == IRC_OK){
            IRCInterface_PlaneRegisterInMessage (message);
            errCheck = send_msg(message);
        }
    }
    IRC_MFree(2, &reason, &message);
    return errCheck;
}

long ucmd_whois(char* command){
    long errCheck;
    char *message, *target, *maskarray;

    errCheck = IRCUserParse_Whois (command, &target);
    if (errCheck == IRC_OK){
        errCheck = IRCMsg_Whois (&message, NULL, target, NULL);
        if (errCheck == IRC_OK){
            IRCInterface_PlaneRegisterInMessage (message);
            errCheck = send_msg (message);
        }
    }
    IRC_MFree(3, &target, &maskarray, &message);
    return errCheck;
}

long ucmd_invite  (char* command){
    long errCheck;
    char *message, *nick, *channel;

    errCheck = IRCUserParse_Invite (command, &nick, &channel);
    if (errCheck == IRC_OK){
        errCheck = IRCMsg_Invite (&message, NULL, nick, channel);
        if (errCheck == IRC_OK){
            IRCInterface_PlaneRegisterInMessage (message);
            errCheck = send_msg (message);
        }
    }
    IRC_MFree(3, &message, &nick, &channel);
    return errCheck;
}

long ucmd_kick(char* command){
    perror(__FUNCTION__);
    return 0;
}

long ucmd_topic(char* command){
    long errCheck;
    char *message, *topic, *channel;

    errCheck = IRCUserParse_Topic (command, &topic);
    if (errCheck == IRC_OK){
        channel = IRCInterface_ActiveChannelName ();
        errCheck = IRCMsg_Topic (&message, NULL, channel, topic);
        if (errCheck == IRC_OK){
            IRCInterface_PlaneRegisterInMessage (message);
            errCheck = send_msg (message);
        }
    }
    IRC_MFree(2, &message, &topic);
    return errCheck;
}

long ucmd_me  (char* command){
    long errCheck;
    char *message, *msg, *channel, *actionchannel;

    errCheck = IRCUserParse_Me (command, &msg);
    if (errCheck == IRC_OK){
        actionchannel = (char*)malloc(sizeof(char) * strlen(msg) + 3);
        if (actionchannel){
            sprintf(actionchannel, "%cACTION %s%c", SOH, msg, SOH);
            channel = IRCInterface_ActiveChannelName ();
            if (channel){
                errCheck = IRCMsg_Privmsg (&message, NULL, channel, msg);
                if (errCheck == IRC_OK){
                    errCheck = send_msg (message);
                }
            }
        }
    }
    IRC_MFree(3, &msg, &message, &actionchannel);
    return errCheck;
}

long ucmd_msg (char* command){
    long errCheck;
    char *message=NULL, *nickorchannel=NULL, *msg=NULL;

    errCheck = IRCUserParse_Msg (command, &nickorchannel, &msg);
    if (errCheck == IRC_OK){
        errCheck = IRCMsg_Privmsg (&message, NULL, nickorchannel, msg);
        if (errCheck == IRC_OK){
            IRCInterface_PlaneRegisterInMessage (message);
            errCheck = send_msg (message);
        }
    }
    IRC_MFree(3, &message, &nickorchannel, &msg);
    return errCheck;
}

long ucmd_query(char* command){
    long errCheck;
    char *message=NULL, *nickorchannel=NULL, *msg=NULL;

    errCheck = IRCUserParse_Query (command, &nickorchannel, &msg);
    if (errCheck == IRC_OK){
        errCheck = IRCMsg_Privmsg (&message, NULL, nickorchannel, msg);
        if (errCheck == IRC_OK){
            IRCInterface_PlaneRegisterInMessage (message);
            errCheck = send_msg (message);
        }
    }
    IRC_MFree(3, &nickorchannel, &message, &msg);
    return errCheck;
}

long ucmd_notice  (char* command){
    long errCheck;
    char *message, *nickchannel, *msg;

    errCheck = IRCUserParse_Notice (command, &nickchannel, &msg);
    if (errCheck == IRC_OK){
        errCheck = IRCMsg_Notice (&message, NULL, nickchannel, msg);
        if (errCheck == IRC_OK){
            IRCInterface_PlaneRegisterInMessage (message);
            errCheck = send_msg (message);
        }
    }
    IRC_MFree(3, &nickchannel, &message, &msg);
    return errCheck;
}

long ucmd_notify  (char* command){
    long errCheck;
    int numnicks, i;
    char **nickarray;

    errCheck = IRCUserParse_Notify (command, &nickarray, &numnicks);
    if (errCheck == IRC_OK){
        for (i = 0 ; i < numnicks; i++){
            perror(nickarray[i]);
        }
        IRCTADUser_FreeList (nickarray, (long)numnicks);
    }
    return errCheck;
}

long ucmd_ignore  (char* command){
    long errCheck;
    int numnicks, i;
    char *message, **nickusers;

    errCheck = IRCUserParse_Ignore (command, &nickusers, &numnicks);
    if (errCheck == IRC_OK){
        for (i = 0; i < numnicks; i++){
            add_to_ignore (nickusers[i]);
        }
        IRCTADUser_FreeList (nickusers, (long)numnicks);
    }
    IRC_MFree(1, &message);
    return errCheck;
}

long ucmd_ping (char* command){
    long errCheck;
    char *message, *user;

    errCheck = IRCUserParse_Ping (command, &user);
    if (errCheck == IRC_OK){
        errCheck = IRCMsg_Ping (&message, NULL, get_server_name (), NULL);
        if (errCheck == IRC_OK){
            IRCInterface_PlaneRegisterInMessage (message);
            errCheck = send_msg (message);
        }
    }
    IRC_MFree(2, &user, &message);
    return errCheck;
}

long ucmd_who (char* command){
    long errCheck;
    char *message, *mask;

    errCheck = IRCUserParse_Who (command, &mask);
    if (errCheck == IRC_OK){
        errCheck = IRCMsg_Who (&message, NULL, mask, NULL);
        if (errCheck == IRC_OK){
            IRCInterface_PlaneRegisterInMessage (message);
            errCheck = send_msg (message);
        }
    }
    IRC_MFree(2, &mask, &message);
    return errCheck;
}

long ucmd_whowas  (char* command){
    perror (__FUNCTION__);
    return 0;
}

long ucmd_ison (char* command){
    perror (__FUNCTION__);
    return 0;
}

long ucmd_cycle (char* command){
    perror (__FUNCTION__);
    return 0;
}

long ucmd_motd(char* command){
    long errCheck;
    char *message, *server;

    errCheck = IRCUserParse_Motd (command, &server);
    if (errCheck == IRC_OK){
        errCheck = IRCMsg_Motd (&message, NULL, server);
        if (errCheck == IRC_OK){
            IRCInterface_PlaneRegisterInMessage (message);
            errCheck = send_msg (message);
        }
    }
    IRC_MFree(2, &server, &message);
    return errCheck;
}

long ucmd_rule(char* command){
    perror(__FUNCTION__);
    return 0;
}

long ucmd_lusers  (char* command){
    perror(__FUNCTION__);
    return 0;
}

long ucmd_version (char* command){
    perror(__FUNCTION__);
    return 0;
}

long ucmd_admi(char* command){
    perror(__FUNCTION__);
    return 0;
}

long ucmd_userhost(char* command){
    perror(__FUNCTION__);
    return 0;
}

long ucmd_knock(char* command){
    perror(__FUNCTION__);
    return 0;
}

long ucmd_vhost (char* command){
    perror(__FUNCTION__);
    return 0;
}

long ucmd_mode (char* command){
    long errCheck;
    char *message, *filter, *mode, *channel;

    errCheck = IRCUserParse_Mode (command, &filter, &mode);
    if (errCheck == IRC_OK){
        channel = IRCInterface_ActiveChannelName ();
        errCheck = IRCMsg_Mode (&message, NULL, channel, mode, NULL);
        if (errCheck == IRC_OK){
            IRCInterface_PlaneRegisterInMessage (message);
            errCheck = send_msg (message);
        }
    }
    IRC_MFree(3, &message, &filter, &mode);
    return errCheck;
}

long ucmd_time(char* command){
    perror(__FUNCTION__);
    return 0;
}

long ucmd_botmotd (char* command){
    perror(__FUNCTION__);
    return 0;
}

long ucmd_identify(char* command){
    perror(__FUNCTION__);
    return 0;
}

long ucmd_dns (char* command){
    long errCheck;
    char *option, *channel;
    struct hostent *server;

    errCheck = IRCUserParse_Dns (command, &option);
    if (errCheck == IRC_OK){
        server = gethostbyname(option);
        channel = IRCInterface_ActiveChannelName();
        IRCInterface_WriteChannel (channel, "*", server->h_name);
    }
    IRC_MFree(1, &option);
    return errCheck;
}

long ucmd_userip  (char* command){
    perror(__FUNCTION__);
    return 0;
}

long ucmd_stats(char* command){
    perror(__FUNCTION__);
    return 0;
}

long ucmd_ctcp(char* command){
    perror(__FUNCTION__);
    return 0;
}

long ucmd_dcc (char* command){
    perror(__FUNCTION__);
    return 0;
}

long ucmd_map (char* command){
    return 0;
}

long ucmd_link(char* command){
    return 0;
}

long ucmd_setname (char* command){
    return 0;
}

long ucmd_license (char* command){
    return 0;
}

long ucmd_module  (char* command){
    return 0;
}

long ucmd_partall (char* command){
    return 0;
}

long ucmd_chat(char* command){
    perror(__FUNCTION__);
    return 0;
}

long ucmd_back(char* command){
    return 0;
}

long ucmd_unaway  (char* command){
    return 0;
}

long ucmd_close(char* command){
    return 0;
}

long ucmd_oper(char* command){
    return 0;
}

long ucmd_fsen(char* command){
    return 0;
}

long ucmd_faccept (char* command){
    return 0;
}

long ucmd_fclose  (char* command){
    return 0;
}
