#include <redes2/ircxchat.h>
#include <redes2/irc.h>
#include <sys/time.h>

#include <string.h>

#include "G-2362-06-P1-tcp.h"
#include "G-2362-06-P2-clientinfo.h"
#include "G-2362-06-P2-clireply.h"
#include "G-2362-06-P2-clierror"

#define NUM_CONNECT_COMM    3
#define RECV_TIMEOUT        500000
typedef long (*command_recieved) (char*);

long IRCInterface_Connect_send (char *nick, char *user, char *realname,
                        char *password, char *server, int sockfd, boolean ssl);


void reception_and_parse (){
    static command_recieved commandFun [] = {
        comrpl_pass, comrpl_nick, comrpl_user, comrpl_oper, comrpl_mode,
        comrpl_service, comrpl_quit, comrpl_squit, comrpl_join, comrpl_part,
        comrpl_topic, comrpl_names, comrpl_list, comrpl_invite, comrpl_kick,
        comrpl_privmsg, comrpl_notice, comrpl_motd, comrpl_lusers,
        comrpl_version, comrpl_stats, comrpl_links, comrpl_time, comrpl_connect,
        comrpl_trace, comrpl_admin, comrpl_info, comrpl_servlist, comrpl_squery,
        comrpl_who, comrpl_whois, comrpl_whowas, comrpl_kill, comrpl_ping,
        comrpl_pong, comrpl_error, comrpl_away, comrpl_rehash, comrpl_die,
        comrpl_restart, comrpl_summon, comrpl_users, comrpl_wallops,
        comrpl_userhost, comrpl_ison, comrpl_help, comrpl_rules, comrpl_server,
        comrpl_encap, comrpl_cnotice, comrpl_cprivmsg, comrpl_namesx,
        comrpl_silence, comrpl_uhnames, comrpl_watch,
        comrpl_knock, comrpl_userip, comrpl_setname, /*58*/

        error_needmoreparams, error_alreadyregistred, error_nonicknamegiven,
        error_erroneusnickname, error_nicknameinuse, error_nickcollision,
        error_unavailresource, error_restricted, reply_youreoper,
        error_nooperhost, error_passwdmismatch, reply_umodeis,
        error_umodeunknownflag, error_usersdontmatch, reply_youreservice,
        reply_yourhost, reply_myinfo, error_noprivileges, error_nosuchserver,
        reply_endofwho, reply_endofwhois, reply_endofwhowas, error_wasnosuchnick,
        reply_whowasuser, reply_whoisuser, reply_whoischannels,
        reply_whoisoperator, reply_whoisserver, reply_whoisidle,
        reply_whoreply, error_badmask, error_cannotsendtochan, error_notexttosend,
        error_notoplevel, error_wildtoplevel, error_badchanmask,
        error_badchannelkey, reply_banlist, error_bannedfromchan,
        error_channelisfull, reply_channelmodeis, error_chanoprivsneeded,
        reply_endofbanlist, reply_endofexceptlist, reply_endofinvitelist,
        reply_endofnames, reply_exceptlist, reply_invitelist,
        error_inviteonlychan, reply_inviting, error_keyset, reply_liststart, /*52*/
        reply_list, reply_listend, reply_namreply, error_nochanmodes,
        error_nosuchchannel, error_notonchannel, reply_notopic,
        error_toomanychannels, error_toomanytargets, error_unknownmode,
        error_usernotinchannel, error_useronchannel, reply_uniqopis, reply_topic,
        reply_adminme, reply_adminloc1, reply_adminloc2, reply_adminemail, reply_info,
        reply_endoflinks, reply_endofinfo, reply_endofmotd, reply_endofstats,
        reply_links, reply_luserchannels, reply_luserclient, reply_luserme,
        reply_luserop, reply_luserunknown, reply_motd, reply_motdstart,
        error_nomotd, reply_statscommands, reply_statslinkinfo, reply_statsoline,
        reply_statsuptime, reply_time, reply_traceclass, reply_traceconnect, /*39*/
        reply_traceconnecting, reply_tracehandshake, reply_tracelink,
        reply_tracenewtype, reply_traceoperator, reply_traceserver,
        reply_traceservice, reply_traceuser, reply_traceunknown, reply_tracelog,
        reply_traceend, reply_version, error_nosuchservice, reply_servlist,
        reply_servlistend, error_cantkillserver, error_noorigin, reply_endofusers,
        error_fileerror, reply_ison, error_nologin, reply_nousers, reply_nowaway,
        reply_rehashing, error_summondisabled, reply_summoning, reply_unaway,
        reply_userhost, reply_users, error_usersdisabled, reply_usersstart, /*31 -> 180*/
        reply_away, error_nosuchnick, reply_welcome, reply_created, reply_bounce,
        reply_tryagain, error_unknowncommand, error_noadmininfo,
        error_notregistered, error_nopermforhost, error_yourebannedcreep,
        error_youwillbebanned, error_banlistfull, error_uniqopprivsneeded,
        error_norecipient, error_toomanymatches, reply_yourid,
        reply_creationtime, reply_localusers, reply_globalusers,
        reply_topicwhotime, reply_channelurl
    };
    char recvStr[TCP_SGMNT_MAX_LEN];
    char *next_command, *command;
    long commNum, errCheck;
    int numRead;

    do {
        numRead = tcp_client_recieve (get_socket_fd(), recvStr, TCP_SGMNT_MAX_LEN);
        if (numRead > 0){
            next_command = recvStr;
            do {
                next_command = IRC_UnPipelineCommands(next_command, &command);
                if (command){
                    IRCInterface_PlaneRegisterOutMessageThread (command);
                    commNum = IRC_CommandQuery (command);
                    if (commNum >= PASS && commNum <= RPL_CHANNELURL){
                        errCheck = commandFun[commNum-1](command);
                    } else {
                        errCheck = commNum;
                    }
                    IRC_perror("Command Query Error.", errCheck);
                    free(command);
                }
            } while (next_command!= NULL);
        }
    } while (numRead != ERR && recep_thread_check());
    pthread_exit(NULL);
}

/**
 * @ingroup IRCInterfaceCallbacks
 *
 * @page IRCInterface_Connect IRCInterface_Connect
 *
 * @brief Llamada por los distintos botones de conexión.
 *
 * @synopsis
 * @code
 *	#include <redes2/ircxchat.h>
 *
 * 	long IRCInterface_Connect (char *nick, char * user, char * realname, char * password, char * server, int port, boolean ssl)
 * @endcode
 *
 * @description
 * Función a implementar por el programador.
 * Llamada por los distintos botones de conexión. Si implementará la comunicación completa, incluido
 * el registro del usuario en el servidor.
 *
 * En cualquier caso sólo se puede realizar si el servidor acepta la orden.
 * Las strings recibidas no deben ser manipuladas por el programador, sólo leída.
 *
 *
 * @param[in] nick nick con el que se va a realizar la conexíón.
 * @param[in] user usuario con el que se va a realizar la conexión.
 * @param[in] realname nombre real con el que se va a realizar la conexión.
 * @param[in] password password del usuario si es necesaria, puede valer NULL.
 * @param[in] server nombre o ip del servidor con el que se va a realizar la conexión.
 * @param[in] port puerto del servidor con el que se va a realizar la conexión.
 * @param[in] ssl puede ser TRUE si la conexión tiene que ser segura y FALSE si no es así.
 *
 * @retval IRC_OK si todo ha sido correcto (debe devolverlo).
 * @retval IRCerror_NOSSL si el valor de SSL es TRUE y no se puede activar la conexión SSL pero sí una
 * conexión no protegida (debe devolverlo).
 * @retval IRCERR_NOCONNECT en caso de que no se pueda realizar la comunicación (debe devolverlo).
 *
 * @warning Esta función debe ser implementada por el alumno.
 *
 * @author
 * Eloy Anguiano (eloy.anguiano@uam.es)
 *
 *<hr>
*/

long IRCInterface_Connect (char *nick, char *user, char *realname,
                            char *password, char *server, int port, boolean ssl)
{
    long errCheck = IRCERR_NOCONNECT;
    int sockfd, errThr;
    struct timeval tv;

    tv.tv_sec = 0;
    tv.tv_usec = RECV_TIMEOUT;

    if (nick && user && realname && server && port){
        sockfd = tcp_client_connection(server, port, &tv);
        if (sockfd > 0){
            errCheck = IRCInterface_Connect_send (nick, user, realname,
                                                password, server, sockfd, ssl);
            if (errCheck == IRC_OK){
                start_client_info();
                set_socket_fd (sockfd);
                errThr = recep_thread_start ((void*)reception_and_parse);
                if (errThr != 0){
                    errCheck = IRCERR_NOCONNECT;
                }
            }
        }
    }
	return (errCheck == IRC_OK)? IRC_OK: IRCERR_NOCONNECT;
}

long IRCInterface_Connect_send (char *nick, char *user, char *realname,
                        char *password, char *server, int sockfd, boolean ssl)
{
    long mulErrChck[NUM_CONNECT_COMM];
    long errCheck = IRC_OK;
    char *cmds[NUM_CONNECT_COMM] = {NULL};
    char *message;
    int i, count, tcpErr;

    count = 0;
    if (strcmp(password, "")){
        mulErrChck[count] = IRCMsg_Pass (&cmds[count], NULL, password);
        count++;
    }
    mulErrChck[count] = IRCMsg_Nick (&cmds[count], NULL, nick, NULL);
    count++;
    mulErrChck[count] = IRCMsg_User (&cmds[count], NULL, user, "0", realname);
    count++;

    for (i = 0; i < count; i++){
        if (mulErrChck[i] != IRC_OK){
            errCheck = IRCERR_NOCONNECT;
            break;
        }
    }
    if (errCheck == IRC_OK){
        errCheck = IRC_PipelineCommands (&message, cmds[0], cmds[1], cmds[2], NULL);
        if (errCheck == IRC_OK){
            tcpErr = tcp_client_send(sockfd, message, strlen(message));
            errCheck = (tcpErr == TCP_OK)? IRC_OK : IRCERR_NOCONNECT;
            free(message);
        }
    }

    return errCheck;
}


/**
 * @ingroup IRCInterfaceCallbacks
 *
 * @page IRCInterface_DisconnectServer IRCInterface_DisconnectServer
 *
 * @brief Llamada por los distintos botones de desconexión.
 *
 * @synopsis
 * @code
 *	#include <redes2/ircxchat.h>
 *
 * 	boolean IRCInterface_DisconnectServer (char * server, int port)
 * @endcode
 *
 * @description
 * Llamada por los distintos botones de desconexión. Debe cerrar la conexión con el servidor.
 *
 * En cualquier caso sólo se puede realizar si el servidor acepta la orden.
 * La string recibida no debe ser manipulada por el programador, sólo leída.

 * @param[in] server nombre o ip del servidor del que se va a realizar la desconexión.
 * @param[in] port puerto sobre el que se va a realizar la desconexión.
 *
 * @retval TRUE si se ha cerrado la conexión (debe devolverlo).
 * @retval FALSE en caso contrario (debe devolverlo).
 *
 * @warning Esta función debe ser implementada por el alumno.
 *
 * @author
 * Eloy Anguiano (eloy.anguiano@uam.es)
 *
 *<hr>
*/

boolean IRCInterface_DisconnectServer(char *server, int port)
{
    char *prefix=NULL, *nick=NULL, *message=NULL;
    char *dateNick, *command;

    IRCMsg_Quit (&command, NULL, "Leaving");
    recep_thread_join ();
    tcp_client_close (get_socket_fd());
    dateNick = add_time_format ("*");
    IRCInterface_WriteSystem (dateNick, "Disconnect ()");
    IRC_MFree(4, &prefix, &nick, &message, &dateNick);

	return TRUE;
}
