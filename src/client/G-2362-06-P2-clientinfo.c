#include "G-2362-06-P2-clientinfo"
#include <pthread.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <redes2/irctad.h>
#include <redes2/ircxchat.h>

#define NUM_LETTERS 26

struct IRC_client_info {
    char                server_name [SERVER_NAME_LEN + 1];
    pthread_t           rcep_thread;
    unsigned char       thread_stop;
    int                 socket_fd;
    int                 aux_counter;
    char                joinName[CHAN_NAM_LEN];
    Mask*               ignorelist;
};

static struct IRC_client_info clientInfo;

void start_client_info(){
    memset(clientInfo.server_name, 0, SERVER_NAME_LEN+1);
    memset(clientInfo.joinName, 0, CHAN_NAM_LEN);
    clientInfo.rcep_thread = -1;
    clientInfo.thread_stop = 0;
    clientInfo.socket_fd = -1;
    clientInfo.aux_counter = 0;
    clientInfo.ignorelist = NULL;
}

void set_server_name (char* nick){
    strncpy(clientInfo.server_name, nick, SERVER_NAME_LEN);
}

char* get_server_name (){
    return clientInfo.server_name;
}

void set_socket_fd (int fd){
    clientInfo.socket_fd = fd;
}

int get_socket_fd (){
    return clientInfo.socket_fd;
}

int recep_thread_start (void* recep_funct){
    pthread_t threadId;
    int errThread;

    clientInfo.rcep_thread = -1;
    clientInfo.thread_stop = 1;

    errThread = pthread_create (&threadId, NULL, (void*)recep_funct, NULL);
    if (errThread == 0){
        clientInfo.rcep_thread = threadId;
    }
    return errThread;
}

int recep_thread_check () {
    return clientInfo.thread_stop;
}

void recep_thread_stop_own (){
    clientInfo.thread_stop = 0;
}

void recep_thread_stop (){
    pthread_exit(NULL);
}

int recep_thread_join () {
    int result = -1;

    if (clientInfo.rcep_thread > 0){
        clientInfo.thread_stop = 0;
        result = pthread_join(clientInfo.rcep_thread, NULL);
    }
    return result;
}

char* add_time_format (char* nick){
    char* dateNick = NULL;
    char* timestamp = NULL;

    dateNick = (char*)malloc(sizeof(char*) * NICKSPACE);
    if (dateNick){
        timestamp = IRCTAD_TimestampToLocalDateFormat (time(NULL), "[%I:%M:%S]");
        if (timestamp){
            snprintf(dateNick, NICKSPACE, "%*s%*s", TIMESTAMP_LEN, timestamp,
                        (IRC_NICK_MAX_LEN + NICK_WHITESPACES), nick);
            free(timestamp);
        }
    }
    return dateNick;
}

int add_to_ignore (char* nick){
    int retVal = ERR;
    Mask *currentMask, *nextMask;

    if (nick){
        nextMask = clientInfo.ignorelist;
        while (nextMask != NULL && retVal != OK){
            currentMask = nextMask;
            if (!strcmp(currentMask->mask, nick)){
                retVal = OK;
            } else {
                nextMask = currentMask->Next;
            }
        }
        if (nextMask == NULL){
            nextMask = (Mask*)malloc(sizeof(Mask));
            if (nextMask){
                nextMask->mask = (char*)malloc(sizeof(char)*(strlen(nick) + 1));
                if (nextMask->mask){
                    nextMask->Next = NULL;
                    strcpy(nextMask->mask, nick);
                    currentMask->Next = nextMask;
                    retVal = OK;
                }
            }
        }
    }
    return retVal;
}

int check_ignore (char* nick){
    int retVal = 0;
    Mask *currentMask;

    if (nick){
        currentMask = clientInfo.ignorelist;
        while (currentMask != NULL && !retVal){
            if (!strcmp(currentMask->mask, nick)){
                retVal = 1;
            } else {
                currentMask = currentMask->Next;
            }
        }
    }
    return retVal;
}

void remove_ignore (char* nick){
    int retVal = ERR;
    Mask *currentMask, *nextMask;

    if (nick){
        nextMask = clientInfo.ignorelist;
        while (nextMask != NULL && retVal != OK){
            if (!strcmp(nextMask->mask, nick)){
                nextMask = nextMask->Next;
                if (nextMask == clientInfo.ignorelist){
                    free(clientInfo.ignorelist->mask);
                    free(clientInfo.ignorelist);
                    clientInfo.ignorelist = nextMask;
                } else {
                    free(currentMask->mask);
                    free(currentMask);
                    currentMask->Next = nextMask;
                }
                retVal = OK;
            } else {
                currentMask = nextMask;
                nextMask = nextMask->Next;
            }
        }
    }
}


long send_msg (char* message){
    int tcpErr;

    fprintf(stderr, "Send Msg %s\n", message);
    tcpErr = tcp_client_send(get_socket_fd(), message, strlen(message) + 1);
    if (tcpErr == IRC_OK)
        return IRC_OK;
    else
        return IRCERR_INVALIDSOCKET;
}


long check_complex_prefix (char* prefix){
    long errCheck;
    int port, ssl;
    char *nick, *user, *real, *password, *host2;
    char *nickname, *username, *host, *server;

    IRCInterface_GetMyUserInfoThread (&nick, &user, &real,
                                    &password, &host2, &port, &ssl);
    IRCParse_ComplexUser (prefix, &nickname, &username, &host, &server);

    if (nick && nickname){
        errCheck = strcmp(nick, nickname);
        if (user && username && !errCheck){
            errCheck = strcmp(username, user);
            if (host && host2 && !errCheck){
                errCheck = strcmp(host, host2);
            }
        }
    } else {
        errCheck = ERR;
    }

    IRC_MFree (9, &nick, &user, &real, &password,
                &host2, &nickname, &username, &host, &server);
    return errCheck;
}
