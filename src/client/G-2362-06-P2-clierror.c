#include "G-2362-06-P2-clierror"
#include <stdio.h>

long error_needmoreparams       (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_alreadyregistred     (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_nonicknamegiven      (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_erroneusnickname     (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_nicknameinuse        (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_nickcollision        (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_unavailresource      (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_restricted           (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_nooperhost           (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_passwdmismatch       (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_umodeunknownflag     (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_usersdontmatch       (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_noprivileges         (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_nosuchserver         (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_wasnosuchnick        (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_badmask              (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_cannotsendtochan     (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_notexttosend         (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_notoplevel           (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_wildtoplevel         (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_badchanmask          (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_badchannelkey        (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_bannedfromchan       (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_channelisfull        (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_chanoprivsneeded     (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_inviteonlychan       (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_keyset               (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_nochanmodes          (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_nosuchchannel        (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_toomanychannels      (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_toomanytargets       (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_unknownmode          (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_usernotinchannel     (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_useronchannel        (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_nomotd               (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_nosuchservice        (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_cantkillserver       (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_noorigin             (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_fileerror            (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_nologin              (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_summondisabled       (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_usersdisabled        (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_nosuchnick           (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_unknowncommand       (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_noadmininfo          (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_notregistered        (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_nopermforhost        (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_yourebannedcreep     (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_youwillbebanned      (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_banlistfull          (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_uniqopprivsneeded    (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_norecipient          (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_toomanymatches       (char* msg){
    perror(__FUNCTION__);
    return 0;
}
long error_notonchannel         (char* msg){
    perror(__FUNCTION__);
    return 0;
}
