#include "G-2362-06-P2-clireply"
#include <stdio.h>
#include <redes2/irc.h>
#include <redes2/ircxchat.h>

#define SERVERNICK "*"

typedef long (*parse3char) (char *, char**, char**, char**);
typedef long (*parse4char) (char *, char**, char**, char**, char**);

long parse_pref_nick_msg_to_system (parse3char parseFunc, char* msg, char* shownick){
    long errCheck;
    char *prefix=NULL, *nick=NULL, *message=NULL;
    char *dateNick;

    errCheck = parseFunc (msg, &prefix, &nick, &message);
    if (errCheck == IRC_OK){
        dateNick = add_time_format (shownick);
        IRCInterface_WriteSystemThread (dateNick, message);
        free(dateNick);
    }
    IRC_MFree(3, &prefix, &nick, &message);
    return errCheck;
}

long parse_pref_nick_msg_to_active (parse3char parseFunc, char* msg, char* shownick){
    long errCheck;
    char *prefix=NULL, *nick=NULL, *message=NULL;
    char *dateNick, *channel;

    errCheck = parseFunc (msg, &prefix, &nick, &message);
    if (errCheck == IRC_OK){
        dateNick = add_time_format (shownick);
        channel = IRCInterface_ActiveChannelNameThread ();
        IRCInterface_WriteChannelThread (channel, dateNick, message);
        free(dateNick);
    }
    IRC_MFree(4, &prefix, &nick, &message, &dateNick, &channel);
    return errCheck;
}

long parse_pref_nick_name_msg_to_active (parse4char parseFunc, char* msg, char* shownick){
    long errCheck;
    char *prefix=NULL, *nick=NULL, *message=NULL, *name=NULL;
    char *dateNick, *channel;

    errCheck = parseFunc (msg, &prefix, &nick, &name ,&message);
    if (errCheck == IRC_OK){
        dateNick = add_time_format (shownick);
        channel = IRCInterface_ActiveChannelNameThread ();
        IRCInterface_WriteChannelThread (channel, dateNick, message);
        IRCInterface_WriteChannelThread (channel, dateNick, name);
        free(dateNick);
    }
    IRC_MFree(4, &prefix, &nick, &name, &message, &dateNick, &channel);
    return errCheck;
}

long comrpl_join (char* msg){
    long errCheck;
    int port, ssl;
    char *prefix=NULL, *channel=NULL, *key=NULL, *message=NULL;
    char *modeMsg=NULL, *whoMsg=NULL;
    char *nick=NULL, *user=NULL, *real=NULL, *password=NULL, *host=NULL;
    char *nickname=NULL, *username=NULL, *host2=NULL, *server=NULL;

    errCheck = IRCParse_Join (msg, &prefix, &channel, &key, &message);
    if (errCheck == IRC_OK){
        IRCInterface_GetMyUserInfoThread (&nick, &user, &real,
                                    &password, &host, &port, &ssl);
        IRCParse_ComplexUser (prefix, &nickname, &username, &host2, &server);
        if (nickname && username && server){
            if ((nick && !strcmp(nick, nickname)) &&
                (user && !strcmp(user, username)) &&
                (host && !strcmp(host, server)) &&
                real)
            {
                errCheck = IRCMsg_Mode (&modeMsg, NULL, message, NULL, NULL);
                if (errCheck == IRC_OK){
                    send_msg (modeMsg);
                    errCheck = IRCMsg_Who (&whoMsg, NULL, NULL, NULL);
                    if (errCheck == IRC_OK){
                        send_msg (whoMsg);
                    }
                }
                if (message){
                    IRCInterface_AddNewChannelThread (message, 0);
                }
            } else {
                IRCInterface_AddNickChannelThread (message, nickname,
                                            username, "", host2, NONE);
            }
        }
    }
    IRC_MFree(15, &prefix, &channel, &key, &modeMsg, &message,
            &whoMsg, &nick, &user, &real, &password, &host,
            &nickname, &username, &server, &server);
    return errCheck;
}


long reply_welcome (char* msg){
    return parse_pref_nick_msg_to_system (IRCParse_RplWelcome, msg, SERVERNICK);
}

long reply_umodeis (char* msg){
    return parse_pref_nick_msg_to_active (IRCParse_RplUModeIs, msg, SERVERNICK);
}

long reply_endofwhois (char* msg){
    return parse_pref_nick_name_msg_to_active (IRCParse_RplEndOfWhoIs , msg, SERVERNICK);
}

long comrpl_part (char* msg){
    long errCheck;
    int port, ssl;
    char *prefix=NULL, *channel=NULL, *message=NULL;
    char *nick=NULL, *user=NULL, *real=NULL, *pass=NULL;
    char *nickname=NULL, *username=NULL, *host=NULL, *host2=NULL, *server=NULL;

    errCheck = IRCParse_Part (msg, &prefix, &channel, &message);
    IRCInterface_GetMyUserInfoThread (&nick, &user, &real,
                                    &pass, &host, &port, &ssl);
    IRCParse_ComplexUser (prefix, &nickname, &username, &host2, &server);

    if (errCheck == IRC_OK && channel && strlen(channel) > 0){
        if (nick && user && real && host){
            if ((nickname && !strcmp(nickname, nick)) &&
                (username && !strcmp(username, user)) &&
                (host2 && !strcmp(host, host2)))
            {
                IRCInterface_RemoveChannelThread (channel);
            } else {
                IRCInterface_DeleteNickChannelThread (channel, nickname);
            }
        }
    }
    IRC_MFree(12, &prefix, &channel, &message, &nick, &user, &real, &pass,
            &nickname, &username, &host, &host2, &server);
    return errCheck;
}

long reply_whoreply (char* msg){
    long errCheck;
    char *prefix, *nick, *channel, *user, *realname, *real, *password;
    char *host, *nick2, *type, *message, *user2, *server, *server2;
    int hopcount, port, ssl;
    nickstate state;

    errCheck = IRCParse_RplWhoReply (msg, &prefix, &nick, &channel, &user, &host,
                        &server, &nick2, &type, &message, &hopcount, &realname);

    IRCInterface_GetMyUserInfoThread (&nick2, &user2, &real,
                                    &password, &server2, &port, &ssl);
    if (strcmp(nick, nick2)){
        if (errCheck == IRC_OK){
            if (nick[0] == '@') state = OPERATOR;
            else if (nick[0] == '+') state = VOICE;
            else state = NONE;
            IRCInterface_AddNickChannelThread (channel, nick,
                                            user, real, host, state);
        }
    }
    IRC_MFree(16, &prefix, &nick, &channel, &user, &host, &server, &server2,
                &type, &message, &realname, &nick2, &user2, &real, &password);
    return errCheck;
}

long reply_banlist (char* msg){
    perror(msg);
    return 0;
}

long reply_channelmodeis (char* msg){
    long errCheck, mode;
    char *prefix, *nick, *channel, *modetxt;

    errCheck = IRCParse_RplChannelModeIs (msg, &prefix,
                                        &nick, &channel, &modetxt);
    if (errCheck == IRC_OK){
        mode = IRCInterface_ModeToIntMode (modetxt);
        IRCInterface_SetModeChannelThread (channel, mode);
    }
    IRC_MFree(4, &prefix, &nick, &channel, &modetxt);
    return errCheck;
}

long reply_motd (char* msg){
    return parse_pref_nick_msg_to_system (IRCParse_RplMotd, msg, SERVERNICK);
}

long reply_motdstart (char* msg){

    long errCheck;
    char *prefix=NULL, *nick=NULL, *message=NULL, *server = NULL;
    char *dateNick;

    errCheck = IRCParse_RplMotdStart (msg, &prefix, &nick, &message, &server);
    if (errCheck == IRC_OK){
        dateNick = add_time_format (SERVERNICK);
        IRCInterface_WriteSystemThread (dateNick, message);
        free(dateNick);
    }
    IRC_MFree(4, &prefix, &nick, &message, &server);
    return errCheck;
}
long reply_namreply (char* msg){
    long errCheck;
    char *prefix=NULL, *nick=NULL, *type=NULL, *channel=NULL, *message=NULL;
    char *nick2=NULL, *user=NULL, *real=NULL, *password=NULL, *server=NULL;
    int port, ssl;
    nickstate state;

    errCheck = IRCParse_RplNamReply (msg, &prefix, &nick, &type, &channel, &message);
    IRCInterface_GetMyUserInfoThread (&nick2, &user, &real,
                                    &password, &server, &port, &ssl);
    if (errCheck == IRC_OK){
        if (message[0] == '@') state = OPERATOR;
        else if (message[0] == '+') state = VOICE;
        else state = NONE;
    }
    if (strcmp(message, nick2)){
        IRCInterface_AddNickChannelThread (channel, message, "", "", "", state);
    } else {
        IRCInterface_AddNickChannelThread (channel, message, user, real, server, state);
    }
    IRC_MFree(10, &prefix, &nick, &type, &channel, &message,
                &nick2, &user, &real, &password, &server);
    return errCheck;
}

long reply_topic (char* msg){
    long errCheck;
    char *prefix=NULL, *nick=NULL, *channel=NULL, *topic=NULL;
    char *modeMsg=NULL, *whoMsg=NULL, *password=NULL;
    UserInt user;

    errCheck = IRCParse_RplTopic (msg, &prefix, &nick, &channel, &topic);
    if (errCheck == IRC_OK){
        errCheck = IRCMsg_Mode (&modeMsg, NULL, channel, NULL, NULL);
        if (errCheck == IRC_OK){
            send_msg (modeMsg);
            errCheck = IRCMsg_Who (&whoMsg, NULL, NULL, NULL);
            if (errCheck == IRC_OK){
                send_msg (whoMsg);
            }
        }
        IRCInterface_SetTopicThread (topic);
    }
    IRC_MFree(11 ,&prefix, &nick, &channel, &topic, &modeMsg, &whoMsg,
            &user.nick, &user.user, &user.real, &password, &user.host);
    return errCheck;
}

long comrpl_privmsg (char* msg){
    long errCheck;
    char *prefix=NULL, *nickorchannel=NULL, *message=NULL, *dateNick=NULL;
    char *nickname=NULL, *username=NULL, *host=NULL, *server=NULL;

    errCheck = IRCParse_Privmsg (msg, &prefix, &nickorchannel, &message);
    if (errCheck == IRC_OK){
        errCheck = IRCParse_ComplexUser (prefix, &nickname, &username, &host, &server);
        if (errCheck == IRC_OK){
            if ((nickorchannel[0] != '&' || nickorchannel[0] != '#')
                && IRCInterface_QueryChannelExist (nickorchannel) == FALSE){
                IRCInterface_AddNewChannel (nickorchannel, 0);
            }
            if (IRCInterface_QueryChannelExist (nickorchannel) == TRUE){
                dateNick = add_time_format (nickname);
                IRCInterface_WriteChannelThread (nickorchannel, dateNick, message);
            }
        }
    }
    IRC_MFree(8, &prefix, &nickorchannel, &message,
            &dateNick, &nickname, &username, &host, &server);
    return errCheck;
}

long comrpl_quit (char* msg){
    long errCheck;
    int ssl, port;
    char *prefix=NULL, *message=NULL;
    char *nick=NULL, *user=NULL, *real=NULL, *pass=NULL, *host=NULL;
    char *dateNick=NULL, *nickname=NULL, *username=NULL, *host2=NULL, *server=NULL;

    errCheck = IRCParse_Quit (msg, &prefix, &message);
    if (errCheck == IRC_OK){
        IRCInterface_GetMyUserInfoThread (&nick, &user, &real,
                                    &pass, &host, &port, &ssl);
        IRCParse_ComplexUser (prefix, &nickname, &username, &host2, &server);
        if (nickname && username && server){
            if ((nick && !strcmp(nick, nickname)) &&
                (user && !strcmp(user, username)) &&
                (host && !strcmp(host, server)) &&
                real)
            {
                recep_thread_stop_own ();
                dateNick = add_time_format ("*");
                tcp_client_close (get_socket_fd());
                IRCInterface_WriteSystemThread (dateNick, "Disconnect ()");
            }
        }
    }
    IRC_MFree(12, &prefix, &message, &nick, &user, &real, &pass, &host,
            &dateNick, &nickname, &username, &host2, &server);
    return errCheck;
}

long reply_whoischannels (char* msg){
    return parse_pref_nick_name_msg_to_active (IRCParse_RplWhoIsChannels, msg, SERVERNICK);
}

long reply_whoisoperator (char* msg){
    return parse_pref_nick_name_msg_to_active (IRCParse_RplWhoIsOperator, msg, SERVERNICK);
}

long reply_whoisserver (char* msg){
    perror(msg);
    return 0;
}

long reply_whoisidle (char* msg){
    perror(msg);
    return 0;
}

long reply_youreoper (char* msg){
    perror(msg);
    return 0;
}

long reply_notopic (char* msg){
    perror(msg);
    return 0;
}

long reply_uniqopis (char* msg){
    perror(msg);
    return 0;
}

long reply_statscommands (char* msg){
    perror(msg);
    return 0;
}

long reply_endofbanlist (char* msg){
    perror(msg);
    return 0;
}

long reply_endofexceptlist (char* msg){
    perror(msg);
    return 0;
}

long reply_endofinvitelist (char* msg){
    perror(msg);
    return 0;
}

long reply_endofnames (char* msg){
    perror(msg);
    return 0;
}

long reply_exceptlist (char* msg){
    perror(msg);
    return 0;
}

long reply_invitelist (char* msg){
    perror(msg);
    return 0;
}

long reply_inviting (char* msg){
    perror(msg);
    return 0;
}

long reply_liststart (char* msg){
    perror(msg);
    return 0;
}

long reply_list (char* msg){
    perror(msg);
    return 0;
}

long reply_listend (char* msg){
    perror(msg);
    return 0;
}

long reply_youreservice (char* msg){
    perror(msg);
    return 0;
}

long reply_yourhost (char* msg){
    perror(msg);
    return 0;
}

long reply_myinfo (char* msg){
    perror(msg);
    return 0;
}

long reply_endofwho (char* msg){
    perror(msg);
    return 0;
}

long reply_endofwhowas (char* msg){
    perror(msg);
    return 0;
}

long reply_whowasuser (char* msg){
    perror(msg);
    return 0;
}

long reply_whoisuser (char* msg){
    perror(msg);
    return 0;
    //return parse_pref_nick_msg_to_active (parse3char parseFunc, msg, SERVERNICK);
}

long reply_adminme (char* msg){
    perror(msg);
    return 0;
}

long reply_adminloc1        (char* msg){
    perror(msg);
    return 0;
}

long reply_adminloc2        (char* msg){
    perror(msg);
    return 0;
}

long reply_adminemail (char* msg){
    perror(msg);
    return 0;
}

long reply_info (char* msg){
    perror(msg);
    return 0;
}

long reply_endoflinks (char* msg){
    perror(msg);
    return 0;
}

long reply_endofinfo (char* msg){
    perror(msg);
    return 0;
}

long reply_endofmotd (char* msg){
    perror(msg);
    return 0;
}

long reply_endofstats (char* msg){
    perror(msg);
    return 0;
}

long reply_links (char* msg){
    perror(msg);
    return 0;
}

long reply_luserchannels (char* msg){
    perror(msg);
    return 0;
}

long reply_luserclient (char* msg){
    perror(msg);
    return 0;
}

long reply_luserme (char* msg){
    perror(msg);
    return 0;
}

long reply_luserop (char* msg){
    perror(msg);
    return 0;
}

long reply_luserunknown (char* msg){
    perror(msg);
    return 0;
}

long reply_statslinkinfo (char* msg){
    perror(msg);
    return 0;
}

long reply_statsoline (char* msg){
    perror(msg);
    return 0;
}

long reply_statsuptime (char* msg){
    perror(msg);
    return 0;
}

long reply_time (char* msg){
    perror(msg);
    return 0;
}

long reply_traceclass (char* msg){
    perror(msg);
    return 0;
}

long reply_traceconnect (char* msg){
    perror(msg);
    return 0;
}

long reply_traceconnecting (char* msg){
    perror(msg);
    return 0;
}

long reply_tracehandshake (char* msg){
    perror(msg);
    return 0;
}

long reply_tracelink (char* msg){
    perror(msg);
    return 0;
}

long reply_tracenewtype (char* msg){
    perror(msg);
    return 0;
}

long reply_traceoperator (char* msg){
    perror(msg);
    return 0;
}

long reply_traceserver (char* msg){
    perror(msg);
    return 0;
}

long reply_traceservice (char* msg){
    perror(msg);
    return 0;
}

long reply_traceuser (char* msg){
    perror(msg);
    return 0;
}

long reply_traceunknown (char* msg){
    perror(msg);
    return 0;
}

long reply_tracelog (char* msg){
    perror(msg);
    return 0;
}

long reply_traceend (char* msg){
    perror(msg);
    return 0;
}

long reply_version (char* msg){
    perror(msg);
    return 0;
}

long reply_servlist (char* msg){
    perror(msg);
    return 0;
}

long reply_servlistend (char* msg){
    perror(msg);
    return 0;
}

long reply_endofusers (char* msg){
    perror(msg);
    return 0;
}

long reply_ison (char* msg){
    perror(msg);
    return 0;
}

long reply_nousers (char* msg){
    perror(msg);
    return 0;
}

long reply_nowaway (char* msg){
    perror(msg);
    return 0;
}

long reply_rehashing (char* msg){
    perror(msg);
    return 0;
}

long reply_summoning (char* msg){
    perror(msg);
    return 0;
}

long reply_unaway (char* msg){
    perror(msg);
    return 0;
}

long reply_userhost (char* msg){
    perror(msg);
    return 0;
}

long reply_users (char* msg){
    perror(msg);
    return 0;
}

long reply_usersstart (char* msg){
    perror(msg);
    return 0;
}

long reply_away (char* msg){
    perror(msg);
    return 0;
}

long reply_created (char* msg){
    perror(msg);
    return 0;
}

long reply_bounce (char* msg){
    perror(msg);
    return 0;
}

long reply_tryagain (char* msg){
    perror(msg);
    return 0;
}

long reply_yourid (char* msg){
    perror(msg);
    return 0;
}

long reply_creationtime (char* msg){
    perror(msg);
    return 0;
}

long reply_localusers (char* msg){
    perror(msg);
    return 0;
}

long reply_globalusers (char* msg){
    perror(msg);
    return 0;
}

long reply_topicwhotime (char* msg){
    perror(msg);
    return 0;
}

long reply_channelurl (char* msg){
    perror(msg);
    return 0;
}


long comrpl_pass (char* msg){
    perror(msg);
    return 0;
}
long comrpl_nick (char* msg){
    perror(msg);
    return 0;
}
long comrpl_user (char* msg){
    perror(msg);
    return 0;
}
long comrpl_oper (char* msg){
    perror(msg);
    return 0;
}
long comrpl_mode (char* msg){
    perror(msg);
    return 0;
}
long comrpl_service (char* msg){
    perror(msg);
    return 0;
}
long comrpl_squit (char* msg){
    perror(msg);
    return 0;
}

long comrpl_topic (char* msg){
    perror(msg);
    return 0;
}
long comrpl_names (char* msg){
    perror(msg);
    return 0;
}
long comrpl_list (char* msg){
    perror(msg);
    return 0;
}
long comrpl_invite (char* msg){
    perror(msg);
    return 0;
}
long comrpl_kick (char* msg){
    perror(msg);
    return 0;
}
long comrpl_notice (char* msg){
    perror(msg);
    return 0;
}
long comrpl_motd (char* msg){
    perror(msg);
    return 0;
}
long comrpl_lusers (char* msg){
    perror(msg);
    return 0;
}
long comrpl_version (char* msg){
    perror(msg);
    return 0;
}
long comrpl_stats (char* msg){
    perror(msg);
    return 0;
}
long comrpl_links (char* msg){
    perror(msg);
    return 0;
}
long comrpl_time (char* msg){
    perror(msg);
    return 0;
}
long comrpl_connect (char* msg){
    perror(msg);
    return 0;
}
long comrpl_trace (char* msg){
    perror(msg);
    return 0;
}
long comrpl_admin (char* msg){
    perror(msg);
    return 0;
}
long comrpl_info (char* msg){
    perror(msg);
    return 0;
}
long comrpl_servlist (char* msg){
    perror(msg);
    return 0;
}
long comrpl_squery (char* msg){
    perror(msg);
    return 0;
}
long comrpl_who (char* msg){
    perror(msg);
    return 0;
}
long comrpl_whois (char* msg){
    perror(msg);
    return 0;
}
long comrpl_whowas (char* msg){
    perror(msg);
    return 0;
}
long comrpl_kill (char* msg){
    perror(msg);
    return 0;
}
long comrpl_ping (char* msg){
    long errCheck;
    char *tcpMsg;
    char *prefix, *server, *server2, *message;

    errCheck = IRCParse_Ping (msg, &prefix, &server, &server2, &message);
    if (errCheck == IRC_OK){
        errCheck = IRCMsg_Pong (&tcpMsg, NULL, server, server2, message);
        if (errCheck == IRC_OK){
            tcp_client_send(get_socket_fd(), tcpMsg, strlen(message)+1);
        }
    }
    return errCheck;
}

long comrpl_pong (char* msg){
    perror(msg);
    return 0;
}
long comrpl_error (char* msg){
    perror(msg);
    return 0;
}
long comrpl_away (char* msg){
    perror(msg);
    return 0;
}
long comrpl_rehash (char* msg){
    perror(msg);
    return 0;
}
long comrpl_die (char* msg){
    perror(msg);
    return 0;
}
long comrpl_restart (char* msg){
    perror(msg);
    return 0;
}
long comrpl_summon (char* msg){
    perror(msg);
    return 0;
}
long comrpl_users (char* msg){
    perror(msg);
    return 0;
}
long comrpl_wallops (char* msg){
    perror(msg);
    return 0;
}
long comrpl_userhost (char* msg){
    perror(msg);
    return 0;
}
long comrpl_ison (char* msg){
    perror(msg);
    return 0;
}
long comrpl_help (char* msg){
    perror(msg);
    return 0;
}
long comrpl_rules (char* msg){
    perror(msg);
    return 0;
}
long comrpl_server (char* msg){
    perror(msg);
    return 0;
}
long comrpl_encap (char* msg){
    perror(msg);
    return 0;
}
long comrpl_cnotice (char* msg){
    perror(msg);
    return 0;
}
long comrpl_cprivmsg (char* msg){
    perror(msg);
    return 0;
}
long comrpl_namesx (char* msg){
    perror(msg);
    return 0;
}
long comrpl_silence (char* msg){
    perror(msg);
    return 0;
}
long comrpl_uhnames (char* msg){
    perror(msg);
    return 0;
}
long comrpl_watch (char* msg){
    perror(msg);
    return 0;
}
long comrpl_knock (char* msg){
    perror(msg);
    return 0;
}
long comrpl_userip (char* msg){
    perror(msg);
    return 0;
}
long comrpl_setname (char* msg){
    perror(msg);
    return 0;
}
