#include <syslog.h>
#include <stdarg.h>
#include <stdio.h>

#include "G-2362-06-P1-ircmsg.h"

void irc_errmsg(int priority, char* errcode, char* message){
    if (message && (priority >= 0)){
        if (flag_check(FLAG_DAEMON)){
            syslog(priority, "%s :%s", errcode, message);
        } else {
            fprintf(stderr, "%s\n", message );
        }
    }
}
/**
 * Envia mensaje de error a stderr o a syslog dependiendo de la bandera
 * de DAEMON.
 * @param error    Codigo del error.
 * @param priority Prioridad del error.
 * @param added    Mensaje al que se encadena.
 */
void irc_error (long error, int priority, char* added){

    if (added){
        if (flag_check(FLAG_DAEMON)){
            IRC_syslog(added, error, priority);
        } else {
            IRC_perror(added, error);
        }
    }
}

/**
 * Mensaje de aviso en o bien el syslog o el stderr.
 * @param message Mensaje a enviar.
 */
void irc_notice (char* format, ...){
    va_list arglist;

    if (format){
        va_start(arglist, format);
        if (flag_check(FLAG_DAEMON)){
            vsyslog(LOG_NOTICE, format, arglist);
        } else {
            vfprintf(stderr, format, arglist);
        }
        va_end(arglist);
    }
}

/**
 * Mensaje de aviso en o bien el syslog o el stderr.
 * @param message Mensaje a enviar.
 */
void irc_warning (char* format, ...){
    va_list arglist;

    if (format){
        va_start(arglist, format);
        if (flag_check(FLAG_DAEMON)){
            vsyslog(LOG_WARNING, format, arglist);
        } else {
            vfprintf(stderr, format, arglist);
        }
        va_end(arglist);
    }
}

/**
 * Mensaje de debugueo en el sy slog o stderr.
 * @param format  Formato de los parametros a imprimir.
 * @param VARARGS Parametros a imprimir.
 */
void irc_debug (char* format, ...){
    va_list arglist;

    if (format && flag_check(FLAG_DEBUG)){
        va_start(arglist, format);
        if (flag_check(FLAG_DAEMON)){
            vsyslog(LOG_DEBUG, format, arglist);
        } else {
            vfprintf(stderr, format, arglist);
        }
        va_end(arglist);
    }
}

/**
 * Mensaje de mas informacion en el syslog o stderr.
 * @param format  Formato de los parametros a imprimir.
 * @param VARARGS Parametros a imprimir.
 */
void irc_info (char* format, ...){
    va_list arglist;

    if (format && flag_check(FLAG_VERBOSE)){
        va_start(arglist, format);
        if (flag_check(FLAG_DAEMON)){
            vsyslog(LOG_DEBUG, format, arglist);
        } else {
            vfprintf(stderr, format, arglist);
        }
        va_end(arglist);
    }
}
