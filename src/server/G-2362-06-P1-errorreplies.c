#include "G-2362-06-P1-errorreplies.h"

#include <stdio.h>
#include <stdlib.h>

#include <stdarg.h>
#include <time.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>
#include <pthread.h>
#include <netdb.h>
#include <syslog.h>

#define ERRFUN3(fun)                    (error_Args3Default (fun, user->nick, user->socket))
#define ERRFUN4(fun, aux)               (error_Args4Default (fun, user->nick, aux, user->socket))
#define ERRFUN5(fun, aux, aux2)         (error_Args5Default (fun, user->nick, aux, aux2, user->socket))
#define ERRFUN6(fun, aux, aux2, aux3)   (error_Args6Default (fun, user->nick, aux, aux2, aux3, user->socket))

/*Funciones con distintos argumentos para evitar repetir codigo en lo posible*/
typedef long (*error3ArgsFunction)(char **command, char *prefix, char* nick);
typedef long (*error4ArgsFunction)(char **command, char *prefix, char *nick, char *aux);
typedef long (*error5ArgsFunction)(char **command, char *prefix, char *nick, char *aux, char* aux2);
typedef long (*error6ArgsFunction)(char **command, char *prefix, char *nick, char *aux, char* aux2, char* aux3);

/*Funciones de 3, 4, 5 y 6 argumentos*/
long error_Args3Default (error3ArgsFunction errorf, char* nick, int socket){
    long errCheck;
    int tcpErr;
	char* message;

	errCheck = errorf (&message, server_get_name(), nick);
	if (errCheck == IRC_OK){
        tcpErr = tcp_server_send(socket, message);
        errCheck = (tcpErr == TCP_OK)? IRC_OK : IRCERR_INVALIDSOCKET;
        irc_debug ("DEBUG: %s Sent %s", __FUNCTION__, message);
        free(message);
    }
	return errCheck;
}

long error_Args4Default(error4ArgsFunction errorf, char* nick, char* aux, int socket){
	long errCheck;
    int tcpErr;
	char* message;

	errCheck = errorf (&message, server_get_name(), nick, aux);
	if (errCheck == IRC_OK){
        tcpErr = tcp_server_send(socket, message);
        errCheck = (tcpErr == TCP_OK)? IRC_OK : IRCERR_INVALIDSOCKET;
        irc_debug ("DEBUG: %s Sent %s", __FUNCTION__, message);
        free(message);
    }
	return errCheck;
}

long error_Args5Default(error5ArgsFunction errorf, char* nick, char* aux, char* aux2, int socket){
	long errCheck;
	char* message;
	int tcpErr;

	errCheck = errorf (&message, server_get_name(), nick, aux, aux2);
	if (errCheck == IRC_OK){
        tcpErr = tcp_server_send(socket, message);
        errCheck = (tcpErr == TCP_OK)? IRC_OK : IRCERR_INVALIDSOCKET;
        irc_debug ("DEBUG: %s Sent %s", __FUNCTION__, message);
        free(message);
    }
	return errCheck;
}

long error_Args6Default(error6ArgsFunction errorf, char* nick, char* aux, char* aux2, char* aux3, int socket){
	long errCheck;
	char* message;
	int tcpErr;

	errCheck = errorf (&message, server_get_name(), nick, aux, aux2, aux3);
	if (errCheck == IRC_OK){
        tcpErr = tcp_server_send(socket, message);
        errCheck = (tcpErr == TCP_OK)? IRC_OK : IRCERR_INVALIDSOCKET;
        irc_debug ("DEBUG: %s Sent %s", __FUNCTION__, message);
        free(message);
    }
	return errCheck;
}

/* Funciones de enviado de errores */
long error_error (User* user){
	return OK;
}

long error_needmoreparams (User* user, char* failedcommand){
    return ERRFUN4(IRCMsg_ErrNeedMoreParams, failedcommand);
}
long error_alreadyregistered (User* user){
    return ERRFUN3(IRCMsg_ErrAlreadyRegistred);
}
long error_nonicknamegiven (User* user){
    return ERRFUN3(IRCMsg_ErrNoNickNameGiven);
}
long error_erroneusnickname (User* user,  char* erroneousnick){
	return ERRFUN4(IRCMsg_ErrErroneusNickName, erroneousnick);
}
long error_nicknameinuse (User* user,  char* erroneousnick){
    return ERRFUN4(IRCMsg_ErrNickNameInUse, erroneousnick);
}
long error_nickcollision (User* user, char* nick2){
    return ERRFUN6(IRCMsg_ErrNickCollision, nick2, user->user, user->host);
}
long error_unavailresource (User* user,  char* nickorchannel){
    return ERRFUN4(IRCMsg_ErrUnavailResource, nickorchannel);
}
long error_restricted (User* user){
    return ERRFUN3(IRCMsg_ErrRestricted);
}
long error_nooperhost (User* user){
    return ERRFUN3(IRCMsg_ErrNoOperHost);
}
long error_passwdmismatch (User* user){
    return ERRFUN3(IRCMsg_ErrPasswdMismatch);
}
long error_umodeunknownflag (User* user){
    return ERRFUN3(IRCMsg_ErrUModeUnknownFlag);
}
long error_usersdontmatch (User* user){
    return ERRFUN3(IRCMsg_ErrUsersDontMatch);
}
long error_noprivileges (User* user){
	return ERRFUN3(IRCMsg_ErrNoPrivileges);
}
long error_nosuchserver (User* user,  char* servername){
    return ERRFUN4(IRCMsg_ErrNoSuchServer, servername);
}
long error_wasnosuchnick (User* user,  char* nickname){
    return ERRFUN4(IRCMsg_ErrWasNoSuchNick, nickname);
}
long error_badmask (User* user,  char* mask){
    return ERRFUN4(IRCMsg_ErrBadMask, mask);
}
long error_cannotsendtochan (User* user,  char* channel){
    return ERRFUN4(IRCMsg_ErrCanNotSendToChan, channel);
}
long error_notexttosend (User* user){
    return ERRFUN3(IRCMsg_ErrNoTextToSend);
}
long error_notoplevel (User* user, char* mask){
    return ERRFUN4(IRCMsg_ErrNoTopLevel, mask);
}
long error_wildtoplevel (User* user, char* mask){
    return ERRFUN4 (IRCMsg_ErrWildTopLevel, mask);
}

/** ERRORES DE CANALES **/

long error_badchanmask (User* user,  char* channel){
	return ERRFUN4(IRCMsg_ErrBadChanMask, channel);
}

long error_badchannelkey (User* user,  char* channel){
	return ERRFUN4(IRCMsg_ErrBadChannelKey, channel);
}

long error_bannedfromchan (User* user,  char* channel){
	return ERRFUN4(IRCMsg_ErrBannedFromChan, channel);
}

long error_channelisfull (User* user,  char* channel){
	return ERRFUN4(IRCMsg_ErrChannelIsFull, channel);
}

long error_chanoprivsneeded (User* user,  char* channel){
	return ERRFUN4(IRCMsg_ErrChanOPrivsNeeded, channel);
}

long error_inviteonlychan (User* user,  char* channel){
	return ERRFUN4(IRCMsg_ErrInviteOnlyChan, channel);
}

long error_keyset (User* user,  char* channel){
	return ERRFUN4(IRCMsg_ErrKeySet, channel);
}

long error_nochanmodes (User* user,  char* channel){
	return ERRFUN4(IRCMsg_ErrNoChanModes, channel);
}

long error_nosuchchannel (User* user, char* channel){
	return ERRFUN4(IRCMsg_ErrNoSuchChannel, channel);
}
long error_notonchannel (User* user, char* userdest, char* channel){
    return ERRFUN5(IRCMsg_ErrNotOnChannel, userdest, channel);
}
long error_toomanychannels (User* user,  char* channelname){
    return ERRFUN4(IRCMsg_ErrTooManyChannels, channelname);
}
long error_toomanytargets (User* user, char* target, char* errorcode, char* abortmsg){
    return ERRFUN6(IRCMsg_ErrTooManyTargets, target, errorcode, abortmsg);
}
long error_unknownmode (User* user, char* modechar, char* channel){
    return ERRFUN5(IRCMsg_ErrUnknownMode, modechar, channel);
}
long error_usernotinchannel (User* user, char* nick2, char* channel){
    return ERRFUN5(IRCMsg_ErrUserNotInChannel, nick2, channel);
}
long error_useronchannel (User* user, char* user2, char* channel){
    return ERRFUN5(IRCMsg_ErrUserOnChannel, user2, channel);
}
long error_nomotd (User* user){
    return ERRFUN3(IRCMsg_ErrNoMotd);
}
long error_nosuchservice (User* user, char* servicename){
    return ERRFUN4(IRCMsg_ErrNoSuchService, servicename);
}
long error_cantkillserver (User* user, char* msg){
    return ERRFUN4(IRCMsg_ErrCantKillServer, msg);
}
long error_noorigin (User* user,  char* msg){
    return ERRFUN4(IRCMsg_ErrNoOrigin, msg);
}
long error_fileerror (User* user, char* filerror, char* file){
    return ERRFUN5(IRCMsg_ErrFileError, filerror, file);
}
long error_nologin (User* user,  char* username){
    return ERRFUN4 (IRCMsg_ErrNoLogin, username);
}
long error_summondisabled (User* user){
    return ERRFUN3 (IRCMsg_ErrSummonDisabled);
}
long error_usersdisabled (User* user){
    return ERRFUN3 (IRCMsg_ErrUsersDisabled);
}
long error_nosuchnick (User* user,  char* nickname){
    return ERRFUN4 (IRCMsg_ErrNoSuchNick, nickname);
}
long error_unknowncommand (User* user,  char* errcommand){
    return ERRFUN4 (IRCMsg_ErrUnKnownCommand, errcommand);
}
long error_noadmininfo (User* user){
    return ERRFUN4 (IRCMsg_ErrNoAdminInfo, server_get_name());
}
long error_notregistered (User* user){
    return ERRFUN3 (IRCMsg_ErrNotRegisterd);
}
long error_nopermforhost (User* user){
    return ERRFUN3 (IRCMsg_ErrNoOperForHost);
}
long error_yourebannedcreep (User* user){
    return ERRFUN3(IRCMsg_ErrYoureBannedCreep);
}
long error_youwillbebanned (User* user){
    return ERRFUN3 (IRCMsg_ErrYouWillBeBanned);
}
long error_banlistfull (User* user, char *channel, char *character, char *msg){
    return ERRFUN6(IRCMsg_ErrBanListFull, channel, character, msg);
}
long error_uniqopprivsneeded (User* user,  char* msg){
    return ERRFUN4(IRCMsg_ErrUniqOpPrivsNeeded, msg);
}
long error_norecipient (User* user,  char* errcommand){
    return ERRFUN4(IRCMsg_ErrNoRecipient, errcommand);
}
long error_toomanymatches (User* user, char* errcommand, char* mask, char* info){
    return ERRFUN6(IRCMsg_ErrTooManyMatches, errcommand, mask, info);
}
