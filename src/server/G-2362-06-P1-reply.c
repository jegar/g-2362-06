#include "G-2362-06-P1-reply.h"

#include <stdio.h>
#include <stdlib.h>

#include <redes2/irc.h>

#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>
#include <pthread.h>
#include <netdb.h>
#include <syslog.h>

#define MODESTRING_LEN  9
#define MOTD_LEN        80

long reply_youreoper 		(){
    return OK;
}

/**
 * Respuesta del comando PRIVMSG a un s&oacute;lo usuario.
 * @param  user     Usuario que manda el mensaje.
 * @param  nickdest Nick del usuario destino.
 * @param  msg      Mensaje que mandar.
 * @return          OK en caso de &eacute;xito, ERR en caso de fallo.
 */
long reply_privmsg_user (User *user, char* nickdest, char* msg){
    long errCheck = IRCERR_NOUSER;
    int tcpErr;
    char *message, *complexUser;
    User userDest;

    user_zero(&userDest);
    userDest.nick = nickdest;

    if (user_find(&userDest) == OK){
        errCheck = user_complex (user, &complexUser);
        if (errCheck == IRC_OK){
            if (userDest.away){
                reply_away (user, &userDest);
            }
            errCheck = IRCMsg_Privmsg (&message, complexUser,
                                    userDest.nick, msg);
            if (errCheck == IRC_OK){
                irc_debug ("%s %s Send %s", DEBUGTEXT, __FUNCTION__, message);
                tcpErr = tcp_server_send (userDest.socket, message);
                errCheck = (tcpErr == TCP_OK)? IRC_OK : IRCERR_INVALIDSOCKET;
                free(message);
            }
            free(complexUser);
        }
        userDest.nick = NULL;
        user_clear(&userDest);
    }
    return errCheck;
}

/**
 * Respuesta del comando PRIVMSG a un canal.
 * @param  user    Usuario que manda el mensaje.
 * @param  channel Canal al que se manda el mensaje.
 * @param  msg     Mensaje que se manda.
 * @return         OK en caso de &eacute;xito, ERR en caso de fallo.
 */
long reply_privmsg_chan (User *user, char* channel, char* msg){
    long errCheck = IRCERR_NOUSER;
    char *message, *complexUser;

    irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
    errCheck = user_complex (user, &complexUser);
    if (errCheck == IRC_OK){
        errCheck = IRCMsg_Privmsg(&message, complexUser, channel, msg);
        if (errCheck == IRC_OK){
            irc_debug ("%s end %s", DEBUGTEXT, "SEND to channel");
            errCheck = channel_send (message, channel, user);
        }
    }
    IRC_MFree (2, &message, &complexUser);
    irc_debug ("%s end %s", DEBUGTEXT, __FUNCTION__);
    return errCheck;
}

/**
 * Respuesta del comando MODE que especifica el modo del usuario.
 * @param  user          Usuario del que se busca el modo.
 * @param  modeOnChannel Representacion del modo en el canal.
 * @return               OK en caso de &eacute;xito, ERR en caso de fallo.
 */
long reply_umodeis (User *user, long modeOnChannel){
    long errCheck = IRCERR_NOUSER;
    int tcpErr;
    char modeString[MODESTRING_LEN] = "+";
    char *awayMsg, *message;

    if (user){
        if (IRCUMODE_OPERATOR && modeOnChannel) strcat(modeString, "o");
        if (IRCUMODE_LOCALOPERATOR && modeOnChannel) strcat(modeString, "O");
        if (IRCUMODE_SERVERNOTICES && modeOnChannel) strcat(modeString, "s");
        if (IRCUMODE_INVISIBLE && modeOnChannel) strcat(modeString, "i");
        if (IRCUMODE_RESTRICTED && modeOnChannel) strcat(modeString, "r");
        if (IRCUMODE_RECEIVEWALLOPS && modeOnChannel) strcat(modeString, "w");

        errCheck = IRCTADUser_GetAway(0, NULL, user->nick, NULL, &awayMsg);
        if (errCheck == IRC_OK){
            if (awayMsg){
                strcat(modeString, "a");
                free(awayMsg);
            }
            errCheck = IRCMsg_RplUModeIs (&message, server_get_name(),
                            user->nick, modeString);
            if (errCheck == IRC_OK){
                irc_debug ("%s %s Send %s", DEBUGTEXT, __FUNCTION__, message);
                tcpErr = tcp_server_send(user->socket, message);
                errCheck = (tcpErr == TCP_OK)? IRC_OK : IRCERR_INVALIDSOCKET;
                free(message);
            }
        }
    }
    return errCheck;
}

/**
 * Respuesta del comando QUIT.
 * @param  user Usuario que se marcha del servidor.
 * @param  msg  Mensaje de despedida.
 * @return      OK en caso de &eacute;xito, ERR en caso de fallo.
 */
long reply_quit (User* user, char* msg){
    long errCheck = IRCERR_NOUSER;
    char defleave[] = "Leaving";
    char *message, *complexUser;
    int tcpErr;

    if (!msg){
        msg = defleave;
    }
    errCheck = user_complex (user, &complexUser);
    if (errCheck == IRC_OK){
        errCheck = IRCMsg_Quit (&message, complexUser, msg);
        if (errCheck == IRC_OK){
            irc_debug ("%s %s Send %s", DEBUGTEXT, __FUNCTION__, message);
            errCheck = all_channels_send (message, user);
            tcpErr = tcp_server_send(user->socket, message);
            errCheck = (tcpErr == TCP_OK)? IRC_OK : IRCERR_INVALIDSOCKET;
            free(message);
        }
        free(complexUser);
    }
    return errCheck;
}

/**
 * Respuesta que dice el servidor.
 * @param  user Usuario del cual queremos el servidor.
 * @return      OK en caso de &eacute;xito, ERR en caso de fallo.
 */
long reply_yourhost (User* user){
    long errCheck = IRCERR_NOUSER;
    int tcpErr;
    char *message;

    errCheck = IRCMsg_RplYourHost (&message, server_get_name(),
                    user->nick, server_get_name(), server_get_vers());
    if (errCheck == IRC_OK){
        irc_debug ("%s %s Send %s", DEBUGTEXT, __FUNCTION__, message);
        tcpErr = tcp_server_send(user->socket, message);
        errCheck = (tcpErr == TCP_OK)? IRC_OK : IRCERR_INVALIDSOCKET;
        free(message);
    }
    return errCheck;
}

/**
 * Respuesta final de el comando WHOIS.
 * @param  user Usuario al que se le manda el fin del comando.
 * @return      OK en caso de &eacute;xito, ERR en caso de fallo.
 */
long reply_endofwhois (User* user){
    long errCheck = IRCERR_NOUSER;
    int tcpErr;
    char* message;

    errCheck = IRCMsg_RplEndOfWhoIs (&message,
                    server_get_name(), user->nick, user->nick);
    if (errCheck == IRC_OK){
        irc_debug ("%s %s Send %s", DEBUGTEXT, __FUNCTION__, message);
        tcpErr = tcp_server_send(user->socket, message);
        errCheck = (tcpErr == TCP_OK)? IRC_OK : IRCERR_INVALIDSOCKET;
        free(message);
    }
    return errCheck;
}

long reply_whoreply (User *user, User *userDest, char *channel, char* oppar, char* mask){
    long errCheck = IRCERR_NOUSER, modeOnChannel;
    int tcpErr;
    char *message, type[6] = "", *away = NULL, *chanAux, nomask[] = "*";

    if (user && userDest){
        modeOnChannel = IRCTAD_GetUserModeOnChannel (channel, userDest->nick);
        if ((modeOnChannel >= 0) && !(modeOnChannel & IRCUMODE_INVISIBLE)){
            if ((modeOnChannel & IRCUMODE_OPERATOR) || !oppar){
                errCheck = IRCTADUser_GetAway(userDest->userId, userDest->user,
                    userDest->nick, userDest->real, &away);
                if (errCheck == IRC_OK){
                    if (away){
                        strcat(type, "G");
                    } else {
                        strcat(type, "H");
                    }
                    if (modeOnChannel & IRCUMODE_LOCALOPERATOR){
                        strcat (type, " *");
                    }
                    if (modeOnChannel & (IRCUMODE_CREATOR | IRCUMODE_OPERATOR)){
                        if (mask)
                            strcat (type, " @");
                    } else if (modeOnChannel & IRCUMODE_VOICE){
                        if (mask)
                            strcat (type, " +");
                    }
                    if (mask){
                        chanAux = channel;
                    } else {
                        chanAux = nomask;
                    }
                    irc_debug ("%s %s Message parameters user: %s", DEBUGTEXT, __FUNCTION__, userDest->user);
                    errCheck = IRCMsg_RplWhoReply (&message, server_get_name(),
                        user->nick, chanAux, userDest->user, userDest->host,
                        server_get_name(), userDest->nick, type, 0, userDest->real);
                    if (errCheck == IRC_OK){
                        irc_debug ("%s %s Send %s", DEBUGTEXT, __FUNCTION__, message);
                        tcpErr = tcp_server_send(user->socket, message);
                        errCheck = (tcpErr == TCP_OK)? IRC_OK : IRCERR_INVALIDSOCKET;
                        free(message);
                    }
                }
            }
        }
    }
    return errCheck;
}

/**
 * Respuesta del comando WHOIS con los datos del usuario.
 * @param  user     Usuario que realiza la consulta.
 * @param  userdest Usuario sobre el que se pregunta.
 * @return          OK en caso de &eacute;xito, ERR en caso de fallo.
 */
long reply_whoisuser (User* user, User* userdest){
    long errCheck = IRCERR_NOUSER;
    int tcpErr;
    char* message;

    if (user){
        errCheck = IRCMsg_RplWhoIsUser (&message, server_get_name(), userdest->nick,
                    userdest->nick, userdest->user, userdest->host, userdest->real);
        if (errCheck == IRC_OK){
            irc_debug ("%s %s Send %s", DEBUGTEXT, __FUNCTION__, message);
            tcpErr = tcp_server_send(user->socket, message);
            errCheck = (tcpErr == TCP_OK)? IRC_OK : IRCERR_INVALIDSOCKET;
            free(message);
        }
    }
    return errCheck;
}

/**
 * Respuesta del comando WHOIS de los canales del usuario.
 * @param  user     Usuario que realiza la consulta.
 * @param  userdest Usuario del cual se pregunta.
 * @return          OK en caso de &eacute;xito, ERR en caso de fallo.
 */
long reply_whoischannels (User* user, User* userdest){
    long errCheck = IRCERR_NOUSER, numChan;
    int tcpErr;
    char *message, *listChan;

    if (user){
        errCheck = IRCTAD_ListChannelsOfUser (userdest->user,
                            userdest->nick, &listChan, &numChan);
        if (errCheck == IRC_OK){
            if (numChan > 0){
                errCheck = IRCMsg_RplWhoIsChannels (&message, server_get_name(),
                                userdest->nick, userdest->nick, listChan);
                if(errCheck == IRC_OK){
                    irc_debug ("%s %s Send %s", DEBUGTEXT, __FUNCTION__, message);
                    tcpErr = tcp_server_send(user->socket, message);
                    errCheck = (tcpErr == TCP_OK)? IRC_OK : IRCERR_INVALIDSOCKET;
                    free(message);
                }
            }
            free(listChan);
        }
    }
    return errCheck;
}

/**
 * Respuesta de la consulta WHOIS cuando el usuario es un operador.
 * @param  user     Usuario que realiza la consulta.
 * @param  userdest Usuario del caual se pregunta.
 * @return          OK en caso de &eacute;xito, ERR en caso de fallo.
 */
long reply_whoisoperator (User* user, User* userdest){
    long errCheck, umodeChan, chanNumber;
    char *message, **chanList;
    int i, tcpErr;

    errCheck = IRCTAD_ListChannelsOfUserArray (userdest->user,
                    userdest->nick, &chanList, &chanNumber);
    if (errCheck == IRC_OK){
        errCheck = IRCMsg_RplWhoIsOperator (&message, server_get_name(),
                                            userdest->nick, userdest->nick);
        if (errCheck == IRC_OK){
            for (i = 0; i < chanNumber; i++){
                umodeChan = IRCTAD_GetUserModeOnChannel(chanList[i], userdest->nick);
                if ((umodeChan >= 0) &&
                    (umodeChan & (IRCUMODE_OPERATOR | IRCUMODE_LOCALOPERATOR))){
                    if (errCheck == IRC_OK){
                        irc_debug ("%s %s Send %s", DEBUGTEXT, __FUNCTION__, message);
                        tcpErr = tcp_server_send(user->socket, message);
                        errCheck = (tcpErr == TCP_OK)? IRC_OK : IRCERR_INVALIDSOCKET;
                    }
                }
            }
            free(message);
        }
        channel_array_free (chanList, chanNumber);
    }
    return errCheck;
}


long reply_whoisserver (User* user, User* userdest){
    long errCheck = IRCERR_NOUSER;
    int tcpErr;
    char* message;

    errCheck = IRCMsg_RplWhoIsServer (&message, server_get_name(),
                user->nick, user->nick, server_get_name(), server_get_info());
    if (errCheck == IRC_OK){
        irc_debug ("%s %s Send %s", DEBUGTEXT, __FUNCTION__, message);
        tcpErr = tcp_server_send(user->socket, message);
        errCheck = (tcpErr == TCP_OK)? IRC_OK : IRCERR_INVALIDSOCKET;
        free(message);
    }
    return errCheck;
}

long reply_whoisidle (User* user, User* userdest){
    long errCheck = IRCERR_NOUSER;
    int tcpErr;
    char* message;

    errCheck = IRCMsg_RplWhoIsIdle (&message, server_get_name(), user->nick,
                    user->nick, IRCTAD_SlapsedTime (userdest->actionTS), NULL);
    if (errCheck == IRC_OK){
        irc_debug ("%s %s Send %s", DEBUGTEXT, __FUNCTION__, message);
        tcpErr = tcp_server_send(user->socket, message);
        errCheck = (tcpErr == TCP_OK)? IRC_OK : IRCERR_INVALIDSOCKET;
        free(message);
    }
    return errCheck;
}

long reply_whois (User* user, User* targetuser){
    long errCheck = IRCERR_NOUSER;

    errCheck = reply_whoisuser (user, targetuser);
    if (errCheck == IRC_OK){
        errCheck = reply_whoisserver (user, targetuser);
        if (errCheck == IRC_OK){
            errCheck = reply_whoisoperator (user, targetuser);
            if (errCheck == IRC_OK){
                errCheck = reply_whoisidle (user, targetuser);
                if (errCheck == IRC_OK){
                    errCheck = reply_whoischannels (user, targetuser);
                    if (user->away && errCheck == IRC_OK){
                        errCheck = reply_away (user, targetuser);
                    }
                    if (errCheck == IRC_OK){
                        errCheck = reply_endofwhois (user);
                    }
                }
            }
        }
    }
    return errCheck;
}

long reply_channelmodechange (User *user, char* channel, char* inputmode){
    char* message;
    int tcpErr;
    char newmode[5] = "+";
    long errCheck = IRCERR_NOUSER;
    char* mode;

    if (inputmode[0] != '+')
        mode = strcat(newmode, inputmode);
    else
        mode = inputmode;

    errCheck = IRCMsg_Mode (&message,server_get_name(),channel,mode,user->nick);
    if (errCheck == IRC_OK){
        irc_debug ("%s %s Send %s", DEBUGTEXT, __FUNCTION__, message);
        tcpErr = tcp_server_send(user->socket, message);
        errCheck = (tcpErr == TCP_OK)? IRC_OK : IRCERR_INVALIDSOCKET;
        free(message);
    }
    return errCheck;
}

long reply_channelmodeis (User *user, char* channel, char* mode){
    char* message;
    int tcpErr;
    long errCheck = IRCERR_NOUSER;

    errCheck = IRCMsg_RplChannelModeIs (&message, server_get_name(),
                                        user->nick, channel, mode);
    if (errCheck == IRC_OK){
        irc_debug ("%s %s Send %s", DEBUGTEXT, __FUNCTION__, message);
        tcpErr = tcp_server_send(user->socket, message);
        errCheck = (tcpErr == TCP_OK)? IRC_OK : IRCERR_INVALIDSOCKET;
        free(message);
    }
    return errCheck;
}

long reply_endofnames (User* user, char* channel){
    long errCheck = IRCERR_NOUSER;
    char* message;
    int tcpErr;

    errCheck =  IRCMsg_RplEndOfNames (&message, server_get_name(),
                                    user->nick, channel);
    if (errCheck == IRC_OK){
        irc_debug ("%s %s Send %s", DEBUGTEXT, __FUNCTION__, message);
        tcpErr = tcp_server_send(user->socket, message);
	    errCheck = (tcpErr == TCP_OK)? IRC_OK : IRCERR_INVALIDSOCKET;
        free(message);
    }
    return errCheck;
}

long reply_liststart (User *user){
    long errCheck = IRCERR_NOUSER;
    int tcpErr;
    char* message;

    if (user){
        errCheck = IRCMsg_RplListStart (&message, server_get_name(), user->nick);
        if (errCheck == IRC_OK){
            irc_debug ("%s %s Send %s", DEBUGTEXT, __FUNCTION__, message);
            tcpErr = tcp_server_send(user->socket, message);
            errCheck = (tcpErr == TCP_OK)? IRC_OK : IRCERR_INVALIDSOCKET;
            free(message);
        }
    }
    return errCheck;
}

long reply_lists (User* user, char* channel){
    long errCheck = IRCERR_NOUSER, numChan, numPar;
    register int i, j;
    char **chanList, **parArray;
    char maskChannel[CHAN_NAME_LEN + 3];

    if (user){
        if (channel){
            errCheck =  IRCParse_ParseLists (channel, &parArray, &numPar);
            if (errCheck == IRC_OK){
                for (j = 0; (j < numPar); j++){
                    sprintf(maskChannel, "*%s*", parArray[j]);
                    errCheck = IRCTADChan_GetList(&chanList, &numChan, maskChannel);
                    if (errCheck == IRC_OK){
                        for (i = 0; i < numChan; i++){
                            errCheck = reply_list (user, chanList[i]);
                            if (errCheck != IRC_OK){
                                break;
                            }
                        }
                        IRCTADChan_FreeList (chanList, numChan);
                    }
                }
                IRCTADChan_FreeMaskList (parArray, numPar);
            }
        } else {
            errCheck = IRCTADChan_GetList(&chanList, &numChan, NULL);
            if (errCheck == IRC_OK){
                for (i = 0; i < numChan; i++){
                    errCheck = reply_list (user, chanList[i]);
                    if (errCheck != IRC_OK){
                        break;
                    }
                }
                IRCTADChan_FreeList (chanList, numChan);
            }
        }
    }
    return errCheck;
}

long reply_list (User* user, char* channel){
    long errCheck = IRC_OK, numvisible, mode, numberOfUsers;
    long i;
    char *message, *topic;
    int tcpErr;
    char visible[100], emptytopic[3] = "";
    char **nicklist;

    mode = IRCTADChan_GetModeInt (channel);
    if (!(mode & IRCMODE_SECRET)){
        numvisible = IRCTADChan_GetNumberOfUsers (channel);
        if (numvisible > 0){
            errCheck =  IRCTAD_ListNicksOnChannelArray (channel, &nicklist,
                                                        &numberOfUsers);
            for (i = 0, numvisible = 0; i < numberOfUsers; i++){
                mode =  IRCTAD_GetUserModeOnChannel (channel, nicklist[i]);
                if (!(mode & IRCUMODE_INVISIBLE))
                    numvisible++;
            }
            IRCTADChan_FreeList (nicklist, numberOfUsers);
            sprintf (visible, "%ld", numvisible);
            if (errCheck == IRC_OK){
                errCheck =  IRCTAD_GetTopic (channel, &topic);
                if (errCheck == IRC_OK){
                    errCheck = IRCMsg_RplList (&message, server_get_name(),
                        user->nick, channel, visible, (topic)? topic : emptytopic);
                    if (errCheck == IRC_OK){
                        irc_debug ("%s %s Send %s", DEBUGTEXT, __FUNCTION__, message);
                        tcpErr = tcp_server_send(user->socket, message);
    	                errCheck = (tcpErr == TCP_OK)? IRC_OK : IRCERR_INVALIDSOCKET;
                        free(message);
                    }
                    IRC_MFree(1, &topic);
                }
            }
        }
    }
    return errCheck;
}

long reply_listend (User *user){
    long errCheck = IRCERR_NOUSER;
    int tcpErr;
    char* message;

    errCheck =  IRCMsg_RplListEnd (&message, server_get_name(), user->nick);
    if (errCheck == IRC_OK){
        irc_debug ("%s %s Send %s", DEBUGTEXT, __FUNCTION__, message);
        tcpErr = tcp_server_send(user->socket, message);
	    errCheck = (tcpErr == TCP_OK)? IRC_OK : IRCERR_INVALIDSOCKET;
        free(message);
    }
    return errCheck;
}

long reply_namreply (User* user, char* channel){
    long errCheck = IRCERR_NOUSER, userMode;
    char *message, *auxExtender;
    char typeStr[2] = "";
    char **namelist;
    long numberOfUsers;
    int channeltype, tcpErr;
    int i;

    errCheck = IRCTAD_ListNicksOnChannelArray(channel, &namelist, &numberOfUsers);
    if (errCheck == IRC_OK){

        channeltype = IRCTADChan_GetModeInt(channel);
        if (IRCMODE_SECRET & channeltype){
            strncpy(typeStr, "@", 2);
        } else if (IRCMODE_PRIVATE & channeltype){
            strncpy(typeStr, "*", 2);
        } else {
            strncpy(typeStr, "=", 2);
        }

        for (i = 0 ; i < numberOfUsers; i++){
            userMode = IRCTAD_GetUserModeOnChannel (channel, namelist[i]);
            if (userMode & (IRCUMODE_CREATOR | IRCUMODE_OPERATOR | IRCUMODE_VOICE)){
                auxExtender = (char*)malloc(sizeof(char) * strlen(namelist[i]) + 2); /*Uno por el final de linea, otro por el prefijo*/
                if (auxExtender){
                    if (userMode & (IRCUMODE_CREATOR | IRCUMODE_OPERATOR)){
                        sprintf (auxExtender, "@%s", namelist[i]);
                    } else if (userMode & IRCUMODE_VOICE){
                        sprintf (auxExtender, "+%s", namelist[i]);
                    }
                    free(namelist[i]);
                    namelist[i] = auxExtender;
                }
            }
        }

        for (i = 0 ;  i < numberOfUsers ; i++){
            errCheck = IRCMsg_RplNamReply (&message, server_get_name(),
                            user->nick, typeStr, channel, namelist[i]);
            if (errCheck == IRC_OK){
                irc_debug ("%s %s Send %s", DEBUGTEXT, __FUNCTION__, message);
                tcpErr = tcp_server_send(user->socket, message);
                errCheck = (tcpErr == TCP_OK)? IRC_OK : IRCERR_INVALIDSOCKET;
                free(message);
            }
        }
        IRCTADUser_FreeList (namelist, numberOfUsers);
    }
    return errCheck;
}

long reply_notopic (User *user, char* channel){
    char* message;
    long errCheck = IRCERR_NOUSER;
    int tcpErr;

    errCheck = IRCMsg_RplNoTopic (&message, server_get_name(),
                                user->nick, channel);
    if (errCheck == IRC_OK){
        irc_debug ("%s %s Send %s", DEBUGTEXT, __FUNCTION__, message);
        tcpErr = tcp_server_send(user->socket, message);
        errCheck = (tcpErr == TCP_OK)? IRC_OK : IRCERR_INVALIDSOCKET;
        free(message);
    }
    return errCheck;
}

long reply_newtopic (User* user, char* channel, char* topic){
    long errCheck = IRCERR_NOUSER;
    char *message, *complexUser;
    int tcpErr;

    errCheck = user_complex (user, &complexUser);
    if (errCheck == IRC_OK){
        errCheck = IRCMsg_Topic (&message, complexUser, channel, topic);
        if (errCheck == IRC_OK){
            irc_debug ("%s %s Send %s", DEBUGTEXT, __FUNCTION__, message);
            tcpErr = tcp_server_send(user->socket, message);
            errCheck = (tcpErr == TCP_OK)? IRC_OK : IRCERR_INVALIDSOCKET;
        }
    }
    IRC_MFree(2, &message, &complexUser);
    return errCheck;
}

long reply_kicked (User* user, User* userdest, char* comment, char* channel){
    long errCheck = IRCERR_NOUSER;
    char *message, *complexUser;

    errCheck = user_complex (user, &complexUser);
    if (errCheck == IRC_OK){
        errCheck = IRCMsg_Kick (&message, complexUser,
                                channel, userdest->nick, comment);
        if (errCheck == IRC_OK){
            irc_debug ("%s %s Send %s", DEBUGTEXT, __FUNCTION__, message);
            errCheck = channel_send (message, channel, user);
            tcp_server_send(user->socket, message);
            tcp_server_send(userdest->socket, message);
        }
    }
    IRC_MFree(2, &message, &complexUser);
    return errCheck;
}

long reply_uniqopis 			(){
    return OK;
}

long reply_topic (User *user, char* channel, char* topic){
    char* message;
    long errCheck = IRCERR_NOUSER;
    int tcpErr;

    errCheck = IRCMsg_RplTopic (&message, server_get_name(),
                                user->nick, channel, topic);
    if (errCheck == IRC_OK){
        irc_debug ("%s %s Send %s", DEBUGTEXT, __FUNCTION__, message);
        tcpErr = tcp_server_send(user->socket, message);
	    errCheck = (tcpErr == TCP_OK)? IRC_OK : IRCERR_INVALIDSOCKET;
        free(message);
    }
    return errCheck;
}


long reply_exceptlist 		(){
    return OK;
}

long reply_invitelist 		(){
    return OK;
}

long reply_inviting 			(){
    return OK;
}

long reply_adminme 			(){
    return OK;
}

long reply_adminloc1 		(){
    return OK;
}

long reply_adminloc2 		(){
    return OK;
}

long reply_adminemail 		(){
    return OK;
}

long reply_info 				(){
    return OK;
}

long reply_endoflinks 		(){
    return OK;
}

long reply_endofinfo 		(){
    return OK;
}

long reply_endofmotd 		(){
    return OK;
}

long reply_endofstats 		(){
    return OK;
}

long reply_links 			(){
    return OK;
}

long reply_luserchannels 	(){
    return OK;
}

long reply_luserclient 		(){
    return OK;
}

long reply_luserme 			(){
    return OK;
}

long reply_luserop 			(){
    return OK;
}

long reply_luserunknown 		(){
    return OK;
}

long reply_endofbanlist 		(){
    return OK;
}

long reply_endofexceptlist 	(){
    return OK;
}

long reply_endofinvitelist 	(){
    return OK;
}

long reply_part (User* user, char* channel, char* msg){
    long errCheck = IRCERR_NOUSER;
    int tcpErr;
    char *message=NULL, *complexUser=NULL;

    errCheck = user_complex (user, &complexUser);
    if (errCheck == IRC_OK){
        errCheck = IRCMsg_Part(&message, complexUser, channel, msg);
        if (errCheck == IRC_OK){
            irc_debug ("%s %s send to channel %s", DEBUGTEXT, __FUNCTION__, message);
            errCheck =  channel_send (message, channel, user);
            tcpErr = tcp_server_send(user->socket, message);
            errCheck = (tcpErr == TCP_OK)? IRC_OK : IRCERR_INVALIDSOCKET;
        }
    }
    IRC_MFree(2, &complexUser, &message);
    return errCheck;
}

long reply_youreservice 		(){
    return OK;
}

long reply_myinfo 			(){
    return OK;
}

long reply_endofwho 			(){
    return OK;
}

long reply_endofwhowas 		(){
    return OK;
}

long reply_whowasuser 		(){
    return OK;
}

long reply_banlist 			(){
    return OK;
}

long reply_motd_start (User* user){
    long errCheck = IRCERR_NOUSER;
    char* message;
    int tcpErr;

    if (user){
        errCheck = IRCMsg_RplMotdStart (&message, server_get_name(),
                                        user->nick, server_get_name());
        if (errCheck == IRC_OK){
            irc_debug ("%s %s Send %s", DEBUGTEXT, __FUNCTION__, message);
            tcpErr = tcp_server_send(user->socket, message);
            errCheck = (tcpErr == TCP_OK)? IRC_OK : IRCERR_INVALIDSOCKET;
            free(message);
        }
    }
    return errCheck;
}

long reply_motd (User *user){
    long errCheck = IRCERR_NOUSER;
    int tcpErr;
    FILE *filedesc;
    char* message, *pos;
    char readbuff[MOTD_LEN];

    filedesc = fopen(server_get_motd_file(), "r");
    if (filedesc){
        tcpErr = TCP_OK;
        while (fgets(readbuff, MOTD_LEN, filedesc) && (tcpErr == TCP_OK)){
            pos = strchr(readbuff, '\n');
            if (pos != NULL){
                *pos = '\0';
            }
            errCheck = IRCMsg_RplMotd (&message, server_get_name(),
                                        user->nick, readbuff);
            if (errCheck == IRC_OK){
                irc_debug ("%s %s Send %s", DEBUGTEXT, __FUNCTION__, message);
                tcpErr = tcp_server_send(user->socket, message);
                errCheck = (tcpErr == TCP_OK)? IRC_OK : IRCERR_INVALIDSOCKET;
                free(message);
            }
        }
        errCheck = (tcpErr == TCP_OK)? IRC_OK : IRCERR_INVALIDSOCKET;
        fclose (filedesc);
    } else {
        irc_debug ("%s %s Cannot find MOTD file.", DEBUGTEXT, __FUNCTION__);
        errCheck = IRCERR_NOFILE;
    }
    return errCheck;
}

long reply_motd_end (User* user){
    long errCheck = IRCERR_NOUSER;
    char* message;
    int tcpErr;

    if (user){
        errCheck = IRCMsg_RplEndOfMotd (&message, server_get_name(), user->nick);
        if (errCheck == IRC_OK){
            irc_debug ("%s %s Send %s", DEBUGTEXT, __FUNCTION__, message);
            tcpErr = tcp_server_send(user->socket, message);
            errCheck = (tcpErr == TCP_OK)? IRC_OK : IRCERR_INVALIDSOCKET;
            free(message);
        }
    }
    return errCheck;
}

long reply_statscommands 	(){
    return OK;
}

long reply_statslinkinfo 	(){
    return OK;
}

long reply_statsoline 		(){
    return OK;
}

long reply_statsuptime 		(){
    return OK;
}

long reply_time 				(){
    return OK;
}

long reply_traceclass 		(){
    return OK;
}

long reply_traceconnect 		(){
    return OK;
}

long reply_traceconnecting 	(){
    return OK;
}

long reply_tracehandshake 	(){
    return OK;
}

long reply_tracelink 		(){
    return OK;
}

long reply_tracenewtype 		(){
    return OK;
}

long reply_traceoperator 	(){
    return OK;
}

long reply_traceserver 		(){
    return OK;
}

long reply_traceservice 		(){
    return OK;
}

long reply_traceuser 		(){
    return OK;
}

long reply_traceunknown 		(){
    return OK;
}

long reply_tracelog 			(){
    return OK;
}

long reply_traceend 			(){
    return OK;
}

long reply_version 			(){
    return OK;
}

long reply_servlist 			(){
    return OK;
}

long reply_servlistend 		(){
    return OK;
}

long reply_endofusers 		(){
    return OK;
}

long reply_ison 				(){
    return OK;
}

long reply_nousers 			(){
    return OK;
}

long reply_pong (User* user, char* server){
    long errCheck = IRCERR_NOUSER;
    char* message;
    int tcpErr;

    if (user){
        errCheck = IRCMsg_Pong (&message, server_get_name(),
                                server_get_name(), NULL, server);
        if (errCheck == IRC_OK){
            irc_debug ("%s %s Send %s", DEBUGTEXT, __FUNCTION__, message);
            tcpErr = tcp_server_send(user->socket, message);
            errCheck = (tcpErr == TCP_OK)? IRC_OK : IRCERR_INVALIDSOCKET;
            free(message);
        }
    }
    return errCheck;
}

long reply_ping (User* user, char* server){
    long errCheck = IRCERR_NOUSER;
    char* message;
    int tcpErr;

    if (user){
        errCheck = IRCMsg_Ping (&message, server_get_name(),
                                server_get_name(), server);
        if (errCheck == IRC_OK){
            irc_debug ("%s %s Send %s", DEBUGTEXT, __FUNCTION__, message);
            tcpErr = tcp_server_send(user->socket, message);
            errCheck = (tcpErr == TCP_OK)? IRC_OK : IRCERR_INVALIDSOCKET;
            free(message);
        }
    }
    return errCheck;
}

long reply_nowaway (User *user){
    long errCheck = IRCERR_NOUSER;
    char* message;
    int tcpErr;

    if (user){
        errCheck = IRCMsg_RplNowAway (&message, server_get_name(), user->nick);
        if (errCheck == IRC_OK){
            irc_debug ("%s %s Send %s", DEBUGTEXT, __FUNCTION__, message);
            tcpErr = tcp_server_send(user->socket, message);
            errCheck = (tcpErr == TCP_OK)? IRC_OK : IRCERR_INVALIDSOCKET;
            free(message);
        }
    }
    return errCheck;
}

long reply_rehashing 		(){
    return OK;
}

long reply_summoning 		(){
    return OK;
}

long reply_unaway (User *user){
    long errCheck = IRCERR_NOUSER;
    char* message;
    int tcpErr;

    if (user){
        errCheck = IRCMsg_RplUnaway (&message, server_get_name(), user->nick);
        if (errCheck == IRC_OK){
            irc_debug ("%s %s Send %s", DEBUGTEXT, __FUNCTION__, message);
            tcpErr = tcp_server_send(user->socket, message);
            errCheck = (tcpErr == TCP_OK)? IRC_OK : IRCERR_INVALIDSOCKET;
            free(message);
        }
    }
    return errCheck;
}

long reply_userhost 			(){
    return OK;
}

long reply_users 			(){
    return OK;
}

long reply_usersstart 		(){
    return OK;
}

long reply_nick (User* user, char* newnick){
    long errCheck = IRCERR_NOUSER;
    int tcpErr;
    char *message, *oldnick, *complexUser;

    if (user){
        oldnick = user->nick;
        user->nick = newnick;
        errCheck = user_complex (user, &complexUser);
        if (errCheck == IRC_OK){
            errCheck = IRCMsg_Nick (&message, complexUser, NULL, newnick);
            if (errCheck == IRC_OK){
                irc_debug ("%s %s Send %s", DEBUGTEXT, __FUNCTION__, message);
                tcpErr = tcp_server_send(user->socket, message);
                errCheck = (tcpErr == TCP_OK)? IRC_OK : IRCERR_INVALIDSOCKET;
                free(message);
            }
            free(complexUser);
        }
        user->nick = oldnick;
    }
    return errCheck;
}

long reply_away (User* user, User *userdest){
    long errCheck = IRCERR_NOUSER;
    int tcpErr;
    char * message;

    if (user){
        errCheck = IRCMsg_RplAway (&message, server_get_name(),
                                    user->nick, userdest->nick, userdest->away);
        if (errCheck == IRC_OK){
            irc_debug ("%s %s Send %s", DEBUGTEXT, __FUNCTION__, message);
            tcpErr = tcp_server_send(user->socket, message);
            errCheck = (tcpErr == TCP_OK)? IRC_OK : IRCERR_INVALIDSOCKET;
            free(message);
        }
    }
    return errCheck;
}

long reply_welcome (User* user){
    long errCheck = IRCERR_NOUSER, errCheck2;
    int tcpErr;
    char* message;

    if (user){
        errCheck = IRCMsg_RplWelcome (&message, server_get_name(),
                                    user->nick, user->nick, user->user, user->host);
        if (errCheck == IRC_OK){
            irc_debug ("%s %s Send %s", DEBUGTEXT, __FUNCTION__, message);
            tcpErr = tcp_server_send(user->socket, message);
            errCheck = (tcpErr == TCP_OK)? IRC_OK : IRCERR_INVALIDSOCKET;
            free(message);
            if (errCheck == IRC_OK){
                errCheck = reply_motd_start (user);
                if (errCheck == IRC_OK){
                    errCheck = reply_motd (user);
                    errCheck2 = reply_motd_end (user);
                    errCheck = (errCheck == IRC_OK)? errCheck2 : errCheck;
                }
            }
        }
    }
    return errCheck;
}

long reply_created (User* user){
    long errCheck = IRCERR_NOUSER;
    int tcpErr;
    char* message;

    if (user){
        errCheck = IRCMsg_RplCreated (&message, server_get_name(),
                                        user->nick, server_get_time());
        if (errCheck == IRC_OK){
            irc_debug ("%s %s Send %s", DEBUGTEXT, __FUNCTION__, message);
            tcpErr = tcp_server_send (user->socket, message);
            errCheck = (tcpErr == TCP_OK)? IRC_OK : IRCERR_INVALIDSOCKET;
            free(message);
        }
    }
    return errCheck;
}

long reply_join (User* user, char* channel){
    long errCheck = IRCERR_NOUSER;
    char* complexUser;
    int tcpErr;
    char *message;

    if (user){
        errCheck = user_complex (user, &complexUser);
        if (errCheck == IRC_OK){
            errCheck = IRCMsg_Join (&message, complexUser , NULL, NULL, channel);
            if (errCheck == IRC_OK){
                irc_debug ("%s %s Send %s", DEBUGTEXT, __FUNCTION__, message);
                errCheck = channel_send (message, channel, user);
                tcpErr = tcp_server_send (user->socket, message);
                errCheck = (tcpErr == TCP_OK)? IRC_OK : IRCERR_INVALIDSOCKET;
                free(message);
            }
            free(complexUser);
        }
    }
    return errCheck;
}

long reply_bounce 			(){
    return OK;
}

long reply_tryagain 			(){
    return OK;
}

long reply_yourid 			(){
    return OK;
}

long reply_creationtime 		(){
    return OK;
}

long reply_localusers 		(){
    return OK;
}

long reply_globalusers 		(){
    return OK;
}

long reply_topicwhotime 		(){
    return OK;
}

long reply_channelurl 		(){
    return OK;
}
