/**
 * @file server.c
 * @author Alberto Velasco y Jesualdo Garcia
 * @date 17/02/17
 * @brief Fichero que contiene las funciones del servidor.
 */
#include "G-2362-06-P1-types.h"
#include "G-2362-06-P1-errorreplies.h"

#include "G-2362-06-P1-serverinfo.h"
#include "G-2362-06-P1-connection.h"

#include <stdio.h>
#include <stdlib.h>

#include <redes2/irc.h>
#include <redes2/irctad.h>

#include <unistd.h>
//#include <errno.h>
#include <signal.h>
//#include <pthread.h>
#include <netdb.h>
#include <syslog.h>
//#include <getopt.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
//#include <sys/wait.h>

#include <netinet/in.h>
#include <arpa/inet.h>

#define PORTTCP 6667
#define NOTMSG_STARTING         "Starting up"
#define NOTMSG_CON_POOL_CREATED "Created the connection pools."
#define NOTMSG_DAEMONIZED       "Daemonized."
#define NOTMSG_SERVER_READY     "IRC server is ready to recieve connections."
#define WARMSG_CANNOTPAR        "Error when reading imput parameters."
#define WARMSG_CONPOOL          "Cannot create connection pool."
#define WARMSG_DAEMON           "Cannot daemonize."
#define NOTMSG_SERVER_CLOSE     "Closing server."
#define DEBUG_IS_ACTIVATED      "Debug mode active."

int server_parameters(int argc, char** argv, char** data, int *port){
    long impResult;
    int retVal = OK;

    /* Comprobamos argumentos */
    impResult = IRC_GetOptionsStandard(argc, argv, &(*data), &(*port));
    if (impResult == IRCERR_NOENOUGHMEMORY || impResult == IRCERR_PARAMS){
        if (*data)
            free (*data);
        retVal = ERR;
    } else {
        if(impResult & IRCOPT_SSL){
            flag_add(FLAG_SSL);
        }
        if(impResult & (IRCOPT_SSLDATA | IRCOPT_DATA)){
            flag_add(FLAG_DATA);
        }
        if (!(*port)){ /* Si no se define puerto, vamos aqui.*/
            *port = PORTTCP;
        }
        if(impResult & IRCOPT_VERBOSE){
            flag_add(FLAG_VERBOSE);
        }
        if(impResult & IRCOPT_DEBUG){
            flag_add(FLAG_DEBUG);
        }
        if(impResult & IRCOPT_HELP){
            return OK;
        }
    }
    return retVal;
}

int do_daemon(void){

	int num_ficheros = 0;
	int i = 0;
    int priority = LOG_NOTICE;

    pid_t pid;
    pid = fork();

    if (pid < 0) return ERR;
    if (pid > 0) exit(EXIT_SUCCESS); //Termina proceso padre


    if (flag_check(FLAG_DEBUG)) priority = LOG_DEBUG;
    else if (flag_check(FLAG_VERBOSE)) priority = LOG_INFO;

    umask(0); //Todos los ficheros creados van a tener permiso     //setlogmask (LOG_UPTO (LOG_INFO)); //Open logs here
    setlogmask(LOG_UPTO(priority)); /*Esta mascara hace que el log ignore los mensajes de prioridad LOG_DEBUG o inferior*/
    openlog (server_get_name(), LOG_CONS | LOG_PID | LOG_NDELAY, LOG_LOCAL3);

    if (setsid()< 0) { //Create a new SID for the child process
        return ERR;
    }
    if ((chdir("/")) < 0) { //Change the current working directory
    	return ERR;
    }

    num_ficheros = getdtablesize();
    for (i = 0; i < num_ficheros; ++i)
    {
    	close(i);
    }
    flag_add(FLAG_DAEMON);
    return 0;
}

void close_server (int signumber){

    irc_notice ("%s\n", NOTMSG_SERVER_CLOSE);
    connect_end();
    closelog();
    exit(0);
}

int main(int argc, char** argv){
    char* data = NULL;
    int port;

    /*Reading config. file.*/
    server_read_conf(argv[0]);
    server_show_attrs();

    /*Reading program parameters*/
    irc_notice ("%s %s\n", NOTMSG_STARTING, argv[0]);
    if (server_parameters(argc, argv, &data, &port) != OK){
        irc_warning("%s\n", WARMSG_CANNOTPAR);
        return ERR;
    }

    /*Setting up signal handlers to finish program.*/
    signal(SIGPIPE, SIG_IGN);
    signal(SIGTERM, close_server);

    /*Daemonizing.*/
    if (flag_check (FLAG_WILLDAEMON)){
        if (do_daemon() == OK){
            irc_notice ("%s %s\n", argv[0], NOTMSG_DAEMONIZED);
        } else {
            irc_warning("%s\n", WARMSG_DAEMON);
            return ERR;
        }
    } else {
        signal(SIGINT, close_server);
    }

    /*Creating connection.*/
    if (connect_create(port) == OK){
        irc_notice ("%s\n", NOTMSG_CON_POOL_CREATED);
    } else {
        irc_warning("%s\n", WARMSG_CONPOOL);
        return ERR;
    }

    irc_debug ("%s", DEBUG_IS_ACTIVATED);

    /*Starting server.*/
    irc_notice ("%s\n", NOTMSG_SERVER_READY);
    while (connect_in() != ERR);

    connect_end();
    closelog();

    return ERR;
}
