#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <redes2/irc.h>
#include <redes2/irctad.h>

#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>
#include <pthread.h>
#include <netdb.h>
#include <syslog.h>

#include "G-2362-06-P1-command.h"

#define STRERR_UNEX_QUIT 	"The user has closed the connection unexpectedly."
#define MAX_MODES 			3


/**
 * @name Server Commands
 * 
 */
//@{
 /**
  * Realiza las operaciones necesarias para el comando PASS.
  *
  * El comando pass permite introducir una contrasenya para la conexion.
  *
  * @param  command Comando recibido.
  * @param  user  	Usuario correspondiente a la conexion.
  * @return         IRC_OK en caso de exito, .
  */
long cmd_pass (char* command, User *user){
	long errCheck;
	char *prefix, *password;

	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	errCheck = IRCParse_Pass (command, &prefix, &password);
	if (errCheck == IRC_OK){
		if (user->userId){
			errCheck = error_alreadyregistered (user);
        } else if (!user->nick && !user->user){
			user->password = password;
		}
	} else if (errCheck == IRCERR_ERRONEUSCOMMAND){
		errCheck = error_needmoreparams (user, command);
	}
	IRC_MFree(1, &prefix);

	irc_debug ("%s end %s", DEBUGTEXT, __FUNCTION__);
	return errCheck;
}

/**
 * Realiza las operaciones necesarias para el comando USER.
 *
 * El comando USER anyade el nombre del usuario. Esto esta ya
 * tratado de manera especial, as&iacute; que actualmente solo devuelve el
 * errorr ALREADYREGISTERED.
 *
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_user (char* command, User *user){
	long errCheck;
	char *complexUser;
	char *newUser = NULL, *modeHost = NULL, *realName = NULL;
	char *prefix = NULL, *serverOther = NULL;

	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	errCheck = IRCParse_User (command, &prefix, &newUser,
                	&modeHost, &serverOther, &realName);
	if (errCheck == IRC_OK){
		if (user->userId || user->user){
			errCheck = error_alreadyregistered (user);
		} else {
			user->user = newUser;
			user->real = realName;
			if (user->nick){
				irc_debug ("%s %s", DEBUGTEXT, "Creating user");
				errCheck = user_new (user);
				if (errCheck == IRC_OK){
					user_complex (user, &complexUser);
	                irc_notice("\nUser created %s", complexUser);
					reply_welcome (user);
					free(complexUser);
					errCheck = FINISHED;
				} else {
					irc_error (errCheck, LOG_ERR, STRERR_CANNOT_USER);
					if (errCheck == IRCERR_NICKUSED){
						errCheck = error_nicknameinuse (user, user->nick);
					} else {
						errCheck = error_alreadyregistered (user);
					}
				}
			}
		}
	} else if (errCheck == IRCERR_ERRONEUSCOMMAND){
		errCheck = error_needmoreparams (user, command);
	}
	IRC_MFree(3, &prefix, &serverOther, &modeHost);

	irc_debug ("%s end %s", DEBUGTEXT, __FUNCTION__);
	return errCheck;
}

/**
 * Realiza las operaciones necesarias para el comando NICK.
 *
 * Cambia el nick que parsea desde el command de entrada y lo actualiza al
 * usuario al que le coresponde el socket.
 *
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_nick (char* command, User *user) {
	long errCheck;
	char *prefix = NULL, *msg=NULL, *newnick=NULL, *complexUser = NULL;

	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	errCheck = IRCParse_Nick(command, &prefix, &newnick, &msg);
	if (errCheck == IRC_OK){
		if (user->userId){
			errCheck = user_set (user, NULL, newnick, NULL);
			if (errCheck == IRC_OK){
				errCheck = reply_nick (user, newnick);
			} else {
				errCheck = error_nicknameinuse (user, newnick);
			}
			free(newnick);
		} else {
			if(user->nick) {
				free(user->nick);
			}
			user->nick = newnick;
			irc_debug("userId %d, user %s, nick %s, real %s", user->userId, user->user, user->nick, user->real);
			if (user->user && user->real){
				errCheck = user_new (user);
				if (errCheck == IRC_OK){
					user_complex (user, &complexUser);
	                irc_notice("\nUser created %s", complexUser);
					reply_welcome (user);
					free(complexUser);
					errCheck = FINISHED;
				} else {
					errCheck = error_nicknameinuse (user, newnick);
				}
			}
		}
	} else if (errCheck == IRCERR_ERRONEUSCOMMAND){
		errCheck = error_needmoreparams (user, command);
	}
	IRC_MFree(3, &prefix, &msg, &complexUser);

	irc_debug ("%s end %s", DEBUGTEXT, __FUNCTION__);
	return errCheck;
}


/**
 * Realiza las operaciones necesarias para el comando OPER.
 *
 * NO IMPLEMENTADO
 *
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_oper (char* command, User *user){
	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	return OK;
}

long cmd_error (char* command, User *user){
	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	return OK;
}
/**
 * Realiza las operaciones necesarias para el comando MODE.
 *
 * NOTA: Solo est&aacute; implementada la parte que cambia el modo de un canal.
 * Cambia el modo de un canal.
 *
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_mode (char* command, User *user){
    long errCheck;
    char *prefix=NULL,*channeluser=NULL,*mode=NULL,*uname=NULL,*modestring=NULL;
    char plusModestring[MAX_MODES + 2] = "+";

	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	errCheck = IRCParse_Mode (command, &prefix, &channeluser, &mode, &uname);
	if (errCheck == IRC_OK){
		if (channeluser[0] == '#' || channeluser[0] == '&'){
			if (!mode){
				modestring = IRCTADChan_GetModeChar(channeluser);
				strncat(plusModestring, modestring, MAX_MODES);
				errCheck = reply_channelmodeis (user, channeluser, plusModestring);
			} else {
				errCheck = IRCTAD_Mode (channeluser, user->nick, mode);
				switch (errCheck) {
					case IRC_OK:
						modestring = IRCTADChan_GetModeChar(channeluser);
						strncat(plusModestring, modestring, MAX_MODES);
						if (!strcmp(mode, "\\+k")){
							errCheck = IRCTADChan_SetPassword(channeluser, uname);
						}
						if (errCheck == IRC_OK){
							errCheck = reply_channelmodechange (user,
												channeluser, plusModestring);
						}
					break;
					case IRCERR_INVALIDCHANNELNAME:
						errCheck =  error_nosuchchannel (user, channeluser);
					break;
					case IRCERR_ISNOTOPERATOR:
						errCheck = error_chanoprivsneeded (user, channeluser);
					break;
					case IRCERR_NOUSERONCHANNEL:
						errCheck = error_notonchannel (user, user->nick, channeluser);
					break;
				}
			}
		}
	} else if (errCheck == IRCERR_ERRONEUSCOMMAND){
		errCheck = error_needmoreparams (user, command);
	}
	IRC_MFree(5, &prefix, &uname, &channeluser, &mode, &modestring);

	irc_debug ("%s end %s", DEBUGTEXT, __FUNCTION__);
	return errCheck;
}

/**
 * Realiza las operaciones necesarias para el comando SERVICE.
 *
 * NO IMPLEMENTADA.
 *
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_service (char* command, User *user){
	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	return OK;
}

/**
 * En caso de que se detecte una conexion inesperada por parte del cliente,
 * este comando avisa al resto de servidores y cierra la conexion.
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_unexpected_quit (User *user){
	long errCheck;

	errCheck = reply_quit (user, NULL);
	if (errCheck != IRC_OK){
		irc_error(errCheck, LOG_ERR, STRERR_UNEX_QUIT);
	}
	IRCTAD_Quit (user->nick);
	return FINISHED;
}

/**
 * Realiza las operaciones necesarias para el comando QUIT.
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_quit (char* command, User *user){
    long errCheck;
    char *complexUser=NULL, *prefix=NULL, *msg=NULL;

	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
    errCheck = IRCParse_Quit (command, &prefix, &msg);
    if (errCheck == IRC_OK){
        errCheck = reply_quit (user, msg);
		if (errCheck == IRC_OK){
			IRCTAD_Quit (user->nick);
			errCheck = FINISHED;
		}
    }
	IRC_MFree(3, &prefix, &msg, &complexUser);

	irc_debug ("%s end %s", DEBUGTEXT, __FUNCTION__);
	return errCheck;
}

/**
 * Realiza las operaciones necesarias para el comando SQUIT.
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_squit (char* command, User *user){
	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	return OK;
}

/**
 * Realiza las operaciones necesarias para el comando JOIN.
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_join (char* command, User *user){
	long errCheck;
	char *channel=NULL, *password=NULL, *msg=NULL, *prefix=NULL;
    char* topic;

	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	errCheck = IRCParse_Join (command, &prefix, &channel, &password, &msg);
	//if (errCheck == IRC_OK){
		//errCheck = IRC_IsValid (channel, 0, NULL, IRC_CHANNELNAME);
	if (errCheck == IRC_OK){
		errCheck = IRCTAD_Join (channel, user->nick, NULL, password);
		switch (errCheck) {
			case IRC_OK:
				errCheck = reply_join (user, channel);
                if (errCheck == IRC_OK){
                    errCheck = IRCTAD_GetTopic (channel, &topic);
                    if (errCheck == IRC_OK){
                        errCheck = reply_topic(user, channel, topic);
                        free(topic);
                    } else {
						errCheck = reply_notopic (user, channel);
					}
                    errCheck = reply_namreply(user, channel);
                    if (errCheck == IRC_OK){
                        errCheck = reply_endofnames (user, channel);
                    }
                }
			break;
			case IRCERR_FAIL:
				errCheck = error_badchannelkey (user, channel);
			break;
			case IRCERR_NOVALIDCHANNEL:
				errCheck = error_nosuchchannel (user, channel);
			break;
			case IRCERR_USERSLIMITEXCEEDED:
				errCheck = error_channelisfull (user, channel);
			break;
			case IRCERR_BANEDUSERONCHANNEL:
				errCheck = error_bannedfromchan (user, channel);
			break;
			case IRCERR_NOINVITEDUSER:
				errCheck = error_inviteonlychan (user, channel);
			break;
		}
	} else if (errCheck == IRCERR_ERRONEUSCOMMAND){
		errCheck = error_needmoreparams (user, command);
	}
	IRC_MFree(4, &prefix, &channel, &password, &msg);

	irc_debug ("%s end %s", DEBUGTEXT, __FUNCTION__);
	return errCheck;
}


/**
 * Realiza las operaciones necesarias para el comando PART.
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_part (char* command, User *user){
	long errCheck;
	char *prefix=NULL, *channel=NULL, *msg=NULL;

	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	errCheck = IRCParse_Part (command, &prefix, &channel, &msg);
	if (errCheck == IRC_OK){
		errCheck = IRCTAD_TestChannelOfUser (channel, user->nick);
		switch (errCheck) {
			case IRC_OK:
				reply_part(user, channel, msg);
				errCheck = IRCTAD_Part (channel, user->nick);
			break;
			case IRCERR_NOVALIDCHANNEL:
				errCheck = error_nosuchchannel (user, channel);
			break;
			case IRCERR_NOVALIDUSER:
			case IRCERR_FAIL:
				errCheck = error_notonchannel (user, user->nick, channel);
			break;
		}
	} else if (errCheck == IRCERR_ERRONEUSCOMMAND){
		errCheck = error_needmoreparams (user, command);
	}
	IRC_MFree(3, &prefix, &msg, &channel);
	irc_debug ("%s end %s", DEBUGTEXT, __FUNCTION__);
	return errCheck;
}

/**
 * Realiza las operaciones necesarias para el comando TOPIC.
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_topic (char* command, User *user){
	long errCheck;
    char *prefix=NULL, *channel=NULL, *topic=NULL;

	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	errCheck = IRCParse_Topic (command, &prefix, &channel, &topic);
	if (errCheck == IRC_OK){
		if (topic){
			errCheck = IRCTAD_SetTopic (channel, user->nick, topic);
			if (errCheck == IRC_OK){
				errCheck = reply_newtopic (user, channel, topic);
			} else if (errCheck == IRCERR_INVALIDUSER){
				if (IRCTAD_TestChannelOfUser (channel, user->nick)){
					errCheck = error_notonchannel (user, user->nick, channel);
				} else {
					errCheck = error_chanoprivsneeded (user, channel);
				}
			} else if (IRCERR_INVALIDCHANNELNAME == errCheck){
				errCheck = error_nosuchchannel  (user, channel);
			}
		} else {
			errCheck = IRCTAD_GetTopic (channel, &topic);
			if (errCheck == IRC_OK){
				if (topic){
					errCheck = reply_topic (user, channel, topic);
                } else {
					errCheck = reply_notopic (user, channel);
				}
			}
		}
	} else if (errCheck == IRCERR_ERRONEUSCOMMAND){
		errCheck = error_needmoreparams (user, command);
	}
	IRC_MFree(3, &prefix, &channel, &topic);

	irc_debug ("%s end %s", DEBUGTEXT, __FUNCTION__);
	return errCheck;
}

/**
 * Realiza las operaciones necesarias para el comando NAMES.
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_names (char* command, User *user){
    long errCheck;
    char *prefix=NULL, *channel=NULL, *target=NULL;

	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
    errCheck = IRCParse_Names (command, &prefix, &channel, &target);
    if (errCheck == IRC_OK){
		errCheck = reply_namreply (user, channel);
		if (errCheck == IRC_OK || errCheck == IRCERR_NOVALIDCHANNEL){
			errCheck = reply_endofnames (user, channel);
		}
		IRC_MFree(3, &prefix, &channel, &target);
	} else if (errCheck == IRCERR_NOPARAMS){
		errCheck = error_needmoreparams (user, command);
	}

	irc_debug ("%s end %s", DEBUGTEXT, __FUNCTION__);
	return errCheck;
}

/**
 * Realiza las operaciones necesarias para el comando LIST.
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_list (char* command, User *user){
	long errCheck;
	char *target = NULL, *prefix = NULL, *channel = NULL;

	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	errCheck = IRCParse_List (command, &prefix, &channel, &target);
	if (errCheck == IRC_OK){
		errCheck = reply_lists(user, channel);
		if (errCheck == IRC_OK){
			errCheck = reply_listend(user);
		}
	} else if (errCheck == IRCERR_NOPARAMS){
		errCheck = error_needmoreparams (user, command);
	}
	IRC_MFree(3, &target, &prefix, &channel);

	irc_debug ("%s end %s", DEBUGTEXT, __FUNCTION__);
	return errCheck;
}

/**
 * Realiza las operaciones necesarias para el comando INVITE.
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_invite (char* command, User *user){
	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	return OK;
}

/**
 * Realiza las operaciones necesarias para el comando KICK.
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_kick (char* command, User *user){
	long errCheck, chanMod;
	char *prefix, *channel, *comment, *kickeduser;
	User userDest;

	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	errCheck = IRCParse_Kick(command, &prefix, &channel, &kickeduser, &comment);
	if (errCheck == IRC_OK){
		chanMod = IRCTAD_GetUserModeOnChannel (channel, user->nick);
		if ((chanMod & IRCUMODE_CREATOR) ||
			(chanMod & IRCUMODE_OPERATOR) ||
			(chanMod & IRCUMODE_LOCALOPERATOR))
		{
			if (!comment){
				comment = user->nick;
			}
			errCheck = IRCTAD_KickUserFromChannel (channel, kickeduser);
			switch (errCheck) {
				case IRC_OK:
					user_zero(&userDest);
					userDest.nick = kickeduser;
					if (user_find(&userDest) == IRC_OK){
						errCheck = reply_kicked (user, &userDest, comment, channel);
					}
					userDest.nick = NULL;
					user_clear(&userDest);
				break;
				case IRCERR_NOVALIDUSER:
					errCheck = error_notonchannel (user, kickeduser, channel);
				break;
				case IRCERR_NOVALIDCHANNEL:
					errCheck = error_nosuchchannel (user, channel);
				break;
			}
		} else {
			errCheck = error_chanoprivsneeded (user, channel);
		}
	} else if (errCheck == IRCERR_NOPARAMS){
		errCheck = error_needmoreparams (user, command);
	}
	IRC_MFree (4, &prefix, &channel, &comment, &kickeduser);

	irc_debug ("%s end %s", DEBUGTEXT, __FUNCTION__);
	return errCheck;
}


/**
 * Realiza las operaciones necesarias para el comando PRIVMSG.
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_privmsg (char* command, User *user){
	long errCheck;
	char *prefix, *msgtarget, *msg;

	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	errCheck = IRCParse_Privmsg (command, &prefix, &msgtarget, &msg);
	switch (errCheck) {
		case IRC_OK:
		irc_debug ("%s parseado %s", DEBUGTEXT, __FUNCTION__);
			if (msgtarget[0] <= 'z' && msgtarget[0] >= 'A'){
				errCheck = reply_privmsg_user (user, msgtarget, msg);
				if (errCheck != IRC_OK){
					errCheck = error_nosuchnick(user, msgtarget);
				}
			} else {
                if (IRCTAD_TestUserOnChannel (msgtarget, user->nick)==IRC_OK){
    				errCheck = reply_privmsg_chan (user, msgtarget, msg);
    				if (errCheck != IRC_OK){
    					errCheck = error_cannotsendtochan (user, msgtarget);
    				}
                }
			}
		break;
		case IRCERR_NOTARGET:
			errCheck = error_norecipient (user, command);
		break;
		case IRCERR_NOMESSAGE:
			errCheck = error_notexttosend(user);
		break;
		case IRCERR_ERRONEUSCOMMAND:
			errCheck = error_needmoreparams (user, command);
		break;
	}
	IRC_MFree (3, &prefix, &msgtarget, &msg);

	irc_debug ("%s end %s", DEBUGTEXT, __FUNCTION__);
	return errCheck;
}

/**
 * Realiza las operaciones necesarias para el comando ERRSEV_NOTICE.
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_notice (char* command, User *user){
	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	return OK;
}

/**
 * Realiza las operaciones necesarias para el comando MOTD.
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_motd (char* command, User *user){
	long errCheck, errCheck2;
	char *prefix, *target;

	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	errCheck = IRCParse_Motd(command, &prefix, &target);
	if (errCheck == IRC_OK){
		errCheck = reply_motd_start (user);
		if (errCheck == IRC_OK){
			errCheck = reply_motd (user);
			errCheck2 = reply_motd_end (user);
			errCheck = (errCheck == IRC_OK)? errCheck2 : errCheck;
		}
		IRC_MFree (2, &prefix, &target);
	} else if (errCheck == IRCERR_NOPARAMS){
		errCheck = error_needmoreparams (user, command);
	}

	irc_debug ("%s end %s", DEBUGTEXT, __FUNCTION__);
	return errCheck;
}

/**
 * Realiza las operaciones necesarias para el comando LUSERS.
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_lusers (char* command, User *user){
	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	return OK;
}

/**
 * Realiza las operaciones necesarias para el comando VERSION.
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_version (char* command, User *user){
	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	return OK;
}

/**
 * Realiza las operaciones necesarias para el comando STATS.
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_stats (char* command, User *user){
	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	return OK;
}

/**
 * Realiza las operaciones necesarias para el comando LINKS.
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_links (char* command, User *user){
	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	return OK;
}

/**
 * Realiza las operaciones necesarias para el comando TIME.
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_time (char* command, User *user){
	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	return OK;
}

/**
 * Realiza las operaciones necesarias para el comando CONNECT.
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_connect (char* command, User *user){
	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	return OK;
}

/**
 * Realiza las operaciones necesarias para el comando TRACE.
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_trace (char* command, User *user){
	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	return OK;
}

/**
 * Realiza las operaciones necesarias para el comando ADMIN.
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_admin (char* command, User *user){
	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	return OK;
}

/**
 * Realiza las operaciones necesarias para el comando INFO.
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_info (char* command, User *user){
	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	return OK;
}

/**
 * Realiza las operaciones necesarias para el comando SERVLIST.
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_servlist (char* command, User *user){
	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	return OK;
}

/**
 * Realiza las operaciones necesarias para el comando SQUERY.
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_squery (char* command, User *user){
	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	return OK;
}

/**
 * Realiza las operaciones necesarias para el comando WHO.
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_who (char* command, User *user){
    long errCheck, numChan = 0, numUsers = 0;
    long i, j;
    char *prefix, *mask, *oppar;
    char **chanList = NULL, **userList = NULL;
    char maskChannel[CHAN_NAME_LEN + 3], *inputMask;
    User userDest;

	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
    if (user){
        errCheck = IRCParse_Who (command, &prefix, &mask, &oppar);
        if (errCheck == IRC_OK){

			if (mask){
            	sprintf(maskChannel, "*%s*", mask);
			} else {
				sprintf(maskChannel, ">0");
			}
			inputMask = maskChannel;

			errCheck = IRCTADChan_GetList (&chanList, &numChan, inputMask);
            if (errCheck == IRC_OK){
			    for (i = 0 ; i < numChan - 1; i++){
	                errCheck = IRCTAD_ListNicksOnChannelArray (chanList[i],
									&userList, &numUsers);
					if (errCheck == IRC_OK){
						for (j = 0; j < numUsers; j++){
	                        user_zero (&userDest);
	                        userDest.nick = userList[j];
	                        user_find (&userDest);
                        	errCheck = reply_whoreply (user, &userDest,
										chanList[i], oppar, inputMask);
							userDest.nick = NULL;
							user_clear(&userDest);
	                    }
						IRCTADUser_FreeList (userList, numUsers);
					}
                }
				IRCTADChan_FreeList (chanList, numChan - 1);
            }
        } else if (errCheck == IRCERR_ERRONEUSCOMMAND){
 		    errCheck = error_needmoreparams (user, command);
        }
    }
	IRC_MFree (3, &oppar, &mask, &prefix);
    irc_debug ("%s end %s", DEBUGTEXT, __FUNCTION__);
	return errCheck;
}

/**
 * Realiza las operaciones necesarias para el comando WHOIS.
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_whois (char* command, User *user){
	long errCheck, nummasks, numusers;
	char *prefix, *target, *maskarray;
	char **masklist, **nicklist;
	register int  i, j;
	User targetuser;

	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	errCheck = IRCParse_Whois (command, &prefix, &target, &maskarray);
	if (errCheck == IRC_OK){
		errCheck = IRCParse_ParseLists (maskarray, &masklist, &nummasks);
		if (errCheck == IRC_OK){
			errCheck = IRCTADUser_GetNickList (&nicklist, &numusers);
			if (errCheck == IRC_OK){
				for (j=0; j < numusers; j++){
					for (i = 0; i < nummasks; i++){
						if (user_mask(nicklist[j], masklist[i])){
							user_zero(&targetuser);
							targetuser.nick = nicklist[j];
							user_find(&targetuser);
                        	errCheck = reply_whois (user, &targetuser);
							targetuser.nick = NULL;
							user_clear (&targetuser);
						}
					}
                }
			}
			IRCTADUser_FreeList (nicklist, numusers);
		}
		IRCTADChan_FreeMaskList (masklist, nummasks);
    } else if (errCheck == IRCERR_ERRONEUSCOMMAND){
        errCheck = error_nonicknamegiven (user);
    }
	IRC_MFree (3, &maskarray, &target, &prefix);

	irc_debug ("%s end %s", DEBUGTEXT, __FUNCTION__);
	return errCheck;
}

/**
 * Realiza las operaciones necesarias para el comando WHOWAS.
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_whowas (char* command, User *user){
	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	return OK;
}

/**
 * Realiza las operaciones necesarias para el comando KILL.
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_kill (char* command, User *user){
	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	return OK;
}

/**
 * Realiza las operaciones necesarias para el comando PING.
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_ping (char* command, User *user){
	long errCheck;
	char *prefix, *server, *server2, *msg;

	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	errCheck = IRCParse_Ping (command, &prefix, &server, &server2, &msg);
	if (errCheck == IRC_OK){
		errCheck = reply_pong(user, server);
	} else {
		error_noorigin(user, msg);
	}
	IRC_MFree (3, &server, &prefix, &server2);

	irc_debug ("%s end %s", DEBUGTEXT, __FUNCTION__);
	return errCheck;
}

/**
 * Realiza las operaciones necesarias para el comando PONG.
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_pong (char* command, User *user){
	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	return IRC_OK;
}

/**
 * Realiza las operaciones necesarias para el comando AWAY.
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_away (char* command, User *user){
	long errCheck;
	char *prefix, *msg;

	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	errCheck = IRCParse_Away (command, &prefix, &msg);
	if (errCheck == IRC_OK){
		errCheck = IRCTADUser_SetAway (user->userId, user->user,
									user->nick, user->real, msg);
		if (msg){
			errCheck = reply_nowaway (user);
		} else {
			errCheck = reply_unaway (user);
		}
		IRC_MFree (2, &prefix, &msg);
	} else if (errCheck == IRCERR_NOPARAMS){
		errCheck = error_needmoreparams (user, command);
	}

	irc_debug ("%s end %s", DEBUGTEXT, __FUNCTION__);
	return errCheck;
}

/**
 * Realiza las operaciones necesarias para el comando REHASH.
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_rehash (char* command, User *user){
	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	return OK;
}

/**
 * Realiza las operaciones necesarias para el comando DIE.
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_die (char* command, User *user){
	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	return OK;
}

/**
 * Realiza las operaciones necesarias para el comando RESTART.
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_restart (char* command, User *user){
	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	return OK;
}

/**
 * Realiza las operaciones necesarias para el comando SUMMON.
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_summon (char* command, User *user){
	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	return OK;
}

/**
 * Realiza las operaciones necesarias para el comando USERS.
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_users (char* command, User *user){
	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	return OK;
}

/**
 * Realiza las operaciones necesarias para el comando WALLOPS.
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_wallops (char* command, User *user){
	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	return OK;
}

/**
 * Realiza las operaciones necesarias para el comando USERHOST.
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_userhost (char* command, User *user){
	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	return OK;
}

/**
 * Realiza las operaciones necesarias para el comando ISON.
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_ison (char* command, User *user){
	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	return OK;
}

/**
 * Realiza las operaciones necesarias para el comando HELP.
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_help (char* command, User *user){
	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	return OK;
}

/**
 * Realiza las operaciones necesarias para el comando RULES.
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_rules (char* command, User *user){
	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	return OK;
}

/**
 * Realiza las operaciones necesarias para el comando SERVER.
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_server (char* command, User *user){
	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	return OK;
}

/**
 * Realiza las operaciones necesarias para el comando ENCAP.
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_encap (char* command, User *user){
	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	return OK;
}

/**
 * Realiza las operaciones necesarias para el comando CNOTICE.
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_cnotice (char* command, User *user){
	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	return OK;
}

/**
 * Realiza las operaciones necesarias para el comando CPRIVMSG.
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_cprivmsg (char* command, User *user){
	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	return OK;
}

/**
 * Realiza las operaciones necesarias para el comando NAMESX.
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_namesx (char* command, User *user){
	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	return OK;
}

/**
 * Realiza las operaciones necesarias para el comando SILENCE.
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_silence (char* command, User *user){
	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	return OK;
}

/**
 * Realiza las operaciones necesarias para el comando UHNAMES.
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_uhnames (char* command, User *user){
	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	return OK;
}

/**
 * Realiza las operaciones necesarias para el comando WATCH.
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_watch (char* command, User *user){
	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	return OK;
}

/**
 * Realiza las operaciones necesarias para el comando KNOCK.
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_knock (char* command, User *user){
	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	return OK;
}

/**
 * Realiza las operaciones necesarias para el comando USERIP.
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_userip (char* command, User *user){
	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	return OK;
}

/**
 * Realiza las operaciones necesarias para el comando SETNAME.
 * @param  command Comando recibido.
 * @param  socket  Socket por el cual se ha recibido.
 * @return         OK en caso de exito, ERR en caso de error.
 */
long cmd_setname (char* command, User *user){
	irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
	return OK;
}
//@}
