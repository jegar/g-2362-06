#include "G-2362-06-P1-serverinfo.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <syslog.h>
#include <string.h>
#include <time.h>
#include <redes2/irc.h> /*irc_error*/

/*Default values*/
#define SERVER_DOMAIN           "irc.com"
#define SERVER_CONFIG_FILE      "config/G-2362-06-P1-server.conf"
#define SERVER_MOTD_FILE        "config/G-2362-06-P1-motd.txt"
#define SERVER_VERS             "0210"
#define SERVER_MOTD_FILE_LEN    250
#define SERVER_INFO_LEN         200
#define SERVER_VERS_LEN         4
#define SERVER_CONFIG_READ_LEN  250

#define STRERR_CONFIG_NOFOUND   "The config file was not found, using default values."

/*Variable de este fichero que manejaran las funciones*/
struct IRC_server_atrbs {
    Flag flags;
    char name[SERVER_NAME_LEN + 1];
    char version[SERVER_VERS_LEN + 1];
    char info[SERVER_INFO_LEN + 1];
    char motdFile [PATH_MAX];
    long t_created;
};

static struct IRC_server_atrbs srvrAttrbs;

void server_show_attrs(){
    char *gmtTime;
    irc_notice("Name: %s\n", server_get_name());
    irc_notice("Version: %s\n", server_get_vers());
    irc_notice("Info: %s\n", server_get_info());
    gmtTime = IRCTAD_TimestampToGMTDate (server_get_time());
    irc_notice("Created on: %s\n", gmtTime);
    free(gmtTime);
}
/**
 * Coloca el nombre del servidor, como un nombre del propio y otro del dominio.
 * @param  name   Nombre del servidor.
 * @param  domain Nombre del dominio donde este se encuentra.
 * @return        OK en caso de exito, ERR en caso de fallo.
 */
int server_set_name(char* name, char* domain){
    if (name && domain){
        if ((strlen(name) + 1 + strlen(domain)) < SERVER_NAME_LEN){
            sprintf(srvrAttrbs.name, "%s.%s", name, domain);
            return OK;
        }
    }
    return ERR;
}

/**
 * Coloca el archivo de MOTD.
 * @param  server_motf Archivo que contiene el MOTD.
 * @return             OK en caso de exito, ERR en caso de fallo.
 */
int server_set_motd_file(char* server_motf){
    FILE *pf;

    if (server_motf){
        pf = fopen(server_motf, "r");
        if (pf){
            realpath(server_motf, srvrAttrbs.motdFile);
        } else {
            realpath(server_motf, srvrAttrbs.motdFile);
            pf = fopen (server_motf, "w");
            fprintf(pf, "Message of the day\n");
        }
        fclose(pf);
        return OK;
    }
    return ERR;
}

/**
 * Coloca la informacion del servidor.
 * @param  serverinfo Cadena de info del servidor de longitud max SERVER_INFO_LEN.
 * @return        OK en caso de exito, ERR en caso de fallo.
 */
int server_set_info(char* serverinfo){
    if (strncpy(srvrAttrbs.info, serverinfo, SERVER_INFO_LEN - 1)){
        return OK;
    }
    return ERR;
}

/**
 * Coloca en los atributos del servidor la hora de creación como actual.
 */
void server_set_time(){
    srvrAttrbs.t_created = time(NULL);
}

/**
* Devuelve el nombre del servidor.
* @return Un puntero a la cadena name.
*/
char* server_get_name(){
    return srvrAttrbs.name;
}

/**
 * Devuelve la cadena de versi&oacute;n del servidor.
 * @return Un puntero a la cadena version.
 */
char* server_get_vers(){
    return srvrAttrbs.version;
}

/**
 * Devuelve el tiempo de creacion.
 * @return El tiempo de creaci&oacute;n.
 */
long server_get_time(){
    return srvrAttrbs.t_created;
}

/**
 * Devuelve la cadena de versi&oacute;n del servidor.
 * @return Un puntero a la cadena info.
 */
char* server_get_info(){
    return srvrAttrbs.info;
}

/**
 * Devuelve el archivo que contiene el MOTD.
 * @return El archivo que contiene el MOTD.
 */
char* server_get_motd_file(){
    return srvrAttrbs.motdFile;
}


/**
 * Lee el archivo de configuracion proporcionado.
 * @param  arg0 Nombre del programa.
 * @return      Ok en caso de exito ERR en caso de error.s
 */
int server_read_conf(char* arg0){
    FILE *config = NULL;
    char server_name    [SERVER_NAME_LEN + 1] = {0};
    char server_domain  [SERVER_NAME_LEN + 1] = SERVER_DOMAIN;
    char server_info    [SERVER_INFO_LEN + 1] = {0};
    char server_motf    [SERVER_MOTD_FILE_LEN + 1] = SERVER_MOTD_FILE;
    char read_string    [SERVER_CONFIG_READ_LEN + 1] = {0};
    char *parameter, *value;

    srvrAttrbs.flags = FLAG_NONE;
    srvrAttrbs.name[0] = '\0';
    strcpy (srvrAttrbs.version, SERVER_VERS);
    srvrAttrbs.info[0] = '\0';
    strcpy(srvrAttrbs.motdFile, SERVER_MOTD_FILE);
    srvrAttrbs.t_created = 0;

    if (!arg0){
        return ERR;
    }
    /* Seting default values */
    strncpy(server_name, arg0, SERVER_NAME_LEN);
    strncpy(server_info, arg0, SERVER_INFO_LEN);

    config = fopen(SERVER_CONFIG_FILE, "r");
    if (config){
        while (fgets(read_string, SERVER_CONFIG_READ_LEN, config)){
            if (read_string[0] != '#' && read_string[0] != '\n'){
                parameter = strtok(read_string, " =");
                value = strtok(NULL, "\n");
                if (value){
                    if (!strcmp(parameter, "server_name")){
                        strncpy(server_name, value, SERVER_NAME_LEN);
                    } else if (!strcmp(parameter, "server_domain")){
                        strncpy(server_domain, value, SERVER_NAME_LEN);
                    } else if (!strcmp(parameter, "server_info")){
                        strncpy(server_info, value, SERVER_INFO_LEN);
                    } else if (!strcmp(parameter, "server_motd")){
                        strncpy(server_motf, value, SERVER_INFO_LEN);
                    } else if (!strcmp(parameter, "server_daemonize_flag")){
                        if (!strcmp(value, "True")){
                            flag_add (FLAG_WILLDAEMON);
                        }
                    }
                }
            }
        }
        fclose(config);
    } else {
        irc_warning ("%s\n", STRERR_CONFIG_NOFOUND);
    }
    server_set_time();
    server_set_name(server_name, server_domain);
    server_set_info(server_info);
    server_set_motd_file(server_motf);
    return OK;
}

/**
 * Activa una (o varias) banderas.
 * @param flagname bandera(s) que comprobar.
 */
void flag_add (Flag flagname){
    srvrAttrbs.flags = srvrAttrbs.flags | flagname;
}

/**
 * Desactiva la bandera.
 * @param flagname Bandera(s) a desactivar.
 */
void flag_del (Flag flagname){
  srvrAttrbs.flags = srvrAttrbs.flags & ~flagname;
}

/**
 * Comprueba si una bandera esta activada o no. Puede usarse para varias, pero
 * devolverá 1 en caso de que alguna de ellas esté activada.
 * @param  flagname bandera(s) que comprobar.
 * @return 1 si alguna de las banderas estan activadas, 0 si no.
 */
int flag_check (Flag flagname){
  if (flagname & srvrAttrbs.flags)
    return True;
 return False;
}

/**
 * Comprueba que todas las banderas que se le pasen en un OR esten activadas.
 * @param  flagname Una o varias banderas a comprobar.
 * @return          1 si están todas activadas, 0 si no.
 */
int flag_check_mul (Flag flagname){
    Flag temp = 0;

    temp = flagname & srvrAttrbs.flags;
    if (temp ^ flagname)
        return False;
    return True;
}
