#include <stdio.h>
#include <stdlib.h>

#include <redes2/irc.h>
#include <redes2/irctad.h>

#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>
#include <pthread.h> //Para los hilos
#include <netdb.h>
#include <syslog.h>

#include "G-2362-06-P1-process.h"

#define MAXDATA     8192
#define IRC_MAXDATA 512
#define MAX_COMMANT 56

#define ERRSTR_PROC_TIMEOUT_FAIL

/* Array de funciones para acceso rapido a los comandos */
typedef long (*parseFunction)(char* mensaje, User* user);

static parseFunction command_functions[] = {
    cmd_pass,     cmd_nick,     cmd_user,      cmd_oper,
    cmd_mode,     cmd_service,  cmd_quit,      cmd_squit,
    cmd_join,     cmd_part,     cmd_topic,     cmd_names,
    cmd_list,     cmd_invite,   cmd_kick,      cmd_privmsg,
    cmd_notice,   cmd_motd,     cmd_lusers,    cmd_version,
    cmd_stats,    cmd_links,    cmd_time,      cmd_connect,
    cmd_trace,    cmd_admin,    cmd_info,      cmd_servlist,
    cmd_squery,   cmd_who,      cmd_whois,     cmd_whowas,
    cmd_kill,     cmd_ping,     cmd_pong,      cmd_error,
    cmd_away,     cmd_rehash,   cmd_die,       cmd_restart,
    cmd_summon,   cmd_users,    cmd_wallops,   cmd_userhost,
    cmd_ison,     cmd_help,     cmd_rules,     cmd_server,
    cmd_encap,    cmd_cnotice,  cmd_cprivmsg,  cmd_namesx,
    cmd_silence,  cmd_uhnames,  cmd_watch,     cmd_knock,
    cmd_userip,   cmd_setname
};

/**
 * Procesa una conexi&oacute;n recibiendo los datos y
 * @param  socket Socket en el que se produce la conexion.
 * @return        ERR en caso de error, OK en caso de exito.
 */
int process_connection (int socket){
    char message[MAXDATA] = {0};
    int retVal = FINISHED;
    User user;

    irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
    user_zero(&user);
    user.socket = socket;

    if (user_find(&user) == OK){
        user_update (&user);
        user_set_ts(&user);
        retVal = tcp_server_recieve(socket, message, MAXDATA);
        if (retVal == TCP_OK){
    	    retVal = process_message (&user, message);
        } else {
            retVal = ERR;
        }
        user_clear(&user);
    }
    irc_debug ("%s end %s", DEBUGTEXT, __FUNCTION__);
    return retVal;
}

/**
 * [process_new_connection  description]
 * @param  socket   [description]
 * @param  hostname [description]
 * @param  ippadr   [description]
 * @return          [description]
 */
int process_new_connection (int socket, char* hostname, char* ippadr){
    int retVal, procMess;
    char message[MAXDATA] = {0};
    User user;

    irc_debug ("%s start %s with socket %d", DEBUGTEXT, __FUNCTION__, socket);
    if ((socket > 0) && hostname && ippadr){

        user_zero(&user);
        user.socket = socket;
        user.host = hostname;
        user.IP = ippadr;

        do{
            retVal = tcp_server_recieve (socket, message, MAXDATA);
            if (retVal == TCP_OK){
                procMess = process_message (&user, message);
            } else {
                retVal = ERR;
            }
        } while ((procMess != FINISHED) || (retVal =! ERR));

        user_clear(&user);
    }
    irc_debug ("%s end %s", DEBUGTEXT, __FUNCTION__);
    return retVal;
}

/**
 * [process_message  description]
 * @param  user    [description]
 * @param  message [description]
 * @return         [description]
 */
int process_message (User *user, char *message){
	char *next_command = message;
    char *command = NULL;
    long commNum;
    int retVal = ERR;
    long errCheck = IRCERR_ERRONEUSCOMMAND;

    irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
    irc_debug("%s Recieved command '%s'", DEBUGTEXT, message);
    if ((message == NULL) || (message[0] == '\0')){
        retVal = cmd_unexpected_quit(user);
    } else {
        do {
            next_command = IRC_UnPipelineCommands(next_command, &command);
            if(command && socket > 0){
                commNum = IRC_CommandQuery(command);
                if (commNum > 0 && commNum <= 202){
                    if (user->userId ||  commNum <= 3){
                        errCheck = command_functions[commNum-1](command, user);
                        retVal = (errCheck == IRC_OK || errCheck == FINISHED)?
                                    errCheck : ERR;
                    }
                } else {
                    errCheck = error_unknowncommand (user,  command);
                }
            }
            free(command);
        } while (next_command!=NULL);
    }
    if (errCheck == IRCERR_INVALIDSOCKET){
        retVal = cmd_unexpected_quit(user);
    }
    irc_error (errCheck, LOG_ERR, message);
    irc_debug ("%s end %s", DEBUGTEXT, __FUNCTION__);
    return retVal;
}


/**
 * [process_timeout  description]
 * @return  [description]
 */
void process_timeout (void) {
    long nelems, errCheck;
    long current_time;
    long *ids=NULL, *modes=NULL, *creatTs=NULL, *actionTs=NULL;
    char *complexUser=NULL;
    char **users=NULL, **nicks=NULL, **real=NULL;
    char **pass=NULL, **hosts=NULL, **ips=NULL;
    int *sockets=NULL;
    int i;
    User userStruc;

    current_time = time(NULL);
    errCheck = IRCTADUser_GetAllLists (&nelems, &ids, &users, &nicks, &real,
                    &pass, &hosts, &ips, &sockets, &modes, &creatTs, &actionTs);
    if (errCheck == IRC_OK){
        for (i = 0; i < nelems; i++){
            if (actionTs[i] + 130 < current_time){
                userStruc.user = users[i];
                userStruc.host = hosts[i];
                userStruc.IP = ips[i];
                userStruc.nick = nicks[i];
                userStruc.password = NULL;
                userStruc.real = real[i];
                userStruc.socket = sockets[i];
                userStruc.userId = ids[i];
                user_complex (&userStruc, &complexUser);
                irc_notice("\nConnection Timeout %s", complexUser);
                cmd_unexpected_quit (&userStruc);
                free(complexUser);
            }
        }
        IRCTADUser_FreeAllLists (nelems, ids, users, nicks, real, pass,
                            hosts, ips, sockets, modes, creatTs, actionTs);
    }
}

void process_delete_users(){
    long nNicks, i;
    char **nicklist;

    IRCTADUser_GetNickList (&nicklist, &nNicks);
    for (i = 0; i < nNicks; i++){
        IRCTAD_Quit (nicklist[i]);
    }
    IRCTADUser_FreeList (nicklist, nNicks);
}
