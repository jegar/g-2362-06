#include "G-2362-06-P1-connection.h"

#include <stdio.h>
#include <redes2/irc.h>
#include <syslog.h>
#include <sys/select.h>
#include <semaphore.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <errno.h>

#define SELECT_REFRESH 500000
#define CONNECT_TIMEOUT 30  /*Seconds*/
#define ERRSTR_CONNECT_SUCCESS          "Conexion abierta con exito.."
#define ERRSTR_MUTEX_SUCCESS            "Mutex creado con exito."
#define ERRSTR_CONNECT_TIMEOUT_FAIL     "Error haciendo ping pong."
#define ERRSTR_CONNECT_TIMEOUT_SUCC     "Ping Pong realizado con exito."
#define NOTMSG_CONNECT_END              "Closing all connections."

/*Estuctura de definicion de conexiones*/
typedef struct _thread_sock {
    pthread_t   id;
    int         sockfd;
} ThreadSock;

struct _connections_t {
    fd_set      sock_fds;       /*Set de conexiones*/
    int         sock_listener;  /*Socket marcado como listener*/
    int         fdmax;          /*Max file descriptor*/
    sem_t       mutex;          /*Semaforo para controlar el acceso y la eliminacion*/
    pthread_t   alarmid;        /*Identidad del hilo de alarma*/
    ThreadSock  thead_sock[MAX_THREADS]; /*Array de fds de los sockets*/
    int         finishing;
};

/* Estuctura privada de definicion */
 static struct _connections_t connection_pool;

/*Inicializa lo necesario para utilizar las conexiones. */

/**
 * Abre un puerto de escucha e inicializa la pool de conexiones.
 * @param  port Puerto de escucha que abrir.Ademas inicializa el
 * temporizador de alarma.
 * @return      OK en caso de exito, ERR en caso de fallo.
 */
int connect_create (int port){
    int i;

    connection_pool.sock_listener = tcp_server_connection(port);
    if (connection_pool.sock_listener >= TCP_OK){
        if (sem_init (&connection_pool.mutex, 0, 1) != ERR){
            FD_ZERO(&connection_pool.sock_fds); /*Ponemos a cero el set de FDs */
            FD_SET(connection_pool.sock_listener, &connection_pool.sock_fds); /*Anyadimos el socket listener.*/
            connection_pool.fdmax = connection_pool.sock_listener; /*Colocamos el socket_listener como maximo*/
            for (i = 0; i < MAX_THREADS; i++){
                connection_pool.thead_sock[i].id = 0;
            }
            connection_pool.finishing = 1;
            if (pthread_create (&connection_pool.alarmid, NULL,
                                (void*)connect_timeout, (void*)NULL) >= 0){
                return OK;
            } else {
                tcp_server_close(connection_pool.sock_listener);
                sem_destroy(&connection_pool.mutex);
            }
            return OK;
        } else {
            tcp_server_close(connection_pool.sock_listener);
        }
    }
    return ERR;
}

/**
 * Acepta la conexion, de la que obtiene el nombre del host y la direcci&oacute;
 * IP. Comprueba si es una nueva conexion o no y la envia a la funcion apropiada
 * de process. Ademas despega el hilo propio y lo cierra cuando acaba la
 * ejecucion. Por ultimo, controla si la conexion se debe acabar o devolver a la
 * pool.
 * @param fdp Puntero al descriptor de fichero del socket que abrimos.
 */
void connect_in_dispatcher(void* fdp){
    int newSocket = ERR;
    char *hostname, *ipaddr;
    int fd = 0;
    int retVal = ERR;
    int i;

    irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
    fd = *(int*)fdp;
    pthread_detach(pthread_self());
    /*Comprobamos si es el listener*/
    if (fd == connection_pool.sock_listener){
        irc_debug ("%s %s New connection detected, socket %d", DEBUGTEXT, __FUNCTION__, fd);
        /*Si lo es creamos uno nuevo*/
        newSocket = tcp_server_accept(connection_pool.sock_listener,
                                    &hostname, &ipaddr);
        irc_debug ("%s %s Assigned socket %d", DEBUGTEXT, __FUNCTION__, newSocket);
        if (newSocket >= 0){
            retVal = process_new_connection(newSocket, hostname, ipaddr);
            /*Bloqueamos la lista de sockets para evitar CC*/
            sem_wait(&connection_pool.mutex);
            /*Anyadimos el socket a la lista*/
            FD_SET(newSocket, &connection_pool.sock_fds);
            if (newSocket > connection_pool.fdmax){
                /*Actualizamos el descriptor mas alto si hace falta.*/
                connection_pool.fdmax = newSocket;
            }
            FD_SET(fd, &connection_pool.sock_fds);
            sem_post(&connection_pool.mutex); /*Abrimos el semaforo de nuevo*/
        }
    } else {
        irc_debug ("%s %s Existing connection detected ", DEBUGTEXT, __FUNCTION__, fd);
        retVal = process_connection(fd);
        if (retVal == FINISHED){
           connect_close(fd);
       } else {
           sem_wait(&connection_pool.mutex);
           FD_SET(fd, &connection_pool.sock_fds);
           sem_post(&connection_pool.mutex); /*Abrimos el semaforo de nuevo*/
       }
    }

    for (i = 0; i < MAX_THREADS; i++){
        sem_wait(&connection_pool.mutex);
        if (connection_pool.thead_sock[i].id == pthread_self()){
            connection_pool.thead_sock[i].id = 0;
            sem_post(&connection_pool.mutex); /*Abrimos el semaforo de nuevo*/
            break;
        }
        sem_post(&connection_pool.mutex); /*Abrimos el semaforo de nuevo*/
    }

    irc_debug ("%s end %s", DEBUGTEXT, __FUNCTION__);
    pthread_exit(NULL);
}

/**
 * Controla la pool de conexiones. Cada vez que la pool recive una conexion,
 * lanza un hilo a connect_in_dispatcher con el descriptor del fichero asociado.
 * Tambi&eacute;n refresca la pool cada medio segundo.
 * @return OK en caso de exito, ERR en caso de fallo.
 */
int connect_in (void){
    int selRet;
    int i, j, threadCreated = ERR;
    fd_set active_sockets;       /*Set de conexiones*/
    struct timeval timeout;

    timeout.tv_sec = 0;
    timeout.tv_usec= SELECT_REFRESH;

    irc_debug ("%s start %s", DEBUGTEXT, __FUNCTION__);
    do{
        FD_ZERO(&active_sockets);
        active_sockets = connection_pool.sock_fds;
        selRet = select(connection_pool.fdmax + 1, &active_sockets, NULL, NULL, &timeout);
    } while (!selRet);

    if (selRet != ERR){
        for (i = 0; i <= connection_pool.fdmax; i++){
            if (FD_ISSET(i, &active_sockets)){
                sem_wait(&connection_pool.mutex);
                FD_CLR(i, &connection_pool.sock_fds); /*Se saca de la pool lo antes posible*/
                sem_post(&connection_pool.mutex);
                /*Aqui se anyade de nuevo a la pool.*/
                for (j = 0; j < MAX_THREADS; j++) {

                    sem_wait(&connection_pool.mutex);
                    if (connection_pool.thead_sock[j].id == 0){
                        connection_pool.thead_sock[j].sockfd = i;
                        if (pthread_create (&connection_pool.thead_sock[j].id,
                                NULL, (void*)connect_in_dispatcher,
                                (void*)&(connection_pool.thead_sock[j].sockfd))
                            < 0){
                                sem_post(&connection_pool.mutex); /*Abrimos el semaforo de nuevo*/
                                irc_errmsg(LOG_CRIT, "SRV_THREAD_NOCREATE.",
                                "Cannot create a new thread.");
                                threadCreated = ERR;
                            }
                        sem_post(&connection_pool.mutex); /*Abrimos el semaforo de nuevo*/
                        break;
                    }
                    sem_post(&connection_pool.mutex); /*Abrimos el semaforo de nuevo*/
                }
                irc_debug ("%s end %s socket %d", DEBUGTEXT, __FUNCTION__, i);
                irc_debug ("%s %s listener socket %d", DEBUGTEXT, __FUNCTION__, connection_pool.sock_listener);
                threadCreated = OK;
            }
        }
    } else {
        irc_errmsg(LOG_CRIT, "SELECT_ERROR.", strerror(errno));
    }
    irc_debug ("%s end %s", DEBUGTEXT, __FUNCTION__);
    return threadCreated;
}

/**
 * Cierra la conexion de un socket.
 * @param socket Descriptor de fichero del socket.
 */
void connect_close (int socket){
    irc_notice ("\n%s %d", "Cerrando conexion de socket", socket);
    sem_wait(&connection_pool.mutex);
    tcp_server_close(socket);
    sem_post(&connection_pool.mutex);
}

/**
 * Finaliza todas las conexiones activas. Ademas destruye la pool de conexiones.
 */
void connect_end(){
    int i;
    int *retval;

    irc_notice ("%s", NOTMSG_CONNECT_END);
    connection_pool.finishing = 0;
    sem_wait(&connection_pool.mutex);
    for (i = 0; i < connection_pool.fdmax; i++){
        if (FD_ISSET(i, &connection_pool.sock_fds)){
            tcp_server_close(i);
        }
    }
    FD_ZERO(&connection_pool.sock_fds);
    connection_pool.fdmax = -1;
    connection_pool.sock_listener = -1;
    process_delete_users();
    sem_post(&connection_pool.mutex);
    sem_destroy(&connection_pool.mutex);
    pthread_join(connection_pool.alarmid, (void*)&retval);
    exit(0);
}

void connect_timeout(void){
    while (connection_pool.finishing){
        process_timeout();
        sleep(30);
    }
    pthread_exit(NULL);
}
